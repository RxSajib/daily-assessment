package com.dailyneed.Services;


import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Settings;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.dailyneed.QuickAlarm.Breakfast;
import com.dailyneed.QuickAlarm.Dinner;
import com.dailyneed.QuickAlarm.Game;
import com.dailyneed.QuickAlarm.Lunch;
import com.dailyneed.QuickAlarm.Work;
import com.dailyneed.alermmanager.DataManager.DataManager;
import com.dailyneed.alermmanager.BroadcastReceiver.CaptuleAlarm;
import com.dailyneed.alermmanager.BroadcastReceiver.MealAlarm;
import com.dailyneed.alermmanager.BroadcastReceiver.StudyAlarm;
import com.dailyneed.alermmanager.BroadcastReceiver.WorkOut;
import com.dailyneed.alermmanager.Memory.CashAlerm;
import com.dailyneed.alermmanager.Memory.ServiceData;
import com.dailyneed.alermmanager.MyEyesTracker;
import com.dailyneed.alermmanager.Offline.AlermManager;
import com.dailyneed.alermmanager.R;

import java.util.Calendar;
import java.util.Locale;

public class MyAlarmServices extends Service {

    MediaPlayer mediaPlayer;
    Handler handler;
    NotificationManagerCompat managerCompat;
    private CashAlerm memory;
    private String AlarmType;
    private ServiceData serviceData;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        serviceData = new ServiceData(getApplicationContext());
        memory = new CashAlerm(getApplicationContext());
        String id = intent.getStringExtra("Timestamp");
        AlarmType = intent.getStringExtra(DataManager.ALARM);
        serviceData.SaveCurrentServiceAlarmType(DataManager.CurrentServiceAlarmType, AlarmType);
        long timestamp = Long.parseLong(id);
        Log.d("TAG", id);

        Calendar timestamp_time = Calendar.getInstance(Locale.ENGLISH);
        timestamp_time.setTimeInMillis(timestamp);
        String Time = DateFormat.format(DataManager.TimeFormat, timestamp_time).toString();


        Intent sajibintent = new Intent();
        sajibintent.setAction(Intent.ACTION_MAIN);
        sajibintent.addCategory(Intent.CATEGORY_LAUNCHER);
        sajibintent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        ComponentName cn = new ComponentName(this, MyEyesTracker.class);
        sajibintent.setComponent(cn);
        startActivity(sajibintent);


        mediaPlayer = MediaPlayer.create(this, Settings.System.DEFAULT_RINGTONE_URI);

        if (mediaPlayer != null) {
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.stop();
            } else {
                mediaPlayer.start();
                mediaPlayer.setLooping(true);

            }
        }

        Intent myintent = new Intent(this, MyEyesTracker.class);
        myintent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingInten = PendingIntent.getActivity(this, 0, myintent, 0);

        managerCompat = NotificationManagerCompat.from(this);

        Notification notifaction = new NotificationCompat.Builder(this, AlermManager.NotifactionChn)
                .setSmallIcon(R.drawable.ic_alarmon)
                .setContentTitle("Alarm ongoing")
                .setContentText("Please tab and cancel your alarm")
                .addAction(R.drawable.ic_eyes, getString(R.string.calcel), pendingInten)
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setCategory(NotificationCompat.CATEGORY_CALL)
                .setContentIntent(pendingInten)
                .setLights(0xff00ff00, 300, 100)

                .build();

        notifaction.flags = Notification.FLAG_ONGOING_EVENT | Notification.FLAG_NO_CLEAR;

        // managerCompat.notify(1, notifaction);
        startForeground(1, notifaction);

        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mediaPlayer.stop();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mediaPlayer.stop();
                Intent serviceIntent = new Intent(getApplicationContext(), MyAlarmServices.class);
                stopService(serviceIntent);
                set_repetalarm();
            }
        }, (60 * 1) * 1000);
    }

    private void set_repetalarm(){

        if(AlarmType.equals(DataManager.Work)){
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MINUTE, 3);
            long time = calendar.getTimeInMillis();
            long TimeConvertCurrentTime = time / 1000;

            memory.PutTime(DataManager.Work, String.valueOf(TimeConvertCurrentTime * 1000));
            AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService(ALARM_SERVICE);
            Intent intent = new Intent(getApplicationContext(), Work.class);

            PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, intent, 0);
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, time, alarmManager.INTERVAL_DAY, pendingIntent);

            Intent alarmChanged = new Intent("android.intent.action.ALARM_CHANGED");
            alarmChanged.putExtra("alarmSet", true);
            getApplicationContext().sendBroadcast(alarmChanged);
            memory.PutTime(DataManager.Work, String.valueOf(time));
            Toast.makeText(getApplicationContext(), "Alarm repeating after 3 min", Toast.LENGTH_SHORT).show();
        }

        if(AlarmType.equals(DataManager.Breakfast)){
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MINUTE, 3);
            long time = calendar.getTimeInMillis();
            long TimeConvertCurrentTime = time / 1000;

            memory.PutTime(DataManager.Work, String.valueOf(TimeConvertCurrentTime * 1000));
            AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService(ALARM_SERVICE);
            Intent intent = new Intent(getApplicationContext(), Breakfast.class);

            PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, intent, 0);
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, time, alarmManager.INTERVAL_DAY, pendingIntent);

            Intent alarmChanged = new Intent("android.intent.action.ALARM_CHANGED");
            alarmChanged.putExtra("alarmSet", true);
            getApplicationContext().sendBroadcast(alarmChanged);
            memory.PutTime(DataManager.Work, String.valueOf(time));
            Toast.makeText(getApplicationContext(), "Alarm repeating after 3 min", Toast.LENGTH_SHORT).show();
        }

        if(AlarmType.equals(DataManager.Lunch)){
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MINUTE, 3);
            long time = calendar.getTimeInMillis();
            long TimeConvertCurrentTime = time / 1000;

            memory.PutTime(DataManager.Work, String.valueOf(TimeConvertCurrentTime * 1000));
            AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService(ALARM_SERVICE);
            Intent intent = new Intent(getApplicationContext(), Lunch.class);

            PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, intent, 0);
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, time, alarmManager.INTERVAL_DAY, pendingIntent);

            Intent alarmChanged = new Intent("android.intent.action.ALARM_CHANGED");
            alarmChanged.putExtra("alarmSet", true);
            getApplicationContext().sendBroadcast(alarmChanged);
            memory.PutTime(DataManager.Work, String.valueOf(time));
            Toast.makeText(getApplicationContext(), "Alarm repeating after 3 min", Toast.LENGTH_SHORT).show();

        }
        if(AlarmType.equals(DataManager.Dinner)){
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MINUTE, 3);
            long time = calendar.getTimeInMillis();
            long TimeConvertCurrentTime = time / 1000;

            memory.PutTime(DataManager.Work, String.valueOf(TimeConvertCurrentTime * 1000));
            AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService(ALARM_SERVICE);
            Intent intent = new Intent(getApplicationContext(), Dinner.class);

            PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, intent, 0);
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, time, alarmManager.INTERVAL_DAY, pendingIntent);

            Intent alarmChanged = new Intent("android.intent.action.ALARM_CHANGED");
            alarmChanged.putExtra("alarmSet", true);
            getApplicationContext().sendBroadcast(alarmChanged);
            memory.PutTime(DataManager.Work, String.valueOf(time));
            Toast.makeText(getApplicationContext(), "Alarm repeating after 3 min", Toast.LENGTH_SHORT).show();

        }
        if(AlarmType.equals(DataManager.Game)){
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MINUTE, 3);
            long time = calendar.getTimeInMillis();
            long TimeConvertCurrentTime = time / 1000;

            memory.PutTime(DataManager.Work, String.valueOf(TimeConvertCurrentTime * 1000));
            AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService(ALARM_SERVICE);
            Intent intent = new Intent(getApplicationContext(), Game.class);

            PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, intent, 0);
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, time, alarmManager.INTERVAL_DAY, pendingIntent);

            Intent alarmChanged = new Intent("android.intent.action.ALARM_CHANGED");
            alarmChanged.putExtra("alarmSet", true);
            getApplicationContext().sendBroadcast(alarmChanged);
            memory.PutTime(DataManager.Work, String.valueOf(time));
            Toast.makeText(getApplicationContext(), "Alarm repeating after 3 min", Toast.LENGTH_SHORT).show();

        }
        if(AlarmType.equals(DataManager.Capsules)){
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MINUTE, 3);
            long time = calendar.getTimeInMillis();
            long TimeConvertCurrentTime = time / 1000;

            memory.PutTime(DataManager.Work, String.valueOf(TimeConvertCurrentTime * 1000));
            AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService(ALARM_SERVICE);
            Intent intent = new Intent(getApplicationContext(), CaptuleAlarm.class);

            PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, intent, 0);
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, time, alarmManager.INTERVAL_DAY, pendingIntent);

            Intent alarmChanged = new Intent("android.intent.action.ALARM_CHANGED");
            alarmChanged.putExtra("alarmSet", true);
            getApplicationContext().sendBroadcast(alarmChanged);
            memory.PutTime(DataManager.Work, String.valueOf(time));
            Toast.makeText(getApplicationContext(), "Alarm repeating after 3 min", Toast.LENGTH_SHORT).show();

        }
        if(AlarmType.equals(DataManager.Study)){
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MINUTE, 3);
            long time = calendar.getTimeInMillis();
            long TimeConvertCurrentTime = time / 1000;

            memory.PutTime(DataManager.Work, String.valueOf(TimeConvertCurrentTime * 1000));
            AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService(ALARM_SERVICE);
            Intent intent = new Intent(getApplicationContext(), StudyAlarm.class);

            PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, intent, 0);
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, time, alarmManager.INTERVAL_DAY, pendingIntent);

            Intent alarmChanged = new Intent("android.intent.action.ALARM_CHANGED");
            alarmChanged.putExtra("alarmSet", true);
            getApplicationContext().sendBroadcast(alarmChanged);
            memory.PutTime(DataManager.Work, String.valueOf(time));
            Toast.makeText(getApplicationContext(), "Alarm repeating after 3 min", Toast.LENGTH_SHORT).show();

        }
        if(AlarmType.equals(DataManager.Workout)){
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MINUTE, 3);
            long time = calendar.getTimeInMillis();
            long TimeConvertCurrentTime = time / 1000;

            memory.PutTime(DataManager.Work, String.valueOf(TimeConvertCurrentTime * 1000));
            AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService(ALARM_SERVICE);
            Intent intent = new Intent(getApplicationContext(), WorkOut.class);

            PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, intent, 0);
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, time, alarmManager.INTERVAL_DAY, pendingIntent);

            Intent alarmChanged = new Intent("android.intent.action.ALARM_CHANGED");
            alarmChanged.putExtra("alarmSet", true);
            getApplicationContext().sendBroadcast(alarmChanged);
            memory.PutTime(DataManager.Work, String.valueOf(time));
            Toast.makeText(getApplicationContext(), "Alarm repeating after 3 min", Toast.LENGTH_SHORT).show();

        }
        if(AlarmType.equals(DataManager.Meal)){
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MINUTE, 3);
            long time = calendar.getTimeInMillis();
            long TimeConvertCurrentTime = time / 1000;

            memory.PutTime(DataManager.Work, String.valueOf(TimeConvertCurrentTime * 1000));
            AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService(ALARM_SERVICE);
            Intent intent = new Intent(getApplicationContext(), MealAlarm.class);

            PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, intent, 0);
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, time, alarmManager.INTERVAL_DAY, pendingIntent);

            Intent alarmChanged = new Intent("android.intent.action.ALARM_CHANGED");
            alarmChanged.putExtra("alarmSet", true);
            getApplicationContext().sendBroadcast(alarmChanged);
            memory.PutTime(DataManager.Work, String.valueOf(time));
            Toast.makeText(getApplicationContext(), "Alarm repeating after 3 min", Toast.LENGTH_SHORT).show();

        }

    }
}
