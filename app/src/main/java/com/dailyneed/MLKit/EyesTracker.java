package com.dailyneed.MLKit;

import android.content.Context;

import com.dailyneed.alermmanager.MyEyesTracker;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.Tracker;
import com.google.android.gms.vision.face.Face;

public class EyesTracker extends Tracker<Face> {

    private static final String TAG = "TAG";
    private final float THRESHOLD = 0.75f;
    private Context context;
    public EyesTracker(Context context) {
        this.context = context;
    }
    @Override
    public void onUpdate(Detector.Detections<Face> detections, Face face) {
        if (face.getIsLeftEyeOpenProbability() > THRESHOLD || face.getIsRightEyeOpenProbability() > THRESHOLD) {

            ((MyEyesTracker)context).updateMainView(Condition.USER_EYES_OPEN);

        }else {

            ((MyEyesTracker)context).updateMainView(com.dailyneed.MLKit.Condition.USER_EYES_CLOSED);
        }
    }
    @Override
    public void onMissing(Detector.Detections<Face> detections) {
        super.onMissing(detections);

        ((MyEyesTracker)context).updateMainView(com.dailyneed.MLKit.Condition.FACE_NOT_FOUND);
    }
    @Override
    public void onDone() {
        super.onDone();
    }
}
