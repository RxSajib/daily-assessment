package com.dailyneed.alermmanager.Model;

public  class TodoList {

    private String Details;
    private String ImageURI;
    private String Themes;
    private String Title;
    private String Type;
    private Long Timestamp;


    public TodoList() {
    }

    public Long getTimestamp() {
        return Timestamp;
    }

    public String getDetails() {
        return Details;
    }

    public String getImageURI() {
        return ImageURI;
    }

    public String getThemes() {
        return Themes;
    }

    public String getTitle() {
        return Title;
    }

    public String getType() {
        return Type;
    }
}
