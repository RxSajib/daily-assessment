package com.dailyneed.alermmanager.Model;

public class AlermTypeData {

    private int image;
    private String title;


    public AlermTypeData() {
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
