package com.dailyneed.alermmanager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.ViewModelProvider;

import android.Manifest;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.dailyneed.MLKit.Condition;
import com.dailyneed.MLKit.FaceTrackerDaemon;
import com.dailyneed.QuickAlarm.Breakfast;
import com.dailyneed.QuickAlarm.Dinner;
import com.dailyneed.QuickAlarm.Game;
import com.dailyneed.QuickAlarm.Lunch;
import com.dailyneed.QuickAlarm.Work;
import com.dailyneed.Services.MyAlarmServices;
import com.dailyneed.ViewModel.ViewModel;
import com.dailyneed.alermmanager.BroadcastReceiver.CaptuleAlarm;
import com.dailyneed.alermmanager.BroadcastReceiver.MealAlarm;
import com.dailyneed.alermmanager.BroadcastReceiver.StudyAlarm;
import com.dailyneed.alermmanager.BroadcastReceiver.WorkOut;
import com.dailyneed.alermmanager.DataManager.DataManager;
import com.dailyneed.alermmanager.Memory.ServiceData;
import com.dailyneed.alermmanager.RoomDatabase.AlarmDatabaseRepository;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.face.FaceDetector;

import java.io.IOException;

public class MyEyesTracker extends AppCompatActivity {


    private ViewModel viewModel;
    boolean flag = false;
    CameraSource cameraSource;
    private MyAlarmServices myAlarmServices;
    private TextView Timetext;
    private String Alarmtype;
    private ServiceData serviceData;
    private AlarmDatabaseRepository databaseRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eyes_tracker);

        databaseRepository = new ViewModelProvider(this).get(AlarmDatabaseRepository.class);
        serviceData = new ServiceData(getApplicationContext());
        Alarmtype = serviceData.GetCurrentServiceType(DataManager.CurrentServiceAlarmType);
        Timetext = findViewById(R.id.Times);
        if (Alarmtype != null) {
            Timetext.setText(Alarmtype);
        }


        //  Timelong = getIntent().getStringExtra("Timestamp");
        //   long Timestamp = Long.parseLong(Timelong)/1000;

        //   Log.d("Timestamp", Timelong);

        //   Calendar calendar = Calendar.getInstance(Locale.ENGLISH);
        //    calendar.setTimeInMillis(Timestamp);
        //    String Time = DateFormat.format(DataManager.TimeFormat, Timestamp).toString();
        //    Timetext.setText(Time);


        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 1);
            Toast.makeText(this, "Permission not granted!\n Grant permission and restart app", Toast.LENGTH_SHORT).show();
        } else {
            init();
        }
    }


    private void init() {

        flag = true;

        initCameraSource();
    }

    //method to create camera source from faceFactoryDaemon class
    private void initCameraSource() {
        FaceDetector detector = new FaceDetector.Builder(this)
                .setTrackingEnabled(true)
                .setClassificationType(FaceDetector.ALL_CLASSIFICATIONS)
                .setMode(FaceDetector.FAST_MODE)
                .build();
        detector.setProcessor(new MultiProcessor.Builder(new FaceTrackerDaemon(MyEyesTracker.this)).build());

        cameraSource = new CameraSource.Builder(this, detector)
                .setRequestedPreviewSize(1024, 768)
                .setFacing(CameraSource.CAMERA_FACING_FRONT)
                .setRequestedFps(30.0f)
                .build();

        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                return;
            }
            cameraSource.start();
        } catch (IOException e) {
            Toast.makeText(MyEyesTracker.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (cameraSource != null) {
            try {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                    return;
                }
                cameraSource.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (cameraSource != null) {
            cameraSource.stop();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (cameraSource != null) {
            cameraSource.release();
        }
    }

    //update view
    public void updateMainView(Condition condition) {
        switch (condition) {
            case USER_EYES_OPEN:

                Intent i = new Intent(this, MyAlarmServices.class);
                stopService(i);
                Animatoo.animateZoom(MyEyesTracker.this);
                finish();
                stopservice();
                break;
            case USER_EYES_CLOSED:
                break;
            case FACE_NOT_FOUND:
                break;
            default:
        }
    }


    private void stopservice() {
        if (Alarmtype != null) {


            if (Alarmtype.equals(DataManager.Capsules)) {
                Intent i = new Intent(getApplicationContext(), MyAlarmServices.class);
                stopService(i);
                AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                Intent intent = new Intent(getApplicationContext(), CaptuleAlarm.class);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, intent, 0);
                alarmManager.cancel(pendingIntent);
                Toast.makeText(getParent(), "Alarm stop " + Alarmtype, Toast.LENGTH_SHORT).show();

                databaseRepository.DeleteDataByID(Alarmtype);
            }
            if (Alarmtype.equals(DataManager.Study)) {
                Intent i = new Intent(getApplicationContext(), MyAlarmServices.class);
                stopService(i);
                AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService(ALARM_SERVICE);
                Intent intent = new Intent(getApplicationContext(), StudyAlarm.class);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, intent, 0);
                alarmManager.cancel(pendingIntent);
                Toast.makeText(getApplicationContext(), "Alarm stop " + Alarmtype, Toast.LENGTH_SHORT).show();

                databaseRepository.DeleteDataByID(Alarmtype);
            }
            if (Alarmtype.equals(DataManager.Workout)) {
                Intent i = new Intent(getApplicationContext(), MyAlarmServices.class);
                stopService(i);
                AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService(ALARM_SERVICE);
                Intent intent = new Intent(getApplicationContext(), WorkOut.class);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, intent, 0);
                alarmManager.cancel(pendingIntent);
                Toast.makeText(getApplicationContext(), "Alarm stop " + Alarmtype, Toast.LENGTH_SHORT).show();

                databaseRepository.DeleteDataByID(Alarmtype);
            }
            if (Alarmtype.equals(DataManager.Meal)) {
                Intent i = new Intent(getApplicationContext(), MyAlarmServices.class);
                stopService(i);
                AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService(ALARM_SERVICE);
                Intent intent = new Intent(getApplicationContext(), MealAlarm.class);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, intent, 0);
                alarmManager.cancel(pendingIntent);
                Toast.makeText(getApplicationContext(), "Alarm stop " + Alarmtype, Toast.LENGTH_SHORT).show();

                databaseRepository.DeleteDataByID(Alarmtype);
            }
            if (Alarmtype.equals(DataManager.Work)) {
                Intent i = new Intent(getApplicationContext(), MyAlarmServices.class);
                stopService(i);
                AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService(ALARM_SERVICE);
                Intent intent = new Intent(getApplicationContext(), Work.class);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, intent, 0);
                alarmManager.cancel(pendingIntent);
                Toast.makeText(getApplicationContext(), "Alarm stop " + Alarmtype, Toast.LENGTH_SHORT).show();

                databaseRepository.DeleteDataByID(Alarmtype);
            }
            if (Alarmtype.equals(DataManager.Breakfast)) {
                Intent i = new Intent(getApplicationContext(), MyAlarmServices.class);
                stopService(i);
                AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService(ALARM_SERVICE);
                Intent intent = new Intent(getApplicationContext(), Breakfast.class);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, intent, 0);
                alarmManager.cancel(pendingIntent);
                Toast.makeText(getApplicationContext(), "Alarm stop " + Alarmtype, Toast.LENGTH_SHORT).show();

                databaseRepository.DeleteDataByID(Alarmtype);
            }
            if (Alarmtype.equals(DataManager.Lunch)) {
                Intent i = new Intent(getApplicationContext(), MyAlarmServices.class);
                stopService(i);
                AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                Intent intent = new Intent(getApplicationContext(), Lunch.class);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, intent, 0);
                alarmManager.cancel(pendingIntent);
                Toast.makeText(getApplicationContext(), "Alarm stop " + Alarmtype, Toast.LENGTH_SHORT).show();

                databaseRepository.DeleteDataByID(Alarmtype);
            }
            if (Alarmtype.equals(DataManager.Dinner)) {
                Intent i = new Intent(getApplicationContext(), MyAlarmServices.class);
                stopService(i);
                AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService(ALARM_SERVICE);
                Intent intent = new Intent(getApplicationContext(), Dinner.class);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, intent, 0);
                alarmManager.cancel(pendingIntent);
                Toast.makeText(getApplicationContext(), "Alarm stop " + Alarmtype, Toast.LENGTH_SHORT).show();

                databaseRepository.DeleteDataByID(Alarmtype);
            }
            if (Alarmtype.equals(DataManager.Game)) {
                Intent i = new Intent(getApplicationContext(), MyAlarmServices.class);
                stopService(i);
                AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService(ALARM_SERVICE);
                Intent intent = new Intent(getApplicationContext(), Game.class);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, intent, 0);
                alarmManager.cancel(pendingIntent);
                Toast.makeText(getApplicationContext(), "Alarm stop " + Alarmtype, Toast.LENGTH_SHORT).show();

                databaseRepository.DeleteDataByID(Alarmtype);
            }

        }
    }

}