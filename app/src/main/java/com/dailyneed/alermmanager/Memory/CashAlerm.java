package com.dailyneed.alermmanager.Memory;


import android.content.Context;
import android.content.SharedPreferences;

public class CashAlerm {

    private SharedPreferences sharedPreferences;

    public CashAlerm(Context context){

        sharedPreferences = context.getSharedPreferences("Key",Context.MODE_PRIVATE );
    }

    public void PutTime(String Key, String Timestamp){
        SharedPreferences.Editor editor =sharedPreferences.edit();
        editor.putString(Key, Timestamp);
        editor.apply();
    }

    public String gettimestamp(String Key){
        return sharedPreferences.getString(Key, null);
    }
}
