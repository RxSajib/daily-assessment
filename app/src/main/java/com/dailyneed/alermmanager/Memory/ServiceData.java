package com.dailyneed.alermmanager.Memory;

import android.content.Context;
import android.content.SharedPreferences;

public class ServiceData {

    private SharedPreferences sharedPreferences;
    public ServiceData(Context context){
        sharedPreferences = context.getSharedPreferences("TAG", Context.MODE_PRIVATE);
    }

    public void SaveCurrentServiceTime(String Key, String Time){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(Key, Time);
        editor.apply();
    }

    public void SaveCurrentServiceAlarmType(String Key, String Type){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(Key, Type);
        editor.apply();
    }

    public String GetCurrentServiceType(String Key){
        return sharedPreferences.getString(Key, null);
    }

    public String GetCurrentServiceTime(String Key){
        return sharedPreferences.getString(Key, null);
    }
}
