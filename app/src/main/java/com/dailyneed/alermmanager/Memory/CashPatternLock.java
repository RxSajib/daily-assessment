package com.dailyneed.alermmanager.Memory;

import android.content.Context;
import android.content.SharedPreferences;

public class CashPatternLock {

    private SharedPreferences sharedPreferences ;

    public CashPatternLock(Context context){
        sharedPreferences = context.getSharedPreferences("TAG", Context.MODE_PRIVATE);
    }

    public void savepassword(String key, String password){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, password);
        editor.apply();
    }

    public void passwordkey(String key,boolean value){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public String getpassword(String key){
        return sharedPreferences.getString(key, null);
    }

    public boolean getpasswordkey(String key){
        return sharedPreferences.getBoolean(key, false);
    }

    public void setlogin_emailaddress(String key, String value){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public String getlogin_emailaddress(String key){
        return sharedPreferences.getString(key, null);
    }
}
