package com.dailyneed.alermmanager.Adapter;

import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dailyneed.alermmanager.DataManager.DataManager;
import com.dailyneed.alermmanager.R;
import com.dailyneed.alermmanager.RoomDatabase.AlarmData;
import com.dailyneed.alermmanager.ViewHolder.AlarmViewHolder;

import java.util.Calendar;
import java.util.List;

public class AlarAdapter extends RecyclerView.Adapter<AlarmViewHolder> {

    private List<AlarmData> alarmModelList;

    public void setAlarmModelList(List<AlarmData> alarmModelList) {
        this.alarmModelList = alarmModelList;
    }

    @NonNull
    @Override
    public AlarmViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new AlarmViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.alerm_layout, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull AlarmViewHolder holder, int position) {
        holder.Details.setText(alarmModelList.get(position).getDetails());
        holder.Title.setText(alarmModelList.get(position).getType());

        long timestamp = alarmModelList.get(position).getTimestamp();

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);
        String Time = DateFormat.format(DataManager.TimeFormat, calendar).toString();
        holder.Time.setText(Time);
    }

    @Override
    public int getItemCount() {
        if(alarmModelList == null){
            return 0;
        }else {
            return alarmModelList.size();
        }
    }
}
