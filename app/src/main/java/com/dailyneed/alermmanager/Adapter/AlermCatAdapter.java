package com.dailyneed.alermmanager.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dailyneed.alermmanager.Model.AlermTypeData;
import com.dailyneed.alermmanager.R;
import com.google.android.material.textview.MaterialTextView;

import java.util.List;

public class AlermCatAdapter extends RecyclerView.Adapter<AlermCatAdapter.CategoryHolder> {

    private setOnClick setOnClick;
    private Context context;
    private List<AlermTypeData> alermTypeDataList;



    public AlermCatAdapter(Context context, List<AlermTypeData> alermTypeDataList) {
        this.context = context;
        this.alermTypeDataList = alermTypeDataList;
    }

    @NonNull
    @Override
    public CategoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.category_single_iteam, parent, false);
        return new CategoryHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryHolder holder, int position) {
        holder.imageView.setImageResource(alermTypeDataList.get(position).getImage());
        holder.title.setText(alermTypeDataList.get(position).getTitle());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setOnClick.Click(alermTypeDataList.get(position).getTitle(), alermTypeDataList.get(position).getImage());
            }
        });
    }

    @Override
    public int getItemCount() {
        if(alermTypeDataList == null){
            return 0;
        }

        return alermTypeDataList.size();
    }

    public class CategoryHolder extends RecyclerView.ViewHolder {

        private ImageView imageView;
        private MaterialTextView title;

        public CategoryHolder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.CategoryImage);
            title = itemView.findViewById(R.id.TitleCategory);
        }
    }


    public interface setOnClick{
        void Click(String title, int image);
    }

    public void setOnClickListener(setOnClick setOnClick){
        this.setOnClick = setOnClick;
    }
}
