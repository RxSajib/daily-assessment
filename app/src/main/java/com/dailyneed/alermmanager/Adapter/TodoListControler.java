package com.dailyneed.alermmanager.Adapter;

import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.dailyneed.alermmanager.Controler.ControlTodoViewList;
import com.dailyneed.alermmanager.DataManager.DataManager;
import com.dailyneed.alermmanager.Model.TodoList;
import com.dailyneed.alermmanager.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class TodoListControler

   extends FirebaseRecyclerAdapter<TodoList, ControlTodoViewList> {


    private OnClickLisiner OnClickLisiner;
    public TodoListControler(@NonNull FirebaseRecyclerOptions<TodoList> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull ControlTodoViewList holder, int position, @NonNull TodoList model) {


        holder.NoteBackground.setVisibility(View.GONE);
        holder.ImageBackground.setVisibility(View.GONE);

        String Type = model.getType();



        if(Type.equals(DataManager.Text)){
            holder.NoteBackground.setVisibility(View.VISIBLE);
            String Background = model.getThemes();

            if(Background.equals(DataManager.Default)){
                holder.NoteBackground.setBackground(holder.itemView.getContext().getResources().getDrawable(R.drawable.todolist_background));


                holder.Title.setText(model.getTitle());
                holder.Details.setText(model.getDetails());



                Long TimestamoLong = model.getTimestamp();
                Calendar timestamp_time = Calendar.getInstance(Locale.ENGLISH);
                timestamp_time.setTimeInMillis(TimestamoLong * 1000);
                String Time = DateFormat.format(DataManager.TimeFormat, timestamp_time).toString();

                Calendar timestamp_date = Calendar.getInstance(Locale.ENGLISH);
                timestamp_date.setTimeInMillis(TimestamoLong * 1000);
                String Date = DateFormat.format(DataManager.DateFormat, timestamp_date).toString();


                Calendar calendar_time = Calendar.getInstance(Locale.ENGLISH);
                SimpleDateFormat simpleDateFormat_time = new SimpleDateFormat(DataManager.TimeFormat);
                String CurrentTime = simpleDateFormat_time.format(calendar_time.getTime());

                Calendar calendar_date = Calendar.getInstance(Locale.ENGLISH);
                SimpleDateFormat simpleDateFormat_date = new SimpleDateFormat(DataManager.DateFormat);
                String CurrentDate = simpleDateFormat_date.format(calendar_date.getTime());

                if(CurrentDate.equals(Date)){
                    holder.Time.setText(DataManager.Today);
                }
                else {
                    holder.Time.setText(Date);
                }
            }

            if(Background.equals(DataManager.Green)){
                holder.NoteBackground.setBackground(holder.itemView.getContext().getResources().getDrawable(R.drawable.green_themes));

                holder.NoteBackground.setVisibility(View.VISIBLE);
                holder.Title.setText(model.getTitle());
                holder.Details.setText(model.getDetails());



                Long TimestamoLong = model.getTimestamp();
                Calendar timestamp_time = Calendar.getInstance(Locale.ENGLISH);
                timestamp_time.setTimeInMillis(TimestamoLong * 1000);
                String Time = DateFormat.format(DataManager.TimeFormat, timestamp_time).toString();

                Calendar timestamp_date = Calendar.getInstance(Locale.ENGLISH);
                timestamp_date.setTimeInMillis(TimestamoLong * 1000);
                String Date = DateFormat.format(DataManager.DateFormat, timestamp_date).toString();


                Calendar calendar_time = Calendar.getInstance(Locale.ENGLISH);
                SimpleDateFormat simpleDateFormat_time = new SimpleDateFormat(DataManager.TimeFormat);
                String CurrentTime = simpleDateFormat_time.format(calendar_time.getTime());

                Calendar calendar_date = Calendar.getInstance(Locale.ENGLISH);
                SimpleDateFormat simpleDateFormat_date = new SimpleDateFormat(DataManager.DateFormat);
                String CurrentDate = simpleDateFormat_date.format(calendar_date.getTime());

                if(CurrentDate.equals(Date)){
                    holder.Time.setText(DataManager.Today);
                }
                else {
                    holder.Time.setText(Date);
                }
            }

            if(Background.equals(DataManager.Red)){
                holder.NoteBackground.setBackground(holder.itemView.getContext().getResources().getDrawable(R.drawable.red_themes));

                holder.NoteBackground.setVisibility(View.VISIBLE);
                holder.Title.setText(model.getTitle());
                holder.Details.setText(model.getDetails());



                Long TimestamoLong = model.getTimestamp();
                Calendar timestamp_time = Calendar.getInstance(Locale.ENGLISH);
                timestamp_time.setTimeInMillis(TimestamoLong * 1000);
                String Time = DateFormat.format(DataManager.TimeFormat, timestamp_time).toString();

                Calendar timestamp_date = Calendar.getInstance(Locale.ENGLISH);
                timestamp_date.setTimeInMillis(TimestamoLong * 1000);
                String Date = DateFormat.format(DataManager.DateFormat, timestamp_date).toString();


                Calendar calendar_time = Calendar.getInstance(Locale.ENGLISH);
                SimpleDateFormat simpleDateFormat_time = new SimpleDateFormat(DataManager.TimeFormat);
                String CurrentTime = simpleDateFormat_time.format(calendar_time.getTime());

                Calendar calendar_date = Calendar.getInstance(Locale.ENGLISH);
                SimpleDateFormat simpleDateFormat_date = new SimpleDateFormat(DataManager.DateFormat);
                String CurrentDate = simpleDateFormat_date.format(calendar_date.getTime());

                if(CurrentDate.equals(Date)){
                    holder.Time.setText(DataManager.Today);
                }
                else {
                    holder.Time.setText(Date);
                }
            }

            if(Background.equals(DataManager.White)){
                holder.NoteBackground.setBackground(holder.itemView.getContext().getResources().getDrawable(R.drawable.white_background));

                holder.NoteBackground.setVisibility(View.VISIBLE);
                holder.Title.setText(model.getTitle());
                holder.Details.setText(model.getDetails());



                Long TimestamoLong = model.getTimestamp();
                Calendar timestamp_time = Calendar.getInstance(Locale.ENGLISH);
                timestamp_time.setTimeInMillis(TimestamoLong * 1000);
                String Time = DateFormat.format(DataManager.TimeFormat, timestamp_time).toString();

                Calendar timestamp_date = Calendar.getInstance(Locale.ENGLISH);
                timestamp_date.setTimeInMillis(TimestamoLong * 1000);
                String Date = DateFormat.format(DataManager.DateFormat, timestamp_date).toString();


                Calendar calendar_time = Calendar.getInstance(Locale.ENGLISH);
                SimpleDateFormat simpleDateFormat_time = new SimpleDateFormat(DataManager.TimeFormat);
                String CurrentTime = simpleDateFormat_time.format(calendar_time.getTime());

                Calendar calendar_date = Calendar.getInstance(Locale.ENGLISH);
                SimpleDateFormat simpleDateFormat_date = new SimpleDateFormat(DataManager.DateFormat);
                String CurrentDate = simpleDateFormat_date.format(calendar_date.getTime());

                if(CurrentDate.equals(Date)){
                    holder.Time.setText(DataManager.Today);
                }
                else {
                    holder.Time.setText(Date);
                }
            }

            if(Background.equals(DataManager.Black)){
                holder.NoteBackground.setBackground(holder.itemView.getContext().getResources().getDrawable(R.drawable.black_themes));

                holder.NoteBackground.setVisibility(View.VISIBLE);
                holder.Title.setText(model.getTitle());
                holder.Details.setText(model.getDetails());



                Long TimestamoLong = model.getTimestamp();
                Calendar timestamp_time = Calendar.getInstance(Locale.ENGLISH);
                timestamp_time.setTimeInMillis(TimestamoLong * 1000);
                String Time = DateFormat.format(DataManager.TimeFormat, timestamp_time).toString();

                Calendar timestamp_date = Calendar.getInstance(Locale.ENGLISH);
                timestamp_date.setTimeInMillis(TimestamoLong * 1000);
                String Date = DateFormat.format(DataManager.DateFormat, timestamp_date).toString();


                Calendar calendar_time = Calendar.getInstance(Locale.ENGLISH);
                SimpleDateFormat simpleDateFormat_time = new SimpleDateFormat(DataManager.TimeFormat);
                String CurrentTime = simpleDateFormat_time.format(calendar_time.getTime());

                Calendar calendar_date = Calendar.getInstance(Locale.ENGLISH);
                SimpleDateFormat simpleDateFormat_date = new SimpleDateFormat(DataManager.DateFormat);
                String CurrentDate = simpleDateFormat_date.format(calendar_date.getTime());

                if(CurrentDate.equals(Date)){
                    holder.Time.setText(DataManager.Today);
                }
                else {
                    holder.Time.setText(Date);
                }
            }

            if(Background.equals(DataManager.Orange)){
                holder.NoteBackground.setBackground(holder.itemView.getContext().getResources().getDrawable(R.drawable.orange_themes));

                holder.NoteBackground.setVisibility(View.VISIBLE);
                holder.Title.setText(model.getTitle());
                holder.Details.setText(model.getDetails());



                Long TimestamoLong = model.getTimestamp();
                Calendar timestamp_time = Calendar.getInstance(Locale.ENGLISH);
                timestamp_time.setTimeInMillis(TimestamoLong * 1000);
                String Time = DateFormat.format(DataManager.TimeFormat, timestamp_time).toString();

                Calendar timestamp_date = Calendar.getInstance(Locale.ENGLISH);
                timestamp_date.setTimeInMillis(TimestamoLong * 1000);
                String Date = DateFormat.format(DataManager.DateFormat, timestamp_date).toString();


                Calendar calendar_time = Calendar.getInstance(Locale.ENGLISH);
                SimpleDateFormat simpleDateFormat_time = new SimpleDateFormat(DataManager.TimeFormat);
                String CurrentTime = simpleDateFormat_time.format(calendar_time.getTime());

                Calendar calendar_date = Calendar.getInstance(Locale.ENGLISH);
                SimpleDateFormat simpleDateFormat_date = new SimpleDateFormat(DataManager.DateFormat);
                String CurrentDate = simpleDateFormat_date.format(calendar_date.getTime());

                if(CurrentDate.equals(Date)){
                    holder.Time.setText(DataManager.Today);
                }
                else {
                    holder.Time.setText(Date);
                }
            }

            if(Background.equals(DataManager.Teal)){
                holder.NoteBackground.setBackground(holder.itemView.getContext().getResources().getDrawable(R.drawable.teal_themes));

                holder.NoteBackground.setVisibility(View.VISIBLE);
                holder.Title.setText(model.getTitle());
                holder.Details.setText(model.getDetails());


                Long TimestamoLong = model.getTimestamp();
                Calendar timestamp_time = Calendar.getInstance(Locale.ENGLISH);
                timestamp_time.setTimeInMillis(TimestamoLong * 1000);
                String Time = DateFormat.format(DataManager.TimeFormat, timestamp_time).toString();

                Calendar timestamp_date = Calendar.getInstance(Locale.ENGLISH);
                timestamp_date.setTimeInMillis(TimestamoLong * 1000);
                String Date = DateFormat.format(DataManager.DateFormat, timestamp_date).toString();


                Calendar calendar_time = Calendar.getInstance(Locale.ENGLISH);
                SimpleDateFormat simpleDateFormat_time = new SimpleDateFormat(DataManager.TimeFormat);
                String CurrentTime = simpleDateFormat_time.format(calendar_time.getTime());

                Calendar calendar_date = Calendar.getInstance(Locale.ENGLISH);
                SimpleDateFormat simpleDateFormat_date = new SimpleDateFormat(DataManager.DateFormat);
                String CurrentDate = simpleDateFormat_date.format(calendar_date.getTime());

                if(CurrentDate.equals(Date)){
                    holder.Time.setText(DataManager.Today);
                }
                else {
                    holder.Time.setText(Date);
                }
            }



        }

        if(Type.equals(DataManager.Image)){
            holder.ImageBackground.setVisibility(View.VISIBLE);
            String Background = model.getThemes();


            if(Background.equals(DataManager.Default)){
                holder.ImageBackground.setBackground(holder.itemView.getContext().getResources().getDrawable(R.drawable.todolist_background));
                ;
                holder.ImageTitle.setText(model.getTitle());
                holder.ImageDetails.setText(model.getDetails());



                Long TimestamoLong = model.getTimestamp();
                Calendar timestamp_time = Calendar.getInstance(Locale.ENGLISH);
                timestamp_time.setTimeInMillis(TimestamoLong * 1000);
                String Time = DateFormat.format(DataManager.TimeFormat, timestamp_time).toString();

                Calendar timestamp_date = Calendar.getInstance(Locale.ENGLISH);
                timestamp_date.setTimeInMillis(TimestamoLong * 1000);
                String Date = DateFormat.format(DataManager.DateFormat, timestamp_date).toString();


                Calendar calendar_time = Calendar.getInstance(Locale.ENGLISH);
                SimpleDateFormat simpleDateFormat_time = new SimpleDateFormat(DataManager.TimeFormat);
                String CurrentTime = simpleDateFormat_time.format(calendar_time.getTime());

                Calendar calendar_date = Calendar.getInstance(Locale.ENGLISH);
                SimpleDateFormat simpleDateFormat_date = new SimpleDateFormat(DataManager.DateFormat);
                String CurrentDate = simpleDateFormat_date.format(calendar_date.getTime());

                if(CurrentDate.equals(Date)){
                    holder.ImageTime.setText(DataManager.Today);
                }
                else {
                    holder.ImageTime.setText(Date);
                }
                Picasso.with(holder.itemView.getContext()).load(model.getImageURI()).networkPolicy(NetworkPolicy.OFFLINE).into(holder.ImageView,
                        new Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError() {
                Picasso.with(holder.itemView.getContext()).load(model.getImageURI()).into(holder.ImageView);
                            }
                        });
            }

            if(Background.equals(DataManager.Green)){
                holder.ImageBackground.setBackground(holder.itemView.getContext().getResources().getDrawable(R.drawable.green_themes));;
                holder.ImageTitle.setText(model.getTitle());
                holder.ImageDetails.setText(model.getDetails());



                Long TimestamoLong = model.getTimestamp();
                Calendar timestamp_time = Calendar.getInstance(Locale.ENGLISH);
                timestamp_time.setTimeInMillis(TimestamoLong * 1000);
                String Time = DateFormat.format(DataManager.TimeFormat, timestamp_time).toString();

                Calendar timestamp_date = Calendar.getInstance(Locale.ENGLISH);
                timestamp_date.setTimeInMillis(TimestamoLong * 1000);
                String Date = DateFormat.format(DataManager.DateFormat, timestamp_date).toString();


                Calendar calendar_time = Calendar.getInstance(Locale.ENGLISH);
                SimpleDateFormat simpleDateFormat_time = new SimpleDateFormat(DataManager.TimeFormat);
                String CurrentTime = simpleDateFormat_time.format(calendar_time.getTime());

                Calendar calendar_date = Calendar.getInstance(Locale.ENGLISH);
                SimpleDateFormat simpleDateFormat_date = new SimpleDateFormat(DataManager.DateFormat);
                String CurrentDate = simpleDateFormat_date.format(calendar_date.getTime());

                if(CurrentDate.equals(Date)){
                    holder.ImageTime.setText(DataManager.Today);
                }
                else {
                    holder.ImageTime.setText(Date);
                }
                Picasso.with(holder.itemView.getContext()).load(model.getImageURI()).
                        networkPolicy(NetworkPolicy.OFFLINE).into(holder.ImageView
                        , new Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError() {
                                Picasso.with(holder.itemView.getContext()).load(model.getImageURI()).into(holder.ImageView);
                            }
                        });
            }

            if(Background.equals(DataManager.Red)){
                holder.ImageBackground.setBackground(holder.itemView.getContext().getResources().getDrawable(R.drawable.red_themes));;
                holder.ImageTitle.setText(model.getTitle());
                holder.ImageDetails.setText(model.getDetails());



                Long TimestamoLong = model.getTimestamp();
                Calendar timestamp_time = Calendar.getInstance(Locale.ENGLISH);
                timestamp_time.setTimeInMillis(TimestamoLong * 1000);
                String Time = DateFormat.format(DataManager.TimeFormat, timestamp_time).toString();

                Calendar timestamp_date = Calendar.getInstance(Locale.ENGLISH);
                timestamp_date.setTimeInMillis(TimestamoLong * 1000);
                String Date = DateFormat.format(DataManager.DateFormat, timestamp_date).toString();


                Calendar calendar_time = Calendar.getInstance(Locale.ENGLISH);
                SimpleDateFormat simpleDateFormat_time = new SimpleDateFormat(DataManager.TimeFormat);
                String CurrentTime = simpleDateFormat_time.format(calendar_time.getTime());

                Calendar calendar_date = Calendar.getInstance(Locale.ENGLISH);
                SimpleDateFormat simpleDateFormat_date = new SimpleDateFormat(DataManager.DateFormat);
                String CurrentDate = simpleDateFormat_date.format(calendar_date.getTime());

                if(CurrentDate.equals(Date)){
                    holder.ImageTime.setText(DataManager.Today);
                }
                else {
                    holder.ImageTime.setText(Date);
                }
                Picasso.with(holder.itemView.getContext()).load(model.getImageURI()).networkPolicy(NetworkPolicy.OFFLINE)
                        .into(holder.ImageView, new Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError() {
                Picasso.with(holder.itemView.getContext()).load(model.getImageURI()).into(holder.ImageView);
                            }
                        });
            }

            if(Background.equals(DataManager.White)){
                holder.ImageBackground.setBackground(holder.itemView.getContext().getResources().getDrawable(R.drawable.white_background));;
                holder.ImageTitle.setText(model.getTitle());
                holder.ImageDetails.setText(model.getDetails());



                Long TimestamoLong = model.getTimestamp();
                Calendar timestamp_time = Calendar.getInstance(Locale.ENGLISH);
                timestamp_time.setTimeInMillis(TimestamoLong * 1000);
                String Time = DateFormat.format(DataManager.TimeFormat, timestamp_time).toString();

                Calendar timestamp_date = Calendar.getInstance(Locale.ENGLISH);
                timestamp_date.setTimeInMillis(TimestamoLong * 1000);
                String Date = DateFormat.format(DataManager.DateFormat, timestamp_date).toString();


                Calendar calendar_time = Calendar.getInstance(Locale.ENGLISH);
                SimpleDateFormat simpleDateFormat_time = new SimpleDateFormat(DataManager.TimeFormat);
                String CurrentTime = simpleDateFormat_time.format(calendar_time.getTime());

                Calendar calendar_date = Calendar.getInstance(Locale.ENGLISH);
                SimpleDateFormat simpleDateFormat_date = new SimpleDateFormat(DataManager.DateFormat);
                String CurrentDate = simpleDateFormat_date.format(calendar_date.getTime());

                if(CurrentDate.equals(Date)){
                    holder.ImageTime.setText(DataManager.Today);
                }
                else {
                    holder.ImageTime.setText(Date);
                }
                Picasso.with(holder.itemView.getContext()).load(model.getImageURI()).networkPolicy(NetworkPolicy.OFFLINE).into(holder.ImageView,
                        new Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError() {
                        Picasso.with(holder.itemView.getContext()).load(model.getImageURI()).into(holder.ImageView);
                            }
                        });
            }

            if(Background.equals(DataManager.Black)){
                holder.ImageBackground.setBackground(holder.itemView.getContext().getResources().getDrawable(R.drawable.black_themes));;
                holder.ImageTitle.setText(model.getTitle());
                holder.ImageDetails.setText(model.getDetails());


                Long TimestamoLong = model.getTimestamp();
                Calendar timestamp_time = Calendar.getInstance(Locale.ENGLISH);
                timestamp_time.setTimeInMillis(TimestamoLong * 1000);
                String Time = DateFormat.format(DataManager.TimeFormat, timestamp_time).toString();

                Calendar timestamp_date = Calendar.getInstance(Locale.ENGLISH);
                timestamp_date.setTimeInMillis(TimestamoLong * 1000);
                String Date = DateFormat.format(DataManager.DateFormat, timestamp_date).toString();


                Calendar calendar_time = Calendar.getInstance(Locale.ENGLISH);
                SimpleDateFormat simpleDateFormat_time = new SimpleDateFormat(DataManager.TimeFormat);
                String CurrentTime = simpleDateFormat_time.format(calendar_time.getTime());

                Calendar calendar_date = Calendar.getInstance(Locale.ENGLISH);
                SimpleDateFormat simpleDateFormat_date = new SimpleDateFormat(DataManager.DateFormat);
                String CurrentDate = simpleDateFormat_date.format(calendar_date.getTime());

                if(CurrentDate.equals(Date)){
                    holder.ImageTime.setText(DataManager.Today);
                }
                else {
                    holder.ImageTime.setText(Date);
                }
                Picasso.with(holder.itemView.getContext()).load(model.getImageURI()).networkPolicy(NetworkPolicy.OFFLINE).into(holder.ImageView
                        , new Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError() {
                Picasso.with(holder.itemView.getContext()).load(model.getImageURI()).into(holder.ImageView);
                            }
                        });
            }
            if(Background.equals(DataManager.Orange)){
                holder.ImageBackground.setBackground(holder.itemView.getContext().getResources().getDrawable(R.drawable.orange_themes));;
                holder.ImageTitle.setText(model.getTitle());
                holder.ImageDetails.setText(model.getDetails());



                Long TimestamoLong = model.getTimestamp();
                Calendar timestamp_time = Calendar.getInstance(Locale.ENGLISH);
                timestamp_time.setTimeInMillis(TimestamoLong * 1000);
                String Time = DateFormat.format(DataManager.TimeFormat, timestamp_time).toString();

                Calendar timestamp_date = Calendar.getInstance(Locale.ENGLISH);
                timestamp_date.setTimeInMillis(TimestamoLong * 1000);
                String Date = DateFormat.format(DataManager.DateFormat, timestamp_date).toString();


                Calendar calendar_time = Calendar.getInstance(Locale.ENGLISH);
                SimpleDateFormat simpleDateFormat_time = new SimpleDateFormat(DataManager.TimeFormat);
                String CurrentTime = simpleDateFormat_time.format(calendar_time.getTime());

                Calendar calendar_date = Calendar.getInstance(Locale.ENGLISH);
                SimpleDateFormat simpleDateFormat_date = new SimpleDateFormat(DataManager.DateFormat);
                String CurrentDate = simpleDateFormat_date.format(calendar_date.getTime());

                if(CurrentDate.equals(Date)){
                    holder.ImageTime.setText(DataManager.Today);
                }
                else {
                    holder.ImageTime.setText(Date);
                }
                Picasso.with(holder.itemView.getContext()).load(model.getImageURI()).networkPolicy(NetworkPolicy.OFFLINE).into(holder.ImageView,
                        new Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError() {
                Picasso.with(holder.itemView.getContext()).load(model.getImageURI()).into(holder.ImageView);
                            }
                        });
            }
            if(Background.equals(DataManager.Teal)){
                holder.ImageBackground.setBackground(holder.itemView.getContext().getResources().getDrawable(R.drawable.teal_themes));;
                holder.ImageTitle.setText(model.getTitle());
                holder.ImageDetails.setText(model.getDetails());



                Long TimestamoLong = model.getTimestamp();
                Calendar timestamp_time = Calendar.getInstance(Locale.ENGLISH);
                timestamp_time.setTimeInMillis(TimestamoLong * 1000);
                String Time = DateFormat.format(DataManager.TimeFormat, timestamp_time).toString();

                Calendar timestamp_date = Calendar.getInstance(Locale.ENGLISH);
                timestamp_date.setTimeInMillis(TimestamoLong * 1000);
                String Date = DateFormat.format(DataManager.DateFormat, timestamp_date).toString();


                Calendar calendar_time = Calendar.getInstance(Locale.ENGLISH);
                SimpleDateFormat simpleDateFormat_time = new SimpleDateFormat(DataManager.TimeFormat);
                String CurrentTime = simpleDateFormat_time.format(calendar_time.getTime());

                Calendar calendar_date = Calendar.getInstance(Locale.ENGLISH);
                SimpleDateFormat simpleDateFormat_date = new SimpleDateFormat(DataManager.DateFormat);
                String CurrentDate = simpleDateFormat_date.format(calendar_date.getTime());

                if(CurrentDate.equals(Date)){
                    holder.ImageTime.setText(DataManager.Today);
                }
                else {
                    holder.ImageTime.setText(Date);
                }


                Picasso.with(holder.itemView.getContext()).load(model.getImageURI()).networkPolicy(NetworkPolicy.OFFLINE).into(holder.ImageView, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {
                Picasso.with(holder.itemView.getContext()).load(model.getImageURI()).into(holder.ImageView);
                    }
                });
            }
        }


        /// todo click event ------------------------------------------------------------
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                OnClickLisiner.OnCick(getRef(holder.getAdapterPosition()).getKey());
                return true;
            }
        });
        /// todo click event ------------------------------------------------------------

    }

    @NonNull
    @Override
    public ControlTodoViewList onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View Mview = LayoutInflater.from(parent.getContext()).inflate(R.layout.todolist_view, parent, false);

        return new ControlTodoViewList(Mview);
    }


    public interface OnClickLisiner{
        void OnCick(String UID);
    }

    public void OnClikLisiner(OnClickLisiner OnClickLisiner){
        this.OnClickLisiner = OnClickLisiner;
    }


}
