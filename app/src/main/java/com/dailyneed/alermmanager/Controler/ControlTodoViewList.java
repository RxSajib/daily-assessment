package com.dailyneed.alermmanager.Controler;

import android.view.View;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.github.siyamed.shapeimageview.mask.PorterShapeImageView;
import com.google.android.material.textview.MaterialTextView;
import com.dailyneed.alermmanager.R;

public class ControlTodoViewList extends RecyclerView.ViewHolder {

    //todo text -----------------------------------------------------------
    public MaterialTextView Title;
    public MaterialTextView Details;
    public MaterialTextView Time;
    public RelativeLayout NoteBackground;

    //todo text ----------------------------------------------------------

    //todo image ----------------------------------------------------------
    public RelativeLayout ImageBackground;
    public PorterShapeImageView ImageView;
    public MaterialTextView ImageTitle;
    public MaterialTextView ImageDetails;
    public MaterialTextView ImageTime;
    //todo image ----------------------------------------------------------

    public ControlTodoViewList(@NonNull View itemView) {
        super(itemView);

        //todo text -----------------------------------------------------------
        Title = itemView.findViewById(R.id.TitleTextID);
        Details = itemView.findViewById(R.id.DetsisTextID);
        Time = itemView.findViewById(R.id.TimeTextID);
        NoteBackground = itemView.findViewById(R.id.NoteLayoutID);
        //todo text -----------------------------------------------------------


        //todo image ----------------------------------------------------------
        ImageBackground = itemView.findViewById(R.id.ImageNoteLayoutID);
        ImageView = itemView.findViewById(R.id.ImageViewID);
        ImageTitle = itemView.findViewById(R.id.ImageTitleTextID);
        ImageDetails = itemView.findViewById(R.id.ImageDetsisTextID);
        ImageTime = itemView.findViewById(R.id.ImageTimeTextID);
        //todo image ----------------------------------------------------------
    }
}
