package com.dailyneed.alermmanager.Cronometer;

import android.content.Context;
import android.content.SharedPreferences;

import com.dailyneed.alermmanager.UI.HomeMenu.StopWatch_Page;

public class MyCronometer implements  Runnable{



        private StopWatch_Page stopWatch_page;


        public static final long MILIS_MIN = 60000;
        public static final long MIL_HOURS = 3600000;

        private Context context;
        private long mstarttime;

        private boolean isRunnign;

        public MyCronometer(Context context) {
            this.context = context;
        }


        public void start() {
            mstarttime = System.currentTimeMillis();
            isRunnign = true;
        }

        public void stop() {
            isRunnign = false;
        }


        @Override
        public void run() {

            while (isRunnign) {
                long since = System.currentTimeMillis() - mstarttime;

                int sec = (int) ((since / 1000) % 60);
                int min = (int) ((since / MILIS_MIN) % 60);
                int hours = (int) ((since / MIL_HOURS) % 24);
                int milis = (int) since / 1000;


                SharedPreferences sharedPreferences_sec = context.getSharedPreferences("pr", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor_sec = sharedPreferences_sec.edit();
                editor_sec.putString("SEC", String.valueOf(sec));
                editor_sec.apply();


                SharedPreferences sharedPreferences_min = context.getSharedPreferences("pr", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor_min = sharedPreferences_min.edit();
                editor_min.putString("MIN", String.valueOf(min));
                editor_min.apply();

            }
        }





}
