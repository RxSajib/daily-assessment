package com.dailyneed.alermmanager.CameraPage;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Toast;

import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class CameraPage extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    ZXingScannerView scannerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        scannerView = new ZXingScannerView(CameraPage.this);
        setContentView(scannerView);
    }

    @Override
    public void handleResult(Result result) {

        Toast.makeText(this, result.toString(), Toast.LENGTH_SHORT).show();
    }


  /*  @Override
    protected void onPause() {
        super.onPause();

        scannerView.stopCamera();
    }

    @Override
    protected void onResume() {
        super.onResume();

        scannerView.setResultHandler(this::handleResult);
        scannerView.startCamera();
    }*/
}