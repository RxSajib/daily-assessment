package com.dailyneed.alermmanager.RoomDatabase;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "AlarmDB")
public class AlarmData {

    @PrimaryKey()
    @NonNull
    private String AlarmID;

    @ColumnInfo(name = "Timestamp")
    @NonNull
    private long Timestamp;


    @ColumnInfo(name = "Details")
    private String Details;


    private String Type;

    public AlarmData() {
    }

    public AlarmData(@NonNull String alarmID, long timestamp, String details, String type) {
        AlarmID = alarmID;
        Timestamp = timestamp;
        Details = details;
        Type = type;
    }

    @NonNull
    public String getAlarmID() {
        return AlarmID;
    }

    public void setAlarmID(@NonNull String alarmID) {
        AlarmID = alarmID;
    }

    public long getTimestamp() {
        return Timestamp;
    }

    public void setTimestamp(long timestamp) {
        Timestamp = timestamp;
    }

    public String getDetails() {
        return Details;
    }

    public void setDetails(String details) {
        Details = details;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }
}
