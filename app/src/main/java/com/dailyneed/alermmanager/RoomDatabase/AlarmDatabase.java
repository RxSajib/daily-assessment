package com.dailyneed.alermmanager.RoomDatabase;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {AlarmData.class}, version = 1)
public abstract class AlarmDatabase extends RoomDatabase {

    public abstract AlarmDao alarmDao();

    private static volatile AlarmDatabase alarmDatabase;


    static AlarmDatabase getInstance(final Context context){
        if(alarmDatabase == null){
            synchronized (AlarmDatabase.class){
                if(alarmDatabase == null){
                    alarmDatabase = Room.databaseBuilder(context.getApplicationContext(), AlarmDatabase.class,
                            "MyAlarmBD")
                            .build();
                }
            }
        }
        return alarmDatabase;
    }
}
