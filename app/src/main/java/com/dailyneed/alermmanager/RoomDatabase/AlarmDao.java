package com.dailyneed.alermmanager.RoomDatabase;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface AlarmDao {

    @Insert
    public void Insertdata(AlarmData alarmData);



    @Query("SELECT * FROM AlarmDB")
    public LiveData<List<AlarmData>> Getdata();


    @Delete
    public void Removedata(AlarmData alarmData);


    @Query("DELETE FROM AlarmDB WHERE AlarmID =:AlarmID")
    public void DeleteByID(String AlarmID);
}
