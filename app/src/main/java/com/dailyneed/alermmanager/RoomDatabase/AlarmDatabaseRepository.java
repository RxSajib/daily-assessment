package com.dailyneed.alermmanager.RoomDatabase;

import android.app.Application;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

public class AlarmDatabaseRepository extends AndroidViewModel {

    private AlarmDao alarmDao;
    private AlarmDatabase alarmDatabase;
    private MutableLiveData<List<AlarmData>> data;

    public AlarmDatabaseRepository(@NonNull Application application) {
        super(application);

        alarmDatabase = AlarmDatabase.getInstance(application);
        alarmDao = alarmDatabase.alarmDao();
        data = new MutableLiveData<>();
    }

    public void DeleteDataByID(String position){
        new DeleteDataAsyTask(alarmDao).execute(position);
    }

    public void DeleteData(AlarmData alarmData){
        new DeleteAsyTask(alarmDao).execute(alarmData);
    }

    public void SetAlarmData(AlarmData alarmData){
        new DataSetAsyTask(alarmDao).execute(alarmData);
    }

    public LiveData<List<AlarmData>> getAlarmData(){
        return alarmDao.Getdata();
    }


    private class DataSetAsyTask extends AsyncTask<AlarmData, Void, Void>{

        private AlarmDao alarmDao;

        public DataSetAsyTask(AlarmDao alarmDao) {
            this.alarmDao = alarmDao;
        }

        @Override
        protected Void doInBackground(AlarmData... alarmData) {
            alarmDao.Insertdata(alarmData[0]);
            return null;
        }
    }

    private class DeleteAsyTask extends AsyncTask<AlarmData, Void, Void>{

        private AlarmDao alarmDao;

        public DeleteAsyTask(AlarmDao alarmDao) {
            this.alarmDao = alarmDao;
        }

        @Override
        protected Void doInBackground(AlarmData... alarmData) {
            alarmDao.Removedata(alarmData[0]);
            return null;
        }
    }


    private class DeleteDataAsyTask extends AsyncTask<String, Void, Void>{

        private AlarmDao alarmDao;

        public DeleteDataAsyTask(AlarmDao alarmDao) {
            this.alarmDao = alarmDao;
        }

        @Override
        protected Void doInBackground(String... strings) {
            alarmDao.DeleteByID(strings[0]);
            return null;
        }
    }

}
