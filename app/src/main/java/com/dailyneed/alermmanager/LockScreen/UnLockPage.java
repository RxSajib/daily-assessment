package com.dailyneed.alermmanager.LockScreen;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;

import com.andrognito.patternlockview.PatternLockView;
import com.andrognito.patternlockview.listener.PatternLockViewListener;
import com.andrognito.patternlockview.utils.PatternLockUtils;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.dailyneed.alermmanager.UI.Activity.MainActivity;
import com.dailyneed.alermmanager.DataManager.DataManager;
import com.dailyneed.alermmanager.Memory.CashPatternLock;
import com.dailyneed.alermmanager.R;
import com.dailyneed.alermmanager.databinding.UnlockBinding;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import java.util.List;

public class UnLockPage extends AppCompatActivity {

    private CashPatternLock cashPatternLock;
    private Animation fad_in_animaction;
    private UnlockBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.unlock);

        init_view();
        setstatusbar();
        send_email_password();
    }

    private void init_view(){
        fad_in_animaction = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadin_animaction);
        cashPatternLock = new CashPatternLock(getApplicationContext());


        cheackpattern();
    }

    private void setstatusbar(){
        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.M){
            getWindow().setStatusBarColor(getResources().getColor(R.color.carbon_black_6));
        }else {
            getWindow().setStatusBarColor(getResources().getColor(R.color.carbon_black_6));
        }
    }


    @Override
    public void onBackPressed() {
    //    super.onBackPressed();


        MaterialAlertDialogBuilder Mbuilder = new MaterialAlertDialogBuilder(UnLockPage.this, R.style.PauseDialog);
        View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.password_backpress_worning, null, false);
        Mbuilder.setView(view);

        AlertDialog alertDialog = Mbuilder.create();

        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();

        LinearLayout Homebutton, Iwantbutton;

        Homebutton = view.findViewById(R.id.HomeButton);
        Iwantbutton = view.findViewById(R.id.IwantButton);

        Homebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goto_homepage();
            }
        });

        Iwantbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

    }

    private void goto_homepage(){
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
        Animatoo.animateSlideRight(UnLockPage.this);
    }

    private void sms_send_password(){
        String email = cashPatternLock.getlogin_emailaddress(DataManager.Email);
        if(email == null){
            MaterialAlertDialogBuilder Mbuilder = new MaterialAlertDialogBuilder(UnLockPage.this);
            View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.login_account_for_resetpassword, null, false);
            Mbuilder.setView(view);


            AlertDialog alertDialog = Mbuilder.create();
            alertDialog.show();


        }else {
            send_email_password();
        }
    }


    private void send_email_password(){

    }

    private void cheackpattern(){
        binding.PatternLockView.addPatternLockListener(new PatternLockViewListener() {
            @Override
            public void onStarted() {

            }

            @Override
            public void onProgress(List<PatternLockView.Dot> progressPattern) {

            }

            @Override
            public void onComplete(List<PatternLockView.Dot> pattern) {
                String patterncode = cashPatternLock.getpassword(DataManager.Password);
                String code = PatternLockUtils.patternToString(binding.PatternLockView, pattern);

                if(code.equals(patterncode)){
                    finish();
                    Animatoo.animateSlideRight(UnLockPage.this);
                }
                else {
                    binding.MessageText.startAnimation(fad_in_animaction);
                    fad_in_animaction.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {
                            binding.MessageText.setText("Pattern error please enter valid pattern");
                            binding.MessageText.setTextColor(getResources().getColor(R.color.carbon_red_400));
                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {

                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });

                }
            }

            @Override
            public void onCleared() {

            }
        });
    }
}