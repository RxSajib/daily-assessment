package com.dailyneed.alermmanager.LockScreen;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.andrognito.patternlockview.PatternLockView;
import com.andrognito.patternlockview.listener.PatternLockViewListener;
import com.andrognito.patternlockview.utils.PatternLockUtils;
import com.dailyneed.alermmanager.DataManager.DataManager;
import com.dailyneed.alermmanager.Memory.CashPatternLock;
import com.dailyneed.alermmanager.R;
import com.dailyneed.alermmanager.databinding.LockscreenBinding;

import java.util.List;

public class CreateLock extends AppCompatActivity {

    private CashPatternLock cashPatternLock;
    private LockscreenBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.lockscreen);


        init_view();
        change_statusbar_color();
    }

    private void init_view(){
        cashPatternLock = new CashPatternLock(getApplicationContext());

        binding.PatternLockView.addPatternLockListener(new PatternLockViewListener() {
            @Override
            public void onStarted() {

            }

            @Override
            public void onProgress(List<PatternLockView.Dot> progressPattern) {

            }

            @Override
            public void onComplete(List<PatternLockView.Dot> pattern) {
                Log.d("code", "Pattern complete: " +
                        PatternLockUtils.patternToString(binding.PatternLockView, pattern));

                binding.SetPasswordButtonID.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String code = PatternLockUtils.patternToString(binding.PatternLockView, pattern);

                        if(code.length() > 3){
                            cashPatternLock.savepassword(DataManager.Password, code);
                            cashPatternLock.passwordkey(DataManager.Passwordkey, true);

                            Toast.makeText(CreateLock.this, "Success set password", Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            Toast.makeText(CreateLock.this, "At list 3 step complected please try again", Toast.LENGTH_SHORT).show();
                        }
                    }
                });


            }

            @Override
            public void onCleared() {
                Log.d("clear", "call");
            }
        });
    }

    private void change_statusbar_color(){
        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.M){
            getWindow().setStatusBarColor(getResources().getColor(R.color.carbon_black_6));
        }
        else {
            getWindow().setStatusBarColor(getResources().getColor(R.color.carbon_black_6));
        }
    }


}