package com.dailyneed.alermmanager.DataManager;

public class DataManager {

    public static final String  Title = "Title";
    public static final String  Details = "Details";
    public static final String  ImageURI = "ImageURI";

    public static final String White = "White";
    public static final String Black = "Black";
    public static final String Red = "Red";
    public static final String Green = "Green";
    public static final String Orange = "Orange";
    public static final String Teal = "Teal";
    public static final String Default = "Default";

    public static final String Type = "Type";
    public static final String Image = "Image";
    public static final String Text = "Text";

    public static final String TodoListImageRoot = "TodoListImage";
    public static final String Themes = "Themes";
    public static final String Timestamp = "Timestamp";
    public static final String Order = "Order";
    public static final String NoteDatabase = "NoteData";

    public static final String TimeFormat = "hh:mm";
    public static final String DateFormat = "dd MMM yyyy";
    public static final String Today = "Today";
    public static final String Search = "Search";
    public static final String  SearchMODE = "\uf8ff";

    public static final String TimeFormatWithAMPM = "hh:mm a";

    public static final String LoginData = "Data";

    public static final String Once = "Once";
    public static final String Daily = "Daily";

    public static final String AlarmTimestamp = "Timestamp";
    public static final String AlarmMode = "Mode";
    public static final String AlarmDetails = "Details";
    public static final String AlarmType = "Type";

    public static final String Capsules = "Capsules";
    public static final String Study = "Study";
    public static final String Workout = "Workout";
    public static final String Meal = "Meal";
    public static final String MyAlarm = "MyAlarm";

    public static final String TimesFormatWithAmPm = "hh:mm a";
    public static final String MacKey = "MacKay";

    public static final String Exit_title = "Confirm Exit!";
    public static final String Exit_details = "If you want to exit our app press exit button, Thanks for downloading our application";

    public static final String Password = "Password";
    public static final String Passwordkey = "Passwordkey";

    public static final String ALARM = "ALARM";
    public static final String STOP_WATCH = "STOP WATCH";
    public static final String CALCULATOR = "CALCULATOR";
    public static final String TODO_LIST = "TODO LIST";
    public static final String Email = "Email";

    public static final String SenderEmailAddress = "lokmanhosen2012@gmail.com";
    public static final String SenderEmailPassword = "~123456Qwerty";
    public static final String QuickAlarm = "QuickAlarm";
    public static final String Work = "Work";
    public static final String Breakfast = "Breakfast";
    public static final String Lunch = "Lunch";
    public static final String Dinner  = "Dinner";
    public static final String Game = "Game";
    public static final int IntentCode = 100;


    public static final String CurrentServiceTime = "Time";
    public static final String CurrentServiceAlarmType = "Type";
}
