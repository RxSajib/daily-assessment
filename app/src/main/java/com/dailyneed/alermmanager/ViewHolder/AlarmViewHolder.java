package com.dailyneed.alermmanager.ViewHolder;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dailyneed.alermmanager.R;
import com.google.android.material.textview.MaterialTextView;

public class AlarmViewHolder extends RecyclerView.ViewHolder {

    public MaterialTextView Time, Title, Details;

    public AlarmViewHolder(@NonNull View itemView) {
        super(itemView);

        Time = itemView.findViewById(R.id.TimeTextID);
        Title = itemView.findViewById(R.id.TitleID);
        Details = itemView.findViewById(R.id.DetailsID);
    }
}
