package com.dailyneed.alermmanager.UI.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.dailyneed.alermmanager.databinding.LoginaccountBinding;
import com.google.firebase.auth.FirebaseAuth;
import com.dailyneed.alermmanager.R;

import dmax.dialog.SpotsDialog;
import es.dmoral.toasty.Toasty;

public class FreeLogin extends AppCompatActivity {

    private FirebaseAuth Mauth;
    private AlertDialog spotsDialog;
    private LoginaccountBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.loginaccount);

        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.login_background));
        } else {
            getWindow().setStatusBarColor(getResources().getColor(R.color.login_background));
        }
        ;

        finding_view();
    }

    void finding_view() {

        Mauth = FirebaseAuth.getInstance();

        binding.backButtonID.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(FreeLogin.this);
        });
        binding.LoginButtonID.setOnClickListener(v -> start_signin());
        binding.ResetPasswordButtonID.setOnClickListener(v -> goto_resetpasswordpage());
    }

    private void start_signin() {
        String EmailText = binding.EmailInputID.getText().toString().trim();
        String PasswordText = binding.PasswordInputID.getText().toString().trim();

        if (EmailText.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Email require", Toast.LENGTH_LONG).show();
        } else if (PasswordText.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Password require", Toast.LENGTH_SHORT).show();
        } else if (PasswordText.length() <= 7) {
            binding.PasswordCharCheackID.setTextColor(getResources().getColor(R.color.carbon_red_400));
        } else {
            spotsDialog = new SpotsDialog.Builder()
                    .setContext(this)
                    .setMessage("Login")
                    .build();
            spotsDialog.show();
            binding.PasswordCharCheackID.setText("All is ok");
            binding.PasswordCharCheackID.setTextColor(getResources().getColor(R.color.carbon_black_38));
            Mauth.signInWithEmailAndPassword(EmailText, PasswordText)
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            gotomain_page();
                            spotsDialog.dismiss();
                        } else {
                            spotsDialog.dismiss();
                            Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnFailureListener(e -> {
                        spotsDialog.dismiss();
                        Toast.makeText(getApplicationContext(), e.getMessage().toString(), Toast.LENGTH_LONG).show();
                    });
        }

    }

    private void gotomain_page() {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
        Toasty.success(getApplicationContext(), "Login Success", Toasty.LENGTH_LONG).show();
    }

    private void goto_resetpasswordpage() {
        Intent intent = new Intent(getApplicationContext(), ForgetPasswordPage.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        Animatoo.animateSlideLeft(FreeLogin.this);
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(FreeLogin.this);
    }
}