package com.dailyneed.alermmanager.UI.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.dailyneed.alermmanager.DataManager.DataManager;
import com.dailyneed.alermmanager.Memory.CashPatternLock;
import com.dailyneed.alermmanager.databinding.FreesignindialoagBinding;
import com.dailyneed.alermmanager.databinding.LoginBinding;
import com.facebook.CallbackManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;
import com.dailyneed.alermmanager.R;

import dmax.dialog.SpotsDialog;

public class LoginPage<mCallbackManager> extends AppCompatActivity {

    private GoogleSignInClient mGoogleSignInClient;
    private static final int RC_SIGN_IN = 100;
    private FirebaseAuth Mauth;

    private CallbackManager mCallbackManager;
    private CashPatternLock cashPatternLock;
    private AlertDialog spotsDialog;
    private LoginBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.login);

        statusbarcolor();
        Mauth = FirebaseAuth.getInstance();


        creating_request();
        inilisize_view();

    }
    private void statusbarcolor(){
        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.M){
            getWindow().setStatusBarColor(getResources().getColor(R.color.login_background));
        }
        else {
            getWindow().setStatusBarColor(getResources().getColor(R.color.login_background));
        }
    }

    private void creating_request(){
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }
    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    private void inilisize_view(){
        cashPatternLock = new CashPatternLock(getApplicationContext());
        binding.CloseButtonID.setOnClickListener(v -> {
            finish();
            Animatoo.animateSlideRight(LoginPage.this);
        });

        binding.SignInID.setOnClickListener(v -> goto_signin_page());
        binding.SignUpID.setOnClickListener(v -> goto_signup_page());
        binding.FacebookSignIn.setOnClickListener(view -> {
            open_dialoag();
        });

        binding.GoogleSignIn.setOnClickListener(v -> {
            signIn();
        });
    }




    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            Toast.makeText(this, "login success", Toast.LENGTH_SHORT).show();

        } catch (ApiException e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
    private void goto_signin_page(){
        Intent intent = new Intent(getApplicationContext(), FreeLogin.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        Animatoo.animateSlideLeft(LoginPage.this);
    }

    private void goto_signup_page(){
        Intent intent = new Intent(getApplicationContext(), FreeRegi.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        Animatoo.animateSlideLeft(LoginPage.this);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);

                firebaseAuthWithGoogle(account.getIdToken());
            } catch (ApiException e) {


                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void firebaseAuthWithGoogle(String idToken) {

        spotsDialog = new SpotsDialog.Builder()
                .setContext(this)
                .setMessage("Login")
                .build();
        spotsDialog.show();

        AuthCredential credential = GoogleAuthProvider.getCredential(idToken, null);
        Mauth.signInWithCredential(credential)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        spotsDialog.dismiss();
                        String Emailaddress = Mauth.getCurrentUser().getEmail();
                        cashPatternLock.setlogin_emailaddress(DataManager.Email, Emailaddress);
                        gotohome_page();


                    } else {
                        spotsDialog.dismiss();
                        Toast.makeText(LoginPage.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });
    }

    private void gotohome_page(){
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(DataManager.LoginData, "Login");
        startActivity(intent);
        finish();
    }


    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(LoginPage.this);
    }

    private void open_dialoag(){
        MaterialAlertDialogBuilder Mbuilder = new MaterialAlertDialogBuilder(LoginPage.this, R.style.PauseDialog);
        FreesignindialoagBinding binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.freesignindialoag, null, false);
        Mbuilder.setView(binding.getRoot());

        androidx.appcompat.app.AlertDialog alertDialog = Mbuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();

        binding.CloseButton.setOnClickListener(view -> {
            alertDialog.dismiss();
        });

        binding.SignButton.setOnClickListener(view -> {
            alertDialog.dismiss();
            spotsDialog = new SpotsDialog.Builder()
                    .setContext(this)
                    .setMessage("Login")
                    .build();
            spotsDialog.show();
            Mauth.signInAnonymously().addOnCompleteListener(task -> {
                if(task.isSuccessful()){
                    gotohome_page();
                    Toast.makeText(getApplicationContext(), "Login success", Toast.LENGTH_SHORT).show();
                }else {
                    spotsDialog.dismiss();
                    Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }).addOnFailureListener(e -> {
                spotsDialog.dismiss();
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            });
        });
    }
}