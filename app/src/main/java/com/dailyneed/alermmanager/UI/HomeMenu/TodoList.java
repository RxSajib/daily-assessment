package com.dailyneed.alermmanager.UI.HomeMenu;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.anstrontechnologies.corehelper.AnstronCoreHelper;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.dailyneed.alermmanager.databinding.TodolistBinding;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textview.MaterialTextView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.iceteck.silicompressorr.FileUtils;
import com.iceteck.silicompressorr.SiliCompressor;
import com.dailyneed.alermmanager.Adapter.TodoListControler;
import com.dailyneed.alermmanager.UI.BottomSheed.TodoBottomSheed;
import com.dailyneed.alermmanager.DataManager.DataManager;
import com.dailyneed.alermmanager.UI.Activity.LoginPage;
import com.dailyneed.alermmanager.R;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import es.dmoral.toasty.Toasty;

import static android.app.Activity.RESULT_OK;


public class TodoList extends Fragment {


    private FirebaseAuth Mauth;
    private DatabaseReference MNoteDatabase;
    private String CurrentUserID;
    private TodoListControler todoListControler;

    private boolean Isopen = true;
    private Handler handler = new Handler();
    private static TodoList instance;
    private TodoBottomSheed todoBottomSheed;

    private RelativeLayout Crossbutton;
    private RelativeLayout PickUpImageButton;
    private static final int IMAGECODE = 100;
    private static final int REQUESTCODE = 100;

    private EditText Title, Details;
    private RelativeLayout AddnoteButton;

    private ImageView WhiteIcon, BlackIcon, RedIcon, GreenIcon, OrangeIcon, TealIcon;
    private RelativeLayout WhiteOval, BlackOval, RedOval, GreenOval, OrangeOval, TealOval;
    private String Color = "";

    private AlertDialog alertDialog;

    //todo image uploading task-----------------
    private ProgressBar ImageUploadProgress;
    private LinearLayout ImageUploadingMessageBox;
    private ImageView ImageUploadStatus;
    private MaterialTextView ImageUploadStatusText;
    //todo image uploading task-----------------


    private StorageReference MImageStores;
    private com.anstrontechnologies.corehelper.AnstronCoreHelper anstronCoreHelper;
    private String ImageDownloadURI = "";

    private DatabaseReference MNoteData;

    private RelativeLayout Progressbar;

    private TodoBottomSheed.BottomSheedLisiner MLisiner;
    private BottomSheetDialog bottomSheetDialog, MDialoagUpdate;
    //todo bottom sheed _inisliize -------------------------


    //todo update bottomsheed view ------------------------------------

    private ImageView WhiteDone, BlackDone, RedDone, GreenDone, OrangeDone, TealDone;
    private RelativeLayout UploadButton;
    //   private RelativeLayout Progressbar;
    private RelativeLayout UploadDataButton;
    //   private RelativeLayout Crossbutton;

    private RelativeLayout PickupImage;
    private static final int IMAGEUPDATE_CODE = 100;


    //todo data-------------------------------------------
    private String TitleText, DetailsText, ImageUri = "";
    private String Type = "";
    private String Themescolor = "";


    private Animation letanimaction_text;
    private Animation right_animaction;

    private Animation slide_to_right;
    private Animation slide_to_right_leat;
    private Animation slide_to_left;
    private Animation close_slide_to_left;
    private TodolistBinding binding;


    public TodoList() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.todolist, container, false);
        binding = DataBindingUtil.inflate(inflater,  R.layout.todolist , container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        initializeview(view);
        return binding.getRoot();
    }

    private void initializeview(View Mview) {

        letanimaction_text = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_to_left);
        right_animaction = AnimationUtils.loadAnimation(getActivity(), R.anim.slider_to_right);

        Mauth = FirebaseAuth.getInstance();

        binding.TodoListViewID.setLayoutManager(
                new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        );

    }

    private void goto_login_page() {
        Intent intent = new Intent(getContext(), LoginPage.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        Animatoo.animateSlideUp(getContext());
    }

    @Override
    public void onStart() {

        FirebaseUser Muser = Mauth.getCurrentUser();

        if (Muser == null) {
            binding.SignInMessage.setVisibility(View.VISIBLE);
            binding.AddButtonID.setVisibility(View.GONE);
            binding.SearchTextID.setVisibility(View.GONE);
            binding.NeststScrollviewID.setVisibility(View.GONE);


            binding.SignInButtonID.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    goto_login_page();
                }
            });


        } else {
            binding.NeststScrollviewID.setVisibility(View.VISIBLE);
            binding.SignInMessage.setVisibility(View.GONE);
            binding.AddButtonID.setVisibility(View.VISIBLE);
            binding.SearchTextID.setVisibility(View.VISIBLE);


            binding.AddButtonID.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    show_fab();

                }
            });


            CurrentUserID = Mauth.getCurrentUser().getUid();
            MNoteDatabase = FirebaseDatabase.getInstance().getReference().child(DataManager.NoteDatabase).child(CurrentUserID);
            MNoteDatabase.keepSynced(true);
            read_server_data();
            todoListControler.startListening();


            binding.SearchTextID.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    String Input = s.toString();
                    if (Input.isEmpty()) {
                        read_server_data();
                    } else {
                        Searching_data(Input, MNoteDatabase);
                    }
                }
            });
        }

        super.onStart();
    }


    public static TodoList getInstance() {
        return instance;
    }

    private void show_fab() {
        if (Isopen) {


            rotateFabForward();
            Isopen = false;

            binding.AddNoteFab.setVisibility(View.VISIBLE);
            binding.AddNoteFabText.setVisibility(View.VISIBLE);

            binding.LogoutFab.setVisibility(View.VISIBLE);
            binding.LogoutFabText.setVisibility(View.VISIBLE);


            slide_to_right_leat = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_to_right_leat);
            binding.AddNoteFabText.startAnimation(slide_to_right_leat);

            slide_to_right_leat.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    binding.AddNoteFabText.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animation animation) {

                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });

            slide_to_right = AnimationUtils.loadAnimation(getActivity(), R.anim.slider_to_right);
            binding.LogoutFabText.startAnimation(slide_to_right);
            slide_to_right.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    binding.LogoutFabText.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animation animation) {

                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });


            slide_to_right_leat = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_to_left);
            binding.LogoutFab.startAnimation(slide_to_right_leat);
            binding.AddNoteFab.startAnimation(slide_to_right_leat);

            slide_to_right_leat.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    binding.LogoutFab.setVisibility(View.VISIBLE);
                    binding.AddNoteFab.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animation animation) {

                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });

            binding.AddNoteFab.setOnClickListener(v -> open_buttonSheed_Dialoag());

            binding.LogoutFab.setOnClickListener(v -> {
                Mauth.signOut();
                binding.AddNoteFab.setVisibility(View.GONE);
                binding.AddNoteFabText.setVisibility(View.GONE);
                binding.LogoutFab.setVisibility(View.GONE);
                binding.LogoutFabText.setVisibility(View.GONE);
                binding.NothingListMessageID.setVisibility(View.GONE);
                rebildcode();

                Toasty.success(getContext(), "LogOut Success", Toasty.LENGTH_LONG).show();

            });
        } else {
            rotateFabBackward();

            slide_to_right_leat = AnimationUtils.loadAnimation(getActivity(), R.anim.diable_slide_to_right);
            binding.AddNoteFab.startAnimation(slide_to_right_leat);
            slide_to_right_leat.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    binding.AddNoteFab.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });


            slide_to_right = AnimationUtils.loadAnimation(getActivity(), R.anim.diable_slide_to_right);
            binding.LogoutFab.startAnimation(slide_to_right);
            slide_to_right.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    binding.LogoutFab.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });

            slide_to_left = AnimationUtils.loadAnimation(getActivity(), R.anim.disiable_slide_toleft);
            binding.AddNoteFabText.startAnimation(slide_to_left);
            binding.LogoutFabText.startAnimation(slide_to_left);
            slide_to_left.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    binding.AddNoteFabText.setVisibility(View.GONE);
                    binding.LogoutFabText.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });

            Isopen = true;
        }
    }


    ///todo read data from server --------------------------------
    public void read_server_data() {


        Query FirebaseASS = MNoteDatabase.orderByChild(DataManager.Order);

        FirebaseRecyclerOptions<com.dailyneed.alermmanager.Model.TodoList> options = new FirebaseRecyclerOptions.Builder<com.dailyneed.alermmanager.Model.TodoList>()
                .setQuery(FirebaseASS, com.dailyneed.alermmanager.Model.TodoList.class)
                .build();

        todoListControler = new TodoListControler(options);
        binding.TodoListViewID.setAdapter(todoListControler);


        todoListControler.startListening();

        FirebaseASS.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    binding.NothingListMessageID.setVisibility(View.GONE);
                } else {
                    binding.NothingListMessageID.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        todoListControler.OnClikLisiner(UID -> open_dialoag(UID));

    }
    ///todo read data from server --------------------------------


    //todo remove data ----------------
    private void open_dialoag(String UID) {


        DatabaseReference MDeleteData = FirebaseDatabase.getInstance().getReference().child(DataManager.NoteDatabase).child(FirebaseAuth.getInstance().getCurrentUser().getUid());


        MaterialAlertDialogBuilder Mbuilder = new MaterialAlertDialogBuilder(getContext());

        Mbuilder.setTitle("Are you sure to remove?");
        Mbuilder.setIcon(R.drawable.delete_red);
        Mbuilder.setMessage("If you want to remove your note press continue and upload press modify");
        Mbuilder.setPositiveButton("Continue", (dialog, which) -> MDeleteData.child(UID)
                .removeValue()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            dialog.dismiss();
                            Toasty.success(getContext(), "Success", Toasty.LENGTH_LONG).show();
                        } else {
                            dialog.dismiss();
                            Toast.makeText(getContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .addOnFailureListener(e -> {
                    dialog.dismiss();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }));

        Mbuilder.setNegativeButton("No", (dialog, which) -> dialog.dismiss());

        Mbuilder.setNeutralButton("Modify", (dialog, which) -> {

            MDialoagUpdate = new BottomSheetDialog(getContext(), R.style.CustomBottomSheetDialogTheme);
            View Mview = LayoutInflater.from(getContext()).inflate(R.layout.todolist_bottomsheed, null, false);
            MDialoagUpdate.setContentView(Mview);

            MDialoagUpdate.show();
            dialog.dismiss();

            updating_view(UID, Mview);
            reading_data_server(UID);

        });


        AlertDialog alertDialog = Mbuilder.create();
        alertDialog.show();
    }


    private void Searching_data(String Details, DatabaseReference MDatabase) {
        String ToLowerCase = Details.toLowerCase();
        Query FirebaseSearcruser = MDatabase.orderByChild(DataManager.Search).startAt(ToLowerCase).endAt(ToLowerCase + DataManager.SearchMODE);


        FirebaseRecyclerOptions<com.dailyneed.alermmanager.Model.TodoList> options = new FirebaseRecyclerOptions.Builder<com.dailyneed.alermmanager.Model.TodoList>()
                .setQuery(FirebaseSearcruser, com.dailyneed.alermmanager.Model.TodoList.class)
                .build();

        todoListControler = new TodoListControler(options);
        binding.TodoListViewID.setAdapter(todoListControler);


        todoListControler.startListening();

        FirebaseSearcruser.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    binding.NothingListMessageID.setVisibility(View.GONE);
                } else {
                    binding.NothingListMessageID.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


        todoListControler.OnClikLisiner(UID -> open_dialoag(UID));
    }


    public void rotateFabForward() {
        ViewCompat.animate(binding.AddButtonID)
                .rotation(45F)
                .withLayer()
                .setDuration(200L)

                .start();
    }

    public void rotateFabBackward() {
        ViewCompat.animate(binding.AddButtonID)
                .rotation(0.0F)
                .withLayer()
                .setDuration(200L)
                .start();
    }


    private void rebildcode() {
        FirebaseUser Muser = Mauth.getCurrentUser();

        if (Muser == null) {
            binding.SignInMessage.setVisibility(View.VISIBLE);
            binding.AddButtonID.setVisibility(View.GONE);
            binding.AddNoteFabText.setVisibility(View.GONE);
            binding.NeststScrollviewID.setVisibility(View.GONE);


            binding.SignInButtonID.setOnClickListener(v -> goto_login_page());


        } else {
            binding.NeststScrollviewID.setVisibility(View.VISIBLE);
            binding.SignInMessage.setVisibility(View.GONE);
            binding.AddButtonID.setVisibility(View.VISIBLE);
            binding.AddNoteFabText.setVisibility(View.VISIBLE);


            binding.AddButtonID.setOnClickListener(v -> show_fab());


            CurrentUserID = Mauth.getCurrentUser().getUid();
            MNoteDatabase = FirebaseDatabase.getInstance().getReference().child(DataManager.NoteDatabase).child(CurrentUserID);
            read_server_data();
            todoListControler.startListening();


            binding.AddNoteFabText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    String Input = s.toString();
                    if (Input.isEmpty()) {
                        read_server_data();
                    } else {
                        Searching_data(Input, MNoteDatabase);
                    }
                }
            });
        }

    }


    //todo button sheed dialoag ------------------------------------------------------------
    private void open_buttonSheed_Dialoag() {
        bottomSheetDialog = new BottomSheetDialog(getContext() ,R.style.CustomBottomSheetDialogTheme);

        View Mview = LayoutInflater.from(getContext()).inflate(R.layout.todolist_bottomsheed, null, false);


        bottomSheetDialog.setContentView(Mview);
        bottomSheetDialog.show();

        bottomSheetDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        set_up_allview(Mview);

    }
    //todo button sheed dialoag ------------------------------------------------------------


    //todo bottomsheed view ----------------------------------
    private void set_up_allview(View view) {


        /// todo convert image by AnstronCoreHelper -----------------------------------
        anstronCoreHelper = new AnstronCoreHelper(getActivity());
        /// todo convert image by AnstronCoreHelper -----------------------------------

        Progressbar = view.findViewById(R.id.ProgressbarID);

        Title = view.findViewById(R.id.TitleID);
        Details = view.findViewById(R.id.DetsilsID);
        AddnoteButton = view.findViewById(R.id.AddNoteButtonID);

        //todo Firebase Utils -------------------------------------------
        Mauth = FirebaseAuth.getInstance();
        CurrentUserID = Mauth.getCurrentUser().getUid();
        MImageStores = FirebaseStorage.getInstance().getReference().child(DataManager.TodoListImageRoot);
        MNoteData = FirebaseDatabase.getInstance().getReference().child(DataManager.NoteDatabase);
        MNoteData.keepSynced(true);
        //todo Firebase Utils -------------------------------------------

        //todo finding all done image and oval button------------------------------------------------------
        WhiteOval = view.findViewById(R.id.WhiteButtonID);
        BlackOval = view.findViewById(R.id.BlackButtonID);
        RedOval = view.findViewById(R.id.RedButttonID);
        GreenOval = view.findViewById(R.id.GreenButtonID);
        OrangeOval = view.findViewById(R.id.OrangeButtonID);
        TealOval = view.findViewById(R.id.TealButtonID);

        WhiteIcon = view.findViewById(R.id.WhiteButtonIconID);
        BlackIcon = view.findViewById(R.id.BlackImageButtonID);
        RedIcon = view.findViewById(R.id.RedImageButtonID);
        GreenIcon = view.findViewById(R.id.GreenImageButtonID);
        OrangeIcon = view.findViewById(R.id.OrangeImageButtonID);
        TealIcon = view.findViewById(R.id.TealImageButtonID);
        //todo finding all done image and oval button------------------------------------------------------


        //todo control all oval button -------------------------------------------------------
        WhiteOval.setOnClickListener(v -> {
            WhiteIcon.setVisibility(View.VISIBLE);
            BlackIcon.setVisibility(View.GONE);
            RedIcon.setVisibility(View.GONE);
            GreenIcon.setVisibility(View.GONE);
            OrangeIcon.setVisibility(View.GONE);
            TealIcon.setVisibility(View.GONE);

            Color = DataManager.White;
        });


        BlackOval.setOnClickListener(v -> {
            WhiteIcon.setVisibility(View.GONE);
            BlackIcon.setVisibility(View.VISIBLE);
            RedIcon.setVisibility(View.GONE);
            GreenIcon.setVisibility(View.GONE);
            OrangeIcon.setVisibility(View.GONE);
            TealIcon.setVisibility(View.GONE);

            Color = DataManager.Black;
        });

        RedOval.setOnClickListener(v -> {
            WhiteIcon.setVisibility(View.GONE);
            BlackIcon.setVisibility(View.GONE);
            RedIcon.setVisibility(View.VISIBLE);
            GreenIcon.setVisibility(View.GONE);
            OrangeIcon.setVisibility(View.GONE);
            TealIcon.setVisibility(View.GONE);

            Color = DataManager.Red;
        });

        GreenOval.setOnClickListener(v -> {
            WhiteIcon.setVisibility(View.GONE);
            BlackIcon.setVisibility(View.GONE);
            RedIcon.setVisibility(View.GONE);
            GreenIcon.setVisibility(View.VISIBLE);
            OrangeIcon.setVisibility(View.GONE);
            TealIcon.setVisibility(View.GONE);

            Color = DataManager.Green;
        });

        OrangeOval.setOnClickListener(v -> {
            WhiteIcon.setVisibility(View.GONE);
            BlackIcon.setVisibility(View.GONE);
            RedIcon.setVisibility(View.GONE);
            GreenIcon.setVisibility(View.GONE);
            OrangeIcon.setVisibility(View.VISIBLE);
            TealIcon.setVisibility(View.GONE);

            Color = DataManager.Orange;
        });

        TealOval.setOnClickListener(v -> {
            WhiteIcon.setVisibility(View.GONE);
            BlackIcon.setVisibility(View.GONE);
            RedIcon.setVisibility(View.GONE);
            GreenIcon.setVisibility(View.GONE);
            OrangeIcon.setVisibility(View.GONE);
            TealIcon.setVisibility(View.VISIBLE);

            Color = DataManager.Teal;
        });
        //todo control all oval button -------------------------------------------------------


        //todo image uploading task------------------------------------
        ImageUploadProgress = view.findViewById(R.id.ImageProgressbarID);
        ImageUploadingMessageBox = view.findViewById(R.id.ImageUploadingMessage);
        ImageUploadStatus = view.findViewById(R.id.ImageUploadStatus);
        ImageUploadStatusText = view.findViewById(R.id.ImageUploadStatusText);
        //todo image uploading task------------------------------------


        AddnoteButton.setOnClickListener(v -> posting_data());

        Crossbutton = view.findViewById(R.id.CrossButtonID);
        Crossbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
            }
        });


        PickUpImageButton = view.findViewById(R.id.PicUpImageID);
        PickUpImageButton.setOnClickListener(v -> {
            MaterialAlertDialogBuilder Mbuilder = new MaterialAlertDialogBuilder(getContext());

            View Mview = LayoutInflater.from(getContext()).inflate(R.layout.pickup_image_layout, null, false);
            Mbuilder.setView(Mview);


            alertDialog = Mbuilder.create();
            alertDialog.show();

            RelativeLayout CameraButton = Mview.findViewById(R.id.CameraButtonID);
            RelativeLayout PhotosButton = Mview.findViewById(R.id.PhotosButtonID);


            CameraButton.setOnClickListener(v1 -> {

            });

            PhotosButton.setOnClickListener(v12 -> {

                if (RequestExternal_Stroge()) {
                    Intent intent = new Intent(Intent.ACTION_PICK);
                    intent.setType("image/*");
                    startActivityForResult(intent, IMAGECODE);
                }


            });
        });
    }


    /// todo start posting to server ------------------------------------------------------
    private void posting_data() {


        String TitleText = Title.getText().toString().trim();
        String DetailsText = Details.getText().toString().trim();

        if (TitleText.isEmpty()) {
            Toast.makeText(getActivity(), "Title Require", Toast.LENGTH_SHORT).show();
        } else if (DetailsText.isEmpty()) {
            Toast.makeText(getActivity(), "Details Require", Toast.LENGTH_SHORT).show();
        } else {

            Progressbar.setVisibility(View.VISIBLE);
            AddnoteButton.setEnabled(false);

            Long Timestamplong = System.currentTimeMillis() / 1000;
            String TimestampString = Timestamplong.toString();

            Map<String, Object> PostMap = new HashMap<String, Object>();
            PostMap.put(DataManager.Title, TitleText);
            PostMap.put(DataManager.Details, DetailsText);
            PostMap.put(DataManager.Timestamp, Timestamplong);
            PostMap.put(DataManager.Timestamp, Timestamplong);
            PostMap.put(DataManager.Search, TitleText.toLowerCase());
            PostMap.put(DataManager.Order, ~(Integer.parseInt(TimestampString) - 1));

            if (ImageDownloadURI != null) {
                PostMap.put(DataManager.ImageURI, ImageDownloadURI);
                PostMap.put(DataManager.Type, DataManager.Image);

            }

            if (Color != null) {
                PostMap.put(DataManager.Themes, Color);
            }
            if (Color == "") {
                PostMap.put(DataManager.Themes, DataManager.Default);
            }


            if (ImageDownloadURI == "") {
                PostMap.put(DataManager.Type, DataManager.Text);
            }


            MNoteData.child(CurrentUserID)
                    .child(TimestampString)
                    .updateChildren(PostMap)
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            ImageDownloadURI = "";
                            Progressbar.setVisibility(View.GONE);
                            AddnoteButton.setEnabled(true);

                            Toast.makeText(getContext(), "Success", Toast.LENGTH_LONG).show();

                            bottomSheetDialog.dismiss();

                            read_server_data();
                        } else {
                            ImageDownloadURI = "";
                            Progressbar.setVisibility(View.GONE);
                            AddnoteButton.setEnabled(true);
                            Toast.makeText(getContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnFailureListener(e -> {
                        ImageDownloadURI = "";
                        Progressbar.setVisibility(View.GONE);
                        AddnoteButton.setEnabled(true);
                        Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    });

        }
    }
    /// todo start posting to server ------------------------------------------------------


    //todo bottomsheed view ----------------------------------


    /// todo request bottomsheed_permission ----------------------
    private boolean RequestExternal_Stroge() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {

            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUESTCODE);

            return false;
        }
    }
    /// todo request bottomsheed_permission ----------------------

    ///todo image request ---------------------------------

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == IMAGECODE && resultCode == RESULT_OK) {
            alertDialog.dismiss();

            ImageUploadProgress.setVisibility(View.VISIBLE);
            ImageUploadingMessageBox.setVisibility(View.GONE);

            Uri image_uri = data.getData();
            File Mfile = new File(SiliCompressor.with(getActivity())
                    .compress(FileUtils.getPath(getActivity(), image_uri), new File(getActivity().getCacheDir(), "temp")));

            Uri from_file = Uri.fromFile(Mfile);
            MImageStores.child(anstronCoreHelper.getFileNameFromUri(from_file))
                    .putFile(from_file)
                    .addOnSuccessListener(taskSnapshot -> {
                        if (taskSnapshot.getMetadata() != null) {
                            if (taskSnapshot.getMetadata().getReference() != null) {
                                Task<Uri> image_uri1 = taskSnapshot.getStorage().getDownloadUrl();

                                image_uri1.addOnSuccessListener(new OnSuccessListener<Uri>() {
                                    @Override
                                    public void onSuccess(Uri uri) {
                                        if (uri != null) {
                                            ImageDownloadURI = uri.toString();

                                            ImageUploadProgress.setVisibility(View.GONE);
                                            ImageUploadingMessageBox.setVisibility(View.VISIBLE);
                                            ImageUploadStatus.setVisibility(View.VISIBLE);
                                            ImageUploadStatusText.setVisibility(View.VISIBLE);
                                            ImageUploadStatusText.setText("Image Upload Success");

                                        } else {

                                            ImageUploadProgress.setVisibility(View.GONE);
                                            ImageUploadingMessageBox.setVisibility(View.VISIBLE);
                                            ImageUploadStatus.setVisibility(View.GONE);
                                            ImageUploadStatusText.setVisibility(View.VISIBLE);
                                            ImageUploadStatusText.setText("Image Upload Failed");

                                            Toast.makeText(getContext(), "Somethings error on the server", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                ImageUploadProgress.setVisibility(View.GONE);
                                                ImageUploadingMessageBox.setVisibility(View.VISIBLE);
                                                ImageUploadStatus.setVisibility(View.GONE);
                                                ImageUploadStatusText.setVisibility(View.VISIBLE);
                                                ImageUploadStatusText.setText("Image Upload Failed");
                                                Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                                            }
                                        });
                            }
                        }
                    })
                    .addOnFailureListener(e -> {
                        ImageUploadProgress.setVisibility(View.GONE);
                        ImageUploadingMessageBox.setVisibility(View.VISIBLE);
                        ImageUploadStatus.setVisibility(View.GONE);
                        ImageUploadStatusText.setVisibility(View.VISIBLE);
                        ImageUploadStatusText.setText("Image Upload Failed");
                        Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    });

        }
        if (requestCode == IMAGEUPDATE_CODE && resultCode == RESULT_OK) {
            alertDialog.dismiss();

            ImageUploadProgress.setVisibility(View.VISIBLE);
            ImageUploadingMessageBox.setVisibility(View.GONE);

            Uri image_uri = data.getData();
            File Mfile = new File(SiliCompressor.with(getActivity())
                    .compress(FileUtils.getPath(getActivity(), image_uri), new File(getActivity().getCacheDir(), "temp")));

            Uri from_file = Uri.fromFile(Mfile);
            MImageStores.child(anstronCoreHelper.getFileNameFromUri(from_file))
                    .putFile(from_file)
                    .addOnSuccessListener(taskSnapshot -> {
                        if (taskSnapshot.getMetadata() != null) {
                            if (taskSnapshot.getMetadata().getReference() != null) {
                                Task<Uri> image_uri12 = taskSnapshot.getStorage().getDownloadUrl();

                                image_uri12.addOnSuccessListener(new OnSuccessListener<Uri>() {
                                    @Override
                                    public void onSuccess(Uri uri) {
                                        if (uri != null) {
                                            ImageUri = uri.toString();
                                            Type = DataManager.Image;
                                            ImageUploadProgress.setVisibility(View.GONE);
                                            ImageUploadingMessageBox.setVisibility(View.VISIBLE);
                                            ImageUploadStatus.setVisibility(View.VISIBLE);
                                            ImageUploadStatusText.setVisibility(View.VISIBLE);
                                            ImageUploadStatusText.setText("Image Upload Success");

                                        } else {
                                            Type = DataManager.Text;
                                            ImageUploadProgress.setVisibility(View.GONE);
                                            ImageUploadingMessageBox.setVisibility(View.VISIBLE);
                                            ImageUploadStatus.setVisibility(View.GONE);
                                            ImageUploadStatusText.setVisibility(View.VISIBLE);
                                            ImageUploadStatusText.setText("Image Upload Failed");

                                            Toast.makeText(getContext(), "Somethings error on the server", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                Type = DataManager.Text;
                                                ImageUploadProgress.setVisibility(View.GONE);
                                                ImageUploadingMessageBox.setVisibility(View.VISIBLE);
                                                ImageUploadStatus.setVisibility(View.GONE);
                                                ImageUploadStatusText.setVisibility(View.VISIBLE);
                                                ImageUploadStatusText.setText("Image Upload Failed");
                                                Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                                            }
                                        });
                            }
                        }
                    })
                    .addOnFailureListener(e -> {
                        Type = DataManager.Text;
                        ImageUploadProgress.setVisibility(View.GONE);
                        ImageUploadingMessageBox.setVisibility(View.VISIBLE);
                        ImageUploadStatus.setVisibility(View.GONE);
                        ImageUploadStatusText.setVisibility(View.VISIBLE);
                        ImageUploadStatusText.setText("Image Upload Failed");
                        Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    });
        }
    }

    ///todo image request ---------------------------------


    ///todo get data from server and update view ----------------------------
    private void updating_view(String UID, View Mview) {

        anstronCoreHelper = new AnstronCoreHelper(getContext());

        /// todo background work initialize ---------------------------------
        ImageUploadProgress = Mview.findViewById(R.id.ImageProgressbarID);
        ImageUploadingMessageBox = Mview.findViewById(R.id.ImageUploadingMessage);
        ImageUploadStatus = Mview.findViewById(R.id.ImageUploadStatus);
        ImageUploadStatusText = Mview.findViewById(R.id.ImageUploadStatusText);
        /// todo background work initialize ---------------------------------

        ///todo server initialize ---------------------------------------------
        Mauth = FirebaseAuth.getInstance();
        CurrentUserID = Mauth.getCurrentUser().getUid();
        MNoteData = FirebaseDatabase.getInstance().getReference().child(DataManager.NoteDatabase).child(CurrentUserID);
        MImageStores = FirebaseStorage.getInstance().getReference().child(DataManager.TodoListImageRoot);
        ///todo server initialize ---------------------------------------------


        PickupImage = Mview.findViewById(R.id.PicUpImageID);
        CurrentUserID = Mauth.getCurrentUser().getUid();
        UploadButton = Mview.findViewById(R.id.AddNoteButtonID);
        Crossbutton = Mview.findViewById(R.id.CrossButtonID);
        Crossbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
            }
        });

        ///todo initialize all button and oval ----------------------------------------------------
        Title = Mview.findViewById(R.id.TitleID);
        Details = Mview.findViewById(R.id.DetsilsID);
        Progressbar = Mview.findViewById(R.id.ProgressbarID);


        WhiteOval = Mview.findViewById(R.id.WhiteButtonID);
        BlackOval = Mview.findViewById(R.id.BlackButtonID);
        RedOval = Mview.findViewById(R.id.RedButttonID);
        GreenOval = Mview.findViewById(R.id.GreenButtonID);
        OrangeOval = Mview.findViewById(R.id.GreenButtonID);
        TealOval = Mview.findViewById(R.id.TealButtonID);


        WhiteDone = Mview.findViewById(R.id.WhiteButtonIconID);
        BlackDone = Mview.findViewById(R.id.BlackImageButtonID);
        RedDone = Mview.findViewById(R.id.RedImageButtonID);
        GreenDone = Mview.findViewById(R.id.GreenImageButtonID);
        OrangeDone = Mview.findViewById(R.id.OrangeImageButtonID);
        TealDone = Mview.findViewById(R.id.TealImageButtonID);


        //todo control themes-------------------------------
        WhiteOval.setOnClickListener(v -> {
            WhiteDone.setVisibility(View.VISIBLE);
            BlackDone.setVisibility(View.GONE);
            RedDone.setVisibility(View.GONE);
            GreenDone.setVisibility(View.GONE);
            OrangeDone.setVisibility(View.GONE);
            TealDone.setVisibility(View.GONE);
            Themescolor = DataManager.White;
        });

        BlackOval.setOnClickListener(v -> {
            WhiteDone.setVisibility(View.GONE);
            BlackDone.setVisibility(View.VISIBLE);
            RedDone.setVisibility(View.GONE);
            GreenDone.setVisibility(View.GONE);
            OrangeDone.setVisibility(View.GONE);
            TealDone.setVisibility(View.GONE);
            Themescolor = DataManager.Black;
        });

        RedOval.setOnClickListener(v -> {
            WhiteDone.setVisibility(View.GONE);
            BlackDone.setVisibility(View.GONE);
            RedDone.setVisibility(View.VISIBLE);
            GreenDone.setVisibility(View.GONE);
            OrangeDone.setVisibility(View.GONE);
            TealDone.setVisibility(View.GONE);
            Themescolor = DataManager.Red;
        });

        GreenOval.setOnClickListener(v -> {
            WhiteDone.setVisibility(View.GONE);
            BlackDone.setVisibility(View.GONE);
            RedDone.setVisibility(View.GONE);
            GreenDone.setVisibility(View.VISIBLE);
            OrangeDone.setVisibility(View.GONE);
            TealDone.setVisibility(View.GONE);
            Themescolor = DataManager.Green;

        });

        OrangeOval.setOnClickListener(v -> {
            WhiteDone.setVisibility(View.GONE);
            BlackDone.setVisibility(View.GONE);
            RedDone.setVisibility(View.GONE);
            GreenDone.setVisibility(View.GONE);
            OrangeDone.setVisibility(View.VISIBLE);
            TealDone.setVisibility(View.GONE);
            Themescolor = DataManager.Orange;
        });

        TealOval.setOnClickListener(v -> {
            WhiteDone.setVisibility(View.GONE);
            BlackDone.setVisibility(View.GONE);
            RedDone.setVisibility(View.GONE);
            GreenDone.setVisibility(View.GONE);
            OrangeDone.setVisibility(View.GONE);
            TealDone.setVisibility(View.VISIBLE);
            Themescolor = DataManager.Teal;
        });
        //todo control themes-------------------------------


        UploadDataButton = Mview.findViewById(R.id.AddNoteButtonID);
        UploadDataButton.setOnClickListener(v -> uploading_data_server(UID));
        ///todo initialize all button and oval ----------------------------------------------------

        PickupImage.setOnClickListener(v -> opendialoag());


    }


    //todo updatebottomsheed dialoag
    private void opendialoag() {
        MaterialAlertDialogBuilder Mbuilder = new MaterialAlertDialogBuilder(getContext());
        View Mview = LayoutInflater.from(getContext()).inflate(R.layout.pickup_image_layout, null, false);
        Mbuilder.setView(Mview);


        alertDialog = Mbuilder.create();
        alertDialog.show();

        RelativeLayout PhotoButton, CameraButton;
        PhotoButton = Mview.findViewById(R.id.PhotosButtonID);
        CameraButton = Mview.findViewById(R.id.CameraButtonID);


        PhotoButton.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_PICK);
            intent.setType("image/*");
            startActivityForResult(intent, IMAGEUPDATE_CODE);
        });

        CameraButton.setOnClickListener(v -> {

        });
    }
    //todo updatebottomsheed dialoag


    // todo read from servier
    private void reading_data_server(String Link) {
        MNoteData.child(Link)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if (snapshot.exists()) {
                            if (snapshot.hasChild(DataManager.Title)) {
                                TitleText = snapshot.child(DataManager.Title).getValue().toString();
                                Title.setText(TitleText);
                            }

                            if (snapshot.hasChild(DataManager.Details)) {
                                DetailsText = snapshot.child(DataManager.Details).getValue().toString();
                                Details.setText(DetailsText);
                            }

                            if (snapshot.hasChild(DataManager.ImageURI)) {
                                ImageUri = snapshot.child(DataManager.ImageURI).getValue().toString();

                            }

                            if (snapshot.hasChild(DataManager.Type)) {
                                Type = snapshot.child(DataManager.Type).getValue().toString();
                            }

                            if (snapshot.hasChild(DataManager.Themes)) {
                                Themescolor = snapshot.child(DataManager.Themes).getValue().toString();

                                if (Themescolor.equals(DataManager.White)) {
                                    WhiteDone.setVisibility(View.VISIBLE);
                                }
                                if (Themescolor.equals(DataManager.Black)) {
                                    BlackDone.setVisibility(View.VISIBLE);

                                }
                                if (Themescolor.equals(DataManager.Red)) {
                                    RedDone.setVisibility(View.VISIBLE);
                                }
                                if (Themescolor.equals(DataManager.Green)) {
                                    GreenOval.setVisibility(View.VISIBLE);
                                }
                                if (Themescolor.equals(DataManager.Orange)) {
                                    OrangeDone.setVisibility(View.VISIBLE);
                                }
                                if (Themescolor.equals(DataManager.Teal)) {
                                    TealDone.setVisibility(View.VISIBLE);
                                }
                            }

                        } else {
//                            Toast.makeText(getContext(), "something error", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
    }

    private void uploading_data_server(String Link) {

        String GetTitleText = Title.getText().toString().trim();
        String GetDetailsText = Details.getText().toString().trim();

        if (GetTitleText.isEmpty()) {
            Toast.makeText(getContext(), "Title Require", Toast.LENGTH_LONG).show();
        } else if (GetDetailsText.isEmpty()) {
            Toast.makeText(getContext(), "Details Require", Toast.LENGTH_SHORT).show();
        } else {

            Progressbar.setVisibility(View.VISIBLE);
            UploadDataButton.setEnabled(false);

            Long Timestamp = System.currentTimeMillis() / 1000;
            String TimestampString = Timestamp.toString();

            Map<String, Object> PostMap = new HashMap<>();
            PostMap.put(DataManager.Title, GetTitleText);
            PostMap.put(DataManager.Details, GetDetailsText);
            PostMap.put(DataManager.Timestamp, Timestamp);
            PostMap.put(DataManager.Search, TitleText.toLowerCase());
            PostMap.put(DataManager.Order, ~(Integer.parseInt(TimestampString) - 1));

            if (Type.equals(DataManager.Image)) {
                PostMap.put(DataManager.ImageURI, ImageUri);
                PostMap.put(DataManager.Type, DataManager.Image);
            }

            if (Type.equals(DataManager.Text)) {
                PostMap.put(DataManager.Type, DataManager.Text);
            }

            if (Themescolor.equals(DataManager.Default)) {
                PostMap.put(DataManager.Themes, DataManager.Default);
            }

            if (!Themescolor.equals(DataManager.Default)) {
                PostMap.put(DataManager.Themes, Themescolor);
            }

            MNoteData.child(Link)
                    .updateChildren(PostMap)
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            Progressbar.setVisibility(View.GONE);
                            UploadDataButton.setEnabled(true);
                            Toasty.success(getContext(), "Success", Toasty.LENGTH_LONG).show();
                            MDialoagUpdate.dismiss();

                            read_server_data();
                        } else {
                            Toast.makeText(getContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            Progressbar.setVisibility(View.GONE);
                            UploadDataButton.setEnabled(true);
                        }
                    })
                    .addOnFailureListener(e -> {
                        Toast.makeText(getContext(), e.getMessage().toString(), Toast.LENGTH_SHORT).show();
                        Progressbar.setVisibility(View.GONE);
                        UploadDataButton.setEnabled(true);
                    });

        }
    }

    ///todo get data from server and update view ----------------------------
}