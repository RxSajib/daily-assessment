package com.dailyneed.alermmanager.UI.Activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.andrognito.patternlockview.PatternLockView;
import com.andrognito.patternlockview.listener.PatternLockViewListener;
import com.andrognito.patternlockview.utils.PatternLockUtils;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.dailyneed.alermmanager.DataManager.DataManager;
import com.dailyneed.alermmanager.LockScreen.CreateLock;
import com.dailyneed.alermmanager.LockScreen.UnLockPage;
import com.dailyneed.alermmanager.Memory.CashMac;
import com.dailyneed.alermmanager.Memory.CashPatternLock;
import com.dailyneed.alermmanager.R;
import com.dailyneed.alermmanager.UI.HomeMenu.Alearm_Page;
import com.dailyneed.alermmanager.UI.HomeMenu.Calclutor_Page;
import com.dailyneed.alermmanager.UI.HomeMenu.StopWatch_Page;
import com.dailyneed.alermmanager.UI.HomeMenu.TodoList;
import com.dailyneed.alermmanager.databinding.ActivitymainBinding;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.shashank.sony.fancydialoglib.Animation;
import com.shashank.sony.fancydialoglib.FancyAlertDialog;
import com.shashank.sony.fancydialoglib.Icon;
import com.suke.widget.SwitchButton;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private boolean IsOpenAlerm = true, IsOpenStopWatch = true, IsOpenCal = true, IsOpenTodoList = true;
    private String data;
    private String MacAddress;
    private CashMac cashMac;
    private CashPatternLock cashPatternLock;
    private android.view.animation.Animation fadin_animaction;
    private boolean islock;
    private ActivitymainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activitymain);




        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        cashPatternLock = new CashPatternLock(getApplicationContext());

        ststusbarcolor();
        inislize_view();
        data = getIntent().getStringExtra(DataManager.LoginData);

        if (data != null) {
            if (data.equals("Login")) {
                goto_todolist(new TodoList());

            }
        }


        goto_alerm_page(new Alearm_Page());
        locationtracker();

    }

    private void ststusbarcolor(){

        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.background));
        } else {
            getWindow().setStatusBarColor(getResources().getColor(R.color.background));
        }
    }

    private void inislize_view() {
        fadin_animaction = AnimationUtils.loadAnimation(MainActivity.this, R.anim.fadin_animaction);
        binding.LockSwitch.setChecked(true);

        binding.LockSwitch.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean isChecked) {

                open_dialoag(isChecked, binding.LockSwitch);
            }
        });


        boolean ispassword = cashPatternLock.getpasswordkey(DataManager.Passwordkey);
        String passwrod = cashPatternLock.getpassword(DataManager.Password);



        if(passwrod == null){
            binding.LockSwitch.setVisibility(View.GONE);
            binding.LockButtonID.setVisibility(View.VISIBLE);
            binding.LockButtonID.setOnClickListener(v -> {
                Intent intent = new Intent(getApplicationContext(), CreateLock.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            });
        }
        else {
            binding.LockSwitch.setVisibility(View.VISIBLE);
            binding.LockButtonID.setImageResource(R.drawable.ic_baseline_lock_24);

        }

        set_cheackradiobutton();




        cashMac = new CashMac(getApplicationContext());


        binding.AlermButtonID.setOnClickListener(v -> {

            if (IsOpenAlerm) {
                IsOpenAlerm = false;
                IsOpenStopWatch = true;
                IsOpenCal = true;
                IsOpenTodoList = true;
                goto_alerm_page(new Alearm_Page());
            }


        });


        binding.CalclutorButtonID.setOnClickListener(v -> {

            if (IsOpenCal) {
                IsOpenCal = false;
                IsOpenAlerm = true;
                IsOpenStopWatch = true;
                IsOpenTodoList = true;
                goto_calclutor_page(new Calclutor_Page());
            }

        });

        binding.TodoListButtonID.setOnClickListener(v -> {
            if (IsOpenTodoList) {

                boolean ispassword1 = cashPatternLock.getpasswordkey(DataManager.Passwordkey);
                Log.d("Setpassword", String.valueOf(ispassword1));
                if (ispassword1) {
                    goto_lockpage();
                } else {
                    goto_todolist(new TodoList());
                }

                goto_todolist(new TodoList());


                IsOpenTodoList = false;
                IsOpenCal = true;
                IsOpenAlerm = true;
                IsOpenStopWatch = true;
            }

        });


        binding.StopWatchID.setOnClickListener(v -> {

            if (IsOpenStopWatch) {
                IsOpenStopWatch = false;
                IsOpenAlerm = true;
                IsOpenCal = true;
                IsOpenTodoList = true;
                goto_stopwatch(new StopWatch_Page());
            }

        });
    }

    private void goto_lockpage() {
        Intent intent = new Intent(getApplicationContext(), UnLockPage.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        Animatoo.animateSlideLeft(MainActivity.this);
    }

    private void goto_stopwatch(Fragment fragment) {
        if (fragment != null) {
            binding.AlermButtonID.setBackground(getResources().getDrawable(R.drawable.un_background));
            binding.CalclutorButtonID.setBackground(getResources().getDrawable(R.drawable.un_background));
            binding.TodoListButtonID.setBackground(getResources().getDrawable(R.drawable.un_background));
            binding.StopWatchID.setBackground(getResources().getDrawable(R.drawable.click_background));


            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.MainFrame, fragment);
            transaction.commit();

            toolbar_title(DataManager.STOP_WATCH);
        }
    }

    private void goto_alerm_page(Fragment fragment) {

        if (fragment != null) {
            binding.AlermButtonID.setBackground(getResources().getDrawable(R.drawable.click_background));
            binding.CalclutorButtonID.setBackground(getResources().getDrawable(R.drawable.un_background));
            binding.TodoListButtonID.setBackground(getResources().getDrawable(R.drawable.un_background));
            binding.StopWatchID.setBackground(getResources().getDrawable(R.drawable.un_background));
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.MainFrame, fragment);
            transaction.commit();

            toolbar_title(DataManager.ALARM);
        }

    }

    private void goto_calclutor_page(Fragment fragment) {

        if (fragment != null) {
            binding.CalclutorButtonID.setBackground(getResources().getDrawable(R.drawable.click_background));
            binding.AlermButtonID.setBackground(getResources().getDrawable(R.drawable.un_background));
            binding.TodoListButtonID.setBackground(getResources().getDrawable(R.drawable.un_background));
            binding.StopWatchID.setBackground(getResources().getDrawable(R.drawable.un_background));
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.MainFrame, fragment);
            transaction.commit();

            toolbar_title(DataManager.CALCULATOR);
        }

    }

    private void goto_todolist(Fragment fragment) {
        if (fragment != null) {
            binding.TodoListButtonID.setBackground(getResources().getDrawable(R.drawable.click_background));
            binding.AlermButtonID.setBackground(getResources().getDrawable(R.drawable.un_background));
            binding.CalclutorButtonID.setBackground(getResources().getDrawable(R.drawable.un_background));
            binding.StopWatchID.setBackground(getResources().getDrawable(R.drawable.un_background));
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.MainFrame, fragment);
            transaction.commit();

            toolbar_title(DataManager.TODO_LIST);
        }
    }

    @Override
    public void onBackPressed() {
        new FancyAlertDialog.Builder(this)
                .setTitle(String.valueOf(DataManager.Exit_title)) //Confirm Exit!
                .setMessage(DataManager.Exit_details)
                .setAnimation(Animation.SLIDE)
                .isCancellable(true)
                .setIcon(R.drawable.ic_hand, Icon.Visible)
                .setNegativeBtnText("Exit")
                .setPositiveBtnText("Stay")

                .isCancellable(true)
                .setBackgroundColor(getResources().getColor(R.color.carbon_teal_400))
                .OnPositiveClicked(() -> {

                })
                .OnNegativeClicked(() -> {
                    finish();
                    // Animatoo.animateSlideLeft(MainActivity.this);
                })
                .build();
    }

    private void locationtracker() {
        MacAddress = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        cashMac.putMacAddress(DataManager.MacKey, MacAddress);
    }




    private void open_dialoag(Boolean ischeack, SwitchButton switchButton) {
        MaterialAlertDialogBuilder Mbuilder = new MaterialAlertDialogBuilder(MainActivity.this, R.style.PauseDialog);
        View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.lockscreen_dialoag, null, false);

        Mbuilder.setView(view);

        AlertDialog alertDialog = Mbuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();

        alertDialog.setOnCancelListener(dialog -> set_cheackradiobutton());


        carbon.widget.LinearLayout LockUnlockButton = view.findViewById(R.id.Button);
        PatternLockView patternLockView = view.findViewById(R.id.PatternLockView);
        TextView message = view.findViewById(R.id.Text);
        ImageView closebutton = view.findViewById(R.id.CloseButton);
        TextView button_text = view.findViewById(R.id.ButtonText);

        closebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();

                set_cheackradiobutton();
            }
        });


        boolean passwordkey = cashPatternLock.getpasswordkey(DataManager.Passwordkey);
        String password = cashPatternLock.getpassword(DataManager.Password);
        if (!passwordkey) {
            button_text.setText("Lock");
        } else {
            button_text.setText("UnLock");
        }


        patternLockView.addPatternLockListener(new PatternLockViewListener() {
            @Override
            public void onStarted() {

            }

            @Override
            public void onProgress(List<PatternLockView.Dot> progressPattern) {

            }

            @Override
            public void onComplete(List<PatternLockView.Dot> pattern) {
                LockUnlockButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String buttontext = button_text.getText().toString();
                        if (!passwordkey) {
                            String code = PatternLockUtils.patternToString(patternLockView, pattern);
                            if (code.isEmpty()) {
                                message.startAnimation(fadin_animaction);
                                fadin_animaction.setAnimationListener(new android.view.animation.Animation.AnimationListener() {
                                    @Override
                                    public void onAnimationStart(android.view.animation.Animation animation) {
                                        message.setText("Please enter your valid pattern");
                                        message.setTextColor(getResources().getColor(R.color.carbon_red_400));
                                    }

                                    @Override
                                    public void onAnimationEnd(android.view.animation.Animation animation) {

                                    }

                                    @Override
                                    public void onAnimationRepeat(android.view.animation.Animation animation) {

                                    }
                                });
                            } else {
                                if (code.equals(password)) {
                                    Log.d("password", String.valueOf(password));
                                    message.startAnimation(fadin_animaction);
                                    fadin_animaction.setAnimationListener(new android.view.animation.Animation.AnimationListener() {
                                        @Override
                                        public void onAnimationStart(android.view.animation.Animation animation) {
                                            cashPatternLock.passwordkey(DataManager.Passwordkey, true);
                                            message.setText("Lock Success!!!");
                                            set_cheackradiobutton();
                                        }

                                        @Override
                                        public void onAnimationEnd(android.view.animation.Animation animation) {

                                        }

                                        @Override
                                        public void onAnimationRepeat(android.view.animation.Animation animation) {

                                        }
                                    });


                                } else {
                                    message.startAnimation(fadin_animaction);
                                    fadin_animaction.setAnimationListener(new android.view.animation.Animation.AnimationListener() {
                                        @Override
                                        public void onAnimationStart(android.view.animation.Animation animation) {
                                            message.setText("This pattern is incorrect please try again!");
                                            message.setTextColor(getResources().getColor(R.color.carbon_red_400));
                                        }

                                        @Override
                                        public void onAnimationEnd(android.view.animation.Animation animation) {

                                        }

                                        @Override
                                        public void onAnimationRepeat(android.view.animation.Animation animation) {

                                        }
                                    });


                                }
                            }
                        }
                        if (passwordkey) {

                            String code = PatternLockUtils.patternToString(patternLockView, pattern);
                            if (code.isEmpty()) {
                                message.startAnimation(fadin_animaction);
                                fadin_animaction.setAnimationListener(new android.view.animation.Animation.AnimationListener() {
                                    @Override
                                    public void onAnimationStart(android.view.animation.Animation animation) {
                                        message.setText("Please enter your valid pattern");
                                        message.setTextColor(getResources().getColor(R.color.carbon_red_400));
                                    }

                                    @Override
                                    public void onAnimationEnd(android.view.animation.Animation animation) {

                                    }

                                    @Override
                                    public void onAnimationRepeat(android.view.animation.Animation animation) {

                                    }
                                });
                            } else {
                                if (code.equals(password)) {
                                    message.startAnimation(fadin_animaction);
                                    fadin_animaction.setAnimationListener(new android.view.animation.Animation.AnimationListener() {
                                        @Override
                                        public void onAnimationStart(android.view.animation.Animation animation) {
                                            cashPatternLock.passwordkey(DataManager.Passwordkey, false);
                                            //   Toast.makeText(MainActivity.this, "UnLock Success!!!", Toast.LENGTH_SHORT).show();
                                            message.setText("UnLock Success!!!");
                                            set_cheackradiobutton();
                                        }

                                        @Override
                                        public void onAnimationEnd(android.view.animation.Animation animation) {

                                        }

                                        @Override
                                        public void onAnimationRepeat(android.view.animation.Animation animation) {

                                        }
                                    });


                                } else {
                                    message.startAnimation(fadin_animaction);
                                    fadin_animaction.setAnimationListener(new android.view.animation.Animation.AnimationListener() {
                                        @Override
                                        public void onAnimationStart(android.view.animation.Animation animation) {
                                            message.setText("This pattern is incorrect please try again!");
                                            message.setTextColor(getResources().getColor(R.color.carbon_red_400));
                                        }

                                        @Override
                                        public void onAnimationEnd(android.view.animation.Animation animation) {

                                        }

                                        @Override
                                        public void onAnimationRepeat(android.view.animation.Animation animation) {

                                        }
                                    });


                                }
                            }


                        }


                    }
                });
            }

            @Override
            public void onCleared() {

            }
        });

    }


    private void set_cheackradiobutton() {
        boolean ispassword = cashPatternLock.getpasswordkey(DataManager.Passwordkey);
        if (ispassword) {
            binding.LockButtonID.setImageResource(R.drawable.ic_lock);
            binding.LockSwitch.setChecked(true);
        } else {
            binding.LockButtonID.setImageResource(R.drawable.ic_unlock);
            binding.LockSwitch.setChecked(false);
        }
    }

    private void toolbar_title(String title){
        binding.ToolbarTitle.startAnimation(fadin_animaction);
        fadin_animaction.setAnimationListener(new android.view.animation.Animation.AnimationListener() {
            @Override
            public void onAnimationStart(android.view.animation.Animation animation) {
                binding.ToolbarTitle.setText(title);
            }

            @Override
            public void onAnimationEnd(android.view.animation.Animation animation) {

            }

            @Override
            public void onAnimationRepeat(android.view.animation.Animation animation) {

            }
        });
    }
}