package com.dailyneed.alermmanager.UI.HomeMenu;

import android.content.Context;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.os.Vibrator;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dailyneed.alermmanager.R;
import com.dailyneed.alermmanager.databinding.CalclutorBinding;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;


public class Calclutor_Page extends Fragment {

    private CalclutorBinding binding;
    public Calclutor_Page() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.calclutor, container, false);
        finding_allview();
        return binding.getRoot();
    }


    private void finding_allview() {

        binding.Power.setOnClickListener(v -> {

            final String[] totaltext = {binding.InputTextID.getText().toString()};

            Vibrator vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(30);

            if (!TextUtils.isEmpty(totaltext[0]))
                totaltext[0] = binding.InputTextID.getText().toString().substring(0, totaltext[0].length() - 1);

            binding.InputTextID.setText(totaltext[0]);
        });

        binding.ModilusID.setOnClickListener(v -> {
            Vibrator vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(30);
            binding.InputTextID.setText(binding.InputTextID.getText() + "%");
        });


        binding.AcButtonID.setOnClickListener(v -> {
            Vibrator vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(30);

            binding.InputTextID.setText("");
            binding.OutputTextID.setText("");
        });


        binding.DivisionID.setOnClickListener(v -> {
            Vibrator vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(30);
            binding.InputTextID.setText(binding.InputTextID.getText() + "/");
        });

        binding.CrossButtonID.setOnClickListener(v -> {
            Vibrator vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(30);
            binding.InputTextID.setText(binding.InputTextID.getText() + "*");
        });

        binding.MulButtonID.setOnClickListener(v -> {
            Vibrator vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(30);
            binding.InputTextID.setText(binding.InputTextID.getText() + "-");
        });


        binding.PlusButtonID.setOnClickListener(v -> {
            Vibrator vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(30);
            binding.InputTextID.setText(binding.InputTextID.getText() + "+");
        });


        binding.EqualButtonID.setOnClickListener(v -> {
            try {
                Vibrator vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(30);
                String Value = binding.InputTextID.getText().toString();
                Double result = null;
                ScriptEngine scriptEngine = new ScriptEngineManager().getEngineByName("rhino");
                try {

                    result = (double) scriptEngine.eval(Value);
                } catch (ScriptException e) {
                    binding.OutputTextID.setText("Error Input");
                    binding.OutputTextID.setTextColor(getResources().getColor(R.color.carbon_red_400));
                }

                if (result != null) {
                    binding.OutputTextID.setText(String.valueOf(result.doubleValue()));
                    binding.OutputTextID.setTextColor(getResources().getColor(R.color.carbon_black_87));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        });


        binding.SevenID.setOnClickListener(v -> {
            Vibrator vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(30);
            binding.InputTextID.setText(binding.InputTextID.getText() + "7");
        });

        binding.EightButttonID.setOnClickListener(v -> {
            Vibrator vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(30);
            binding.InputTextID.setText(binding.InputTextID.getText() + "8");
        });

        binding.NineButtonID.setOnClickListener(v -> {
            Vibrator vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(30);
            binding.InputTextID.setText(binding.InputTextID.getText() + "9");
        });

        binding.FourButtonID.setOnClickListener(v -> {
            Vibrator vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(30);
            binding.InputTextID.setText(binding.InputTextID.getText() + "4");
        });

        binding.FiveButtonID.setOnClickListener(v -> {
            Vibrator vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(30);
            binding.InputTextID.setText(binding.InputTextID.getText() + "5");
        });

        binding.SixButtonID.setOnClickListener(v -> {
            Vibrator vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(30);
            binding.InputTextID.setText(binding.InputTextID.getText() + "6");
        });

        binding.OneButtonID.setOnClickListener(v -> {
            Vibrator vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(30);

            binding.InputTextID.setText(binding.InputTextID.getText() + "1");
        });

        binding.TwoButtonID.setOnClickListener(v -> {
            Vibrator vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(30);

            binding.InputTextID.setText(binding.InputTextID.getText() + "2");
        });

        binding.ThreeButtonID.setOnClickListener(v -> {
            Vibrator vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(30);
            binding.InputTextID.setText(binding.InputTextID.getText() + "3");
        });

        binding.ZeroButtonID.setOnClickListener(v -> {
            Vibrator vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(30);
            binding.InputTextID.setText(binding.InputTextID.getText() + "0");
        });

        binding.DotButtonID.setOnClickListener(v -> {
            Vibrator vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(30);
            binding.InputTextID.setText(binding.InputTextID.getText() + ".");
        });

    }


}