package com.dailyneed.alermmanager.UI.HomeMenu;

import static android.content.Context.ALARM_SERVICE;

import android.Manifest;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.ClipboardManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.URLUtil;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.dailyneed.QuickAlarm.Breakfast;
import com.dailyneed.QuickAlarm.Dinner;
import com.dailyneed.QuickAlarm.Game;
import com.dailyneed.QuickAlarm.Lunch;
import com.dailyneed.QuickAlarm.Work;
import com.dailyneed.Services.MyAlarmServices;
import com.dailyneed.ViewModel.ViewModel;
import com.dailyneed.alermmanager.Adapter.AlarAdapter;
import com.dailyneed.alermmanager.DataManager.DataManager;
import com.dailyneed.alermmanager.BroadcastReceiver.CaptuleAlarm;
import com.dailyneed.alermmanager.BroadcastReceiver.MealAlarm;
import com.dailyneed.alermmanager.BroadcastReceiver.StudyAlarm;
import com.dailyneed.alermmanager.BroadcastReceiver.WorkOut;
import com.dailyneed.alermmanager.Memory.CashAlerm;
import com.dailyneed.alermmanager.Memory.CashMac;
import com.dailyneed.alermmanager.RoomDatabase.AlarmData;
import com.dailyneed.alermmanager.RoomDatabase.AlarmDatabaseRepository;
import com.dailyneed.alermmanager.databinding.AlearmPageBinding;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textview.MaterialTextView;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.dailyneed.alermmanager.BroadcastReceiver.Alerm_Page;
import com.dailyneed.alermmanager.R;
import com.shawnlin.numberpicker.NumberPicker;
import com.tapadoo.alerter.Alerter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;


public class Alearm_Page extends Fragment {

    private static final int PERMISSIONCODE = 10;
    private boolean IsOpen = false;
    private Animation slide_to_right;
    private Animation slide_to_right_leat;
    private Animation slide_to_left;
    private Animation close_slide_to_left;
    private DatabaseReference MAlarmDatabase;
    private CashMac cashMac;
    private int TimeCount = 5;
    private String AlarmType;
    private CashAlerm memory;
    private AlearmPageBinding binding;
    private ViewModel viewModel;
    private List<AlarmData> alarmModelList = new ArrayList<>();
    private AlarAdapter adapter;
    private AlarmDatabaseRepository alarmDatabaseRepository;


    public Alearm_Page() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.alearm__page, container, false);


        inisilize_view();
        getdata_fromserver();
        sweapto_remove();
        return binding.getRoot();
    }

    private void getdata_fromserver() {

        alarmDatabaseRepository = new ViewModelProvider(this).get(AlarmDatabaseRepository.class);
        MAlarmDatabase = FirebaseDatabase.getInstance().getReference().child(DataManager.MyAlarm);
        cashMac = new CashMac(getActivity());
        String macaddress = cashMac.getMacAddress(DataManager.MacKey);

        alarmDatabaseRepository.getAlarmData().observe(getViewLifecycleOwner()
                , new Observer<List<AlarmData>>() {
                    @Override
                    public void onChanged(List<AlarmData> alarmData) {

                        if (alarmData != null) {
                            alarmModelList.clear();
                            alarmModelList.addAll(alarmData);
                            adapter.setAlarmModelList(alarmModelList);
                            adapter.notifyDataSetChanged();
                            binding.MessageID.setVisibility(View.GONE);


                        }
                        if (alarmModelList.size() == 0) {
                            binding.MessageID.setVisibility(View.VISIBLE);
                        }
                    }
                });
    }

    //todo*********************  GET DATA   **************************/

    private void sweapto_remove() {
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT | ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {

                cashMac = new CashMac(getActivity());
                String macaddress = cashMac.getMacAddress(DataManager.MacKey);
                alarmDatabaseRepository.DeleteData(alarmModelList.get(viewHolder.getAdapterPosition()));
                String Type = alarmModelList.get(viewHolder.getAdapterPosition()).getType();

                if (Type.equals(DataManager.Capsules)) {
                    Intent i = new Intent(getActivity(), MyAlarmServices.class);
                    getActivity().stopService(i);
                    AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(ALARM_SERVICE);
                    Intent intent = new Intent(getActivity(), CaptuleAlarm.class);
                    PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), 0, intent, 0);
                    alarmManager.cancel(pendingIntent);
                    Toast.makeText(getActivity(), "Alarm stop "+Type, Toast.LENGTH_SHORT).show();
                }
                if (Type.equals(DataManager.Study)) {
                    Intent i = new Intent(getActivity(), MyAlarmServices.class);
                    getActivity().stopService(i);
                    AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(ALARM_SERVICE);
                    Intent intent = new Intent(getActivity(), StudyAlarm.class);
                    PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), 0, intent, 0);
                    alarmManager.cancel(pendingIntent);
                    Toast.makeText(getActivity(), "Alarm stop "+Type, Toast.LENGTH_SHORT).show();
                }
                if (Type.equals(DataManager.Workout)) {
                    Intent i = new Intent(getActivity(), MyAlarmServices.class);
                    getActivity().stopService(i);
                    AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(ALARM_SERVICE);
                    Intent intent = new Intent(getActivity(), WorkOut.class);
                    PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), 0, intent, 0);
                    alarmManager.cancel(pendingIntent);
                    Toast.makeText(getActivity(), "Alarm stop "+Type, Toast.LENGTH_SHORT).show();
                }
                if (Type.equals(DataManager.Meal)) {
                    Intent i = new Intent(getActivity(), MyAlarmServices.class);
                    getActivity().stopService(i);
                    AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(ALARM_SERVICE);
                    Intent intent = new Intent(getActivity(), MealAlarm.class);
                    PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), 0, intent, 0);
                    alarmManager.cancel(pendingIntent);
                    Toast.makeText(getActivity(), "Alarm stop "+Type, Toast.LENGTH_SHORT).show();
                }
                if (Type.equals(DataManager.Work)) {
                    Intent i = new Intent(getActivity(), MyAlarmServices.class);
                    getActivity().stopService(i);
                    AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(ALARM_SERVICE);
                    Intent intent = new Intent(getActivity(), Work.class);
                    PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), 0, intent, 0);
                    alarmManager.cancel(pendingIntent);
                    Toast.makeText(getActivity(), "Alarm stop "+Type, Toast.LENGTH_SHORT).show();
                }
                if (Type.equals(DataManager.Breakfast)) {
                    Intent i = new Intent(getActivity(), MyAlarmServices.class);
                    getActivity().stopService(i);
                    AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(ALARM_SERVICE);
                    Intent intent = new Intent(getActivity(), Breakfast.class);
                    PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), 0, intent, 0);
                    alarmManager.cancel(pendingIntent);
                    Toast.makeText(getActivity(), "Alarm stop "+Type, Toast.LENGTH_SHORT).show();
                }
                if (Type.equals(DataManager.Lunch)) {
                    Intent i = new Intent(getActivity(), MyAlarmServices.class);
                    getActivity().stopService(i);
                    AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(ALARM_SERVICE);
                    Intent intent = new Intent(getActivity(), Lunch.class);
                    PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), 0, intent, 0);
                    alarmManager.cancel(pendingIntent);
                    Toast.makeText(getActivity(), "Alarm stop "+Type, Toast.LENGTH_SHORT).show();
                }
                if (Type.equals(DataManager.Dinner)) {
                    Intent i = new Intent(getActivity(), MyAlarmServices.class);
                    getActivity().stopService(i);
                    AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(ALARM_SERVICE);
                    Intent intent = new Intent(getActivity(), Dinner.class);
                    PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), 0, intent, 0);
                    alarmManager.cancel(pendingIntent);
                    Toast.makeText(getActivity(), "Alarm stop "+Type, Toast.LENGTH_SHORT).show();
                }
                if (Type.equals(DataManager.Game)) {
                    Intent i = new Intent(getActivity(), MyAlarmServices.class);
                    getActivity().stopService(i);
                    AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(ALARM_SERVICE);
                    Intent intent = new Intent(getActivity(), Game.class);
                    PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), 0, intent, 0);
                    alarmManager.cancel(pendingIntent);
                    Toast.makeText(getActivity(), "Alarm stop "+Type, Toast.LENGTH_SHORT).show();
                }

                alarmModelList.remove(viewHolder.getAdapterPosition());
                adapter.notifyDataSetChanged();


            }
        });
        itemTouchHelper.attachToRecyclerView(binding.RecylerViewID);

    }


    private void inisilize_view() {

        adapter = new AlarAdapter();
        memory = new CashAlerm(getActivity());
        binding.RecylerViewID.setHasFixedSize(true);
        binding.RecylerViewID.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.RecylerViewID.setAdapter(adapter);
        binding.AddButtonID.setOnClickListener(v -> {
            if (!IsOpen) {

                IsOpen = true;
                rotateFabForward();
                Log.d("TAG", "IF");

                slide_to_right_leat = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_to_right_leat);
                binding.AlermTextID.startAnimation(slide_to_right_leat);

                binding.AlermTextID.setVisibility(View.VISIBLE);
                binding.QrCodeScannerTextID.setVisibility(View.VISIBLE);
                binding.AlermButton.setVisibility(View.VISIBLE);
                binding.QrCodeScanner.setVisibility(View.VISIBLE);

                binding.QuickAlarmButton.setVisibility(View.VISIBLE);
                binding.QuickAlarmText.setVisibility(View.VISIBLE);

                slide_to_right = AnimationUtils.loadAnimation(getActivity(), R.anim.slider_to_right);
                binding.QrCodeScannerTextID.startAnimation(slide_to_right);

                binding.QuickAlarmText.startAnimation(slide_to_right);
                slide_to_right.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        binding.QuickAlarmText.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });

                slide_to_right_leat.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        binding.AlermTextID.setVisibility(View.VISIBLE);

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });

                slide_to_right.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        binding.QrCodeScannerTextID.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });


                slide_to_right_leat = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_to_left);
                binding.AlermButton.startAnimation(slide_to_right_leat);
                binding.QrCodeScanner.setAnimation(slide_to_right_leat);
                binding.QuickAlarmButton.startAnimation(slide_to_right_leat);
                slide_to_right_leat.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        binding.QuickAlarmButton.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });

                slide_to_right_leat.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        binding.AlermButton.setVisibility(View.VISIBLE);
                        binding.QrCodeScanner.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });


                binding.AlermButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (IsPermissionCamera()) {
                            goto_alerm_page();
                        }

                    }
                });

                binding.QrCodeScanner.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        scanner();
                    }
                });

                binding.QuickAlarmButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (IsPermissionCamera()) {
                            quick_alarm();
                        }

                    }
                });


            } else {

                Log.d("TAG", "ELSE");
                slide_to_right_leat = AnimationUtils.loadAnimation(getActivity(), R.anim.diable_slide_to_right);
                binding.AlermButton.startAnimation(slide_to_right_leat);
                binding.QuickAlarmButton.startAnimation(slide_to_right_leat);
                binding.QrCodeScanner.startAnimation(slide_to_right_leat);

                slide_to_right_leat.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        binding.QuickAlarmButton.setVisibility(View.GONE);
                        binding.QrCodeScanner.setVisibility(View.GONE);
                        binding.AlermButton.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });


                slide_to_left = AnimationUtils.loadAnimation(getActivity(), R.anim.disiable_slide_toleft);
                binding.QrCodeScannerTextID.startAnimation(slide_to_left);
                binding.AlermTextID.startAnimation(slide_to_left);
                binding.QuickAlarmText.startAnimation(slide_to_left);


                slide_to_left.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        binding.QuickAlarmText.setVisibility(View.GONE);

                        binding.AlermTextID.setVisibility(View.GONE);
                        binding.QrCodeScannerTextID.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                IsOpen = false;
                rotateFabBackward();

            }
        });
    }


    private void goto_alerm_page() {
        Intent intent = new Intent(getContext(), Alerm_Page.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        Animatoo.animateSlideLeft(getActivity());
    }

    public void rotateFabForward() {
        ViewCompat.animate(binding.AddButtonID)
                .rotation(45F)
                .withLayer()
                .setDuration(200L)

                .start();
    }

    public void rotateFabBackward() {
        ViewCompat.animate(binding.AddButtonID)
                .rotation(0.0F)
                .withLayer()
                .setDuration(200L)
                .start();
    }

    private void scanner() {
        IntentIntegrator.forSupportFragment(this).initiateScan();

        IntentIntegrator.forSupportFragment(this)
                .setDesiredBarcodeFormats(IntentIntegrator.QR_CODE)
                .setBeepEnabled(false)
                .setPrompt("Scanning Code")
                .initiateScan();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() != null) {

                boolean isValid = URLUtil.isValidUrl(result.getContents());

                BottomSheetDialog Mbuilder = new BottomSheetDialog(getContext(), R.style.CustomBottomSheetDialogTheme);
                View view = LayoutInflater.from(getActivity()).inflate(R.layout.bottom_sheed_dialoag_qrcode, null, false);
                Mbuilder.setContentView(view);

                Mbuilder.show();
                init_qrcode_dialoag(view, result.getContents());


            } else {
                Toast.makeText(getContext(), "error try again scan", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(getContext(), "error try again scan", Toast.LENGTH_LONG).show();
        }
    }

    private void remove_alerm(String UID) {
        MaterialAlertDialogBuilder Mbuilder = new MaterialAlertDialogBuilder(getActivity());
        Mbuilder.setTitle("Do you want to remove?");
        Mbuilder.setMessage("If you remove the alarm press continue");
        Mbuilder.setPositiveButton("Continue", (dialog, which) -> MAlarmDatabase.child(UID).removeValue()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(getActivity(), "Alarm Remove Success", Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        } else {
                            Toast.makeText(getActivity(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }).addOnFailureListener(e -> Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show()));
        Mbuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });


        androidx.appcompat.app.AlertDialog alertDialog = Mbuilder.create();
        alertDialog.show();

    }

    private boolean IsPermissionCamera() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, PERMISSIONCODE);
            return false;

        }
    }


    private void quick_alarm() {

        MaterialAlertDialogBuilder Mbuilder = new MaterialAlertDialogBuilder(getActivity(), R.style.PauseDialog);
        View Mview = LayoutInflater.from(getActivity()).inflate(R.layout.quick_alarm_dialoag, null, false);
        Mbuilder.setView(Mview);


        AlertDialog alertDialog = Mbuilder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.show();

        NumberPicker numberPicker = Mview.findViewById(R.id.NumberPickerID);
        TextView timecounter = Mview.findViewById(R.id.TimeCounterText);
        Spinner typespinner = Mview.findViewById(R.id.TypeSpinner);

        numberPicker.setMaxValue(59);
        numberPicker.setMinValue(5);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.quick_alarm_spinner_layout, R.id.TypeText, getResources().getStringArray(R.array.typeof_quickalarm));
        typespinner.setAdapter(adapter);

        typespinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                AlarmType = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        numberPicker.setOnValueChangedListener((picker, oldVal, newVal) -> {
            Log.d("TAG", String.format(Locale.US, "oldVal: %d, newVal: %d", oldVal, newVal));
            timecounter.setText(String.valueOf(newVal) + " Min left");
            TimeCount = newVal;
        });

        carbon.widget.LinearLayout selectbtn = Mview.findViewById(R.id.SelectButtonID);
        selectbtn.setOnClickListener(view -> save_quickalarm(alertDialog, view, TimeCount, AlarmType));


    }



    private void save_quickalarm(AlertDialog alertDialog, View view, int TimeCount, String Type) {
        if (Type.equals("Alarm Type")) {
            Toast.makeText(getActivity(), "Select alarm type", Toast.LENGTH_SHORT).show();
        } else {

            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MINUTE, TimeCount);
            long time = calendar.getTimeInMillis();
            long TimeConvertCurrentTime = time / 1000;

            cashMac = new CashMac(getActivity());
            AlarmData alarmData = new AlarmData(Type, time, null, Type);


            alarmDatabaseRepository.DeleteDataByID(Type);
            alarmDatabaseRepository.SetAlarmData(alarmData);


            if (Type.equals(DataManager.Work)) {
                memory.PutTime(DataManager.Work, String.valueOf(TimeConvertCurrentTime * 1000));
                AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(ALARM_SERVICE);
                Intent intent = new Intent(getActivity(), Work.class);

                PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), 0, intent, 0);
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, time, alarmManager.INTERVAL_DAY, pendingIntent);

                Intent alarmChanged = new Intent("android.intent.action.ALARM_CHANGED");
                alarmChanged.putExtra("alarmSet", true);
                getActivity().sendBroadcast(alarmChanged);
                memory.PutTime(DataManager.Work, String.valueOf(time));
            }

            if (Type.equals(DataManager.Breakfast)) {
                memory.PutTime(DataManager.Work, String.valueOf(TimeConvertCurrentTime * 1000));
                AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(ALARM_SERVICE);
                Intent intent = new Intent(getActivity(), Breakfast.class);

                PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), 0, intent, 0);
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, time, alarmManager.INTERVAL_DAY, pendingIntent);

                Intent alarmChanged = new Intent("android.intent.action.ALARM_CHANGED");
                alarmChanged.putExtra("alarmSet", true);
                getActivity().sendBroadcast(alarmChanged);
            }

            if (Type.equals(DataManager.Lunch)) {
                memory.PutTime(DataManager.Work, String.valueOf(TimeConvertCurrentTime * 1000));
                AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(ALARM_SERVICE);
                Intent intent = new Intent(getActivity(), Lunch.class);

                PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), 0, intent, 0);
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, time, alarmManager.INTERVAL_DAY, pendingIntent);

                Intent alarmChanged = new Intent("android.intent.action.ALARM_CHANGED");
                alarmChanged.putExtra("alarmSet", true);
                getActivity().sendBroadcast(alarmChanged);
            }

            if (Type.equals(DataManager.Dinner)) {
                memory.PutTime(DataManager.Work, String.valueOf(TimeConvertCurrentTime * 1000));
                AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(ALARM_SERVICE);
                Intent intent = new Intent(getActivity(), Dinner.class);

                PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), 0, intent, 0);
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, time, alarmManager.INTERVAL_DAY, pendingIntent);

                Intent alarmChanged = new Intent("android.intent.action.ALARM_CHANGED");
                alarmChanged.putExtra("alarmSet", true);
                getActivity().sendBroadcast(alarmChanged);
            }

            if (Type.equals(DataManager.Game)) {
                memory.PutTime(DataManager.Work, String.valueOf(TimeConvertCurrentTime * 1000));
                AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(ALARM_SERVICE);
                Intent intent = new Intent(getActivity(), Game.class);

                PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), 0, intent, 0);
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, time, alarmManager.INTERVAL_DAY, pendingIntent);

                Intent alarmChanged = new Intent("android.intent.action.ALARM_CHANGED");
                alarmChanged.putExtra("alarmSet", true);
                getActivity().sendBroadcast(alarmChanged);
            }

            alertDialog.dismiss();
        }
    }

    private void init_qrcode_dialoag(View view, String ScanResult) {

        MaterialTextView ScanText;

        String Result;
        LinearLayout CopyBtn;
        LinearLayout WebBtn;

        ScanText = view.findViewById(R.id.QrCodeScanTextID);
        CopyBtn = view.findViewById(R.id.CopyButtonID);
        WebBtn = view.findViewById(R.id.WebButtonID);


        if (ScanResult != null) {

            boolean isValid = URLUtil.isValidUrl(ScanResult);
            if (isValid) {
                WebBtn.setVisibility(View.VISIBLE);

                WebBtn.setOnClickListener(v -> {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(ScanResult));
                    startActivity(i);
                });

                CopyBtn.setOnClickListener(v -> {
                    ClipboardManager clipboardManager = (ClipboardManager) (getActivity()).getSystemService(Context.CLIPBOARD_SERVICE);
                    clipboardManager.setText(ScanResult);

                    Alerter.create(getActivity())
                            .setTitle("Copy Success")
                            .setText(ScanResult)
                            .show();
                });

            } else {
                WebBtn.setVisibility(View.GONE);

                CopyBtn.setOnClickListener(v -> {

                    ClipboardManager clipboardManager = (ClipboardManager) (getActivity()).getSystemService(Context.CLIPBOARD_SERVICE);
                    clipboardManager.setText(ScanResult);

                    Alerter.create(getActivity())
                            .setTitle("Copy Success")
                            .setText(ScanResult)
                            .show();

                });
            }

            ScanText.setText(ScanResult);
        } else {
            ScanText.setText("Error scan the qr code or bar code try again !");
        }
    }

}