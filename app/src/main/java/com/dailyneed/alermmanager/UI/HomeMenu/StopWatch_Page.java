package com.dailyneed.alermmanager.UI.HomeMenu;

import android.content.Context;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.os.SystemClock;
import android.os.Vibrator;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.dailyneed.alermmanager.databinding.StopWatchPageBinding;
import com.dailyneed.alermmanager.DataManager.DataManager;
import com.dailyneed.alermmanager.R;

import java.util.Calendar;
import java.util.Locale;

import static android.view.View.GONE;


public class StopWatch_Page extends Fragment {

    private String WorkingState = "";
    private Handler mHandler = new Handler();
    private boolean CurrentState = true;
    private Thread t;
    private long pushoffset;
    private int count = 0;
    private Animation animation, nullanim;
    private StopWatchPageBinding binding;

    public StopWatch_Page() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.stop_watch__page, container, false);
        initview();
        return binding.getRoot();
    }


    private void initview() {
        animation = AnimationUtils.loadAnimation(getActivity(), R.anim.fadin_outani);
        nullanim = AnimationUtils.loadAnimation(getActivity(), R.anim.null_anim);

        binding.StartButtonID.startAnimation(animation);


        if (WorkingState.equals("")) {

            binding.StartButtonID.setOnClickListener(v -> {
                binding.Progressbar.setVisibility(View.VISIBLE);
                Vibrator vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(30);

                binding.Timer.setBase(SystemClock.elapsedRealtime() - pushoffset);
                binding.Timer.start();

                binding.StartButtonID.setVisibility(GONE);
                binding.StartButtonID.startAnimation(nullanim);
                binding.ResetButtonID.setVisibility(View.VISIBLE);
                binding.ResumButtonID.setVisibility(View.VISIBLE);

                binding.LapTextID.setText("Lap");
                binding.PushTextID.setText("Pause");

                WorkingState = "start";

            });

        }

            binding.ResumButtonID.setOnClickListener(v -> {

                if(WorkingState.equals("start")){
                    binding.Progressbar.setVisibility(GONE);
                    Vibrator vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
                    vibrator.vibrate(30);
                    pushoffset = SystemClock.elapsedRealtime() - binding.Timer.getBase();
                    binding.Timer.stop();

                    WorkingState = "starting";
                    binding.LapTextID.setText("Reset");
                    binding.PushTextID.setText("Resume");

                }
                else if(WorkingState.equals("starting")){
                    binding.Progressbar.setVisibility(View.VISIBLE);
                    Vibrator vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
                    vibrator.vibrate(30);
                    binding.Timer.setBase(SystemClock.elapsedRealtime() - pushoffset);
                    binding.Timer.start();

                }

            });


            binding.ResetButtonID.setOnClickListener(v -> {
                if(WorkingState.equals("start")){
                    Vibrator vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
                    vibrator.vibrate(30);

                    long Timestamp = System.currentTimeMillis() / 1000;
                    Calendar calendar = Calendar.getInstance(Locale.ENGLISH);
                    calendar.setTimeInMillis(Timestamp * 1000);

                    String Time = DateFormat.format(DataManager.TimeFormatWithAMPM, calendar).toString();

                    count ++;
                    binding.LapTimeID.append(binding.Timer.getText().toString()+"\n");
                    binding.CounterLap.append(String.valueOf(count)+"\n");
                    binding.TimeDate.append(Time+"\n");

                }

               else if(WorkingState.equals("starting")){
                    binding.Timer.setBase(SystemClock.elapsedRealtime());
                    pushoffset = 0;

                    Vibrator vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
                    vibrator.vibrate(30);

                    binding.ResetButtonID.setVisibility(View.GONE);
                    binding.ResumButtonID.setVisibility(View.GONE);
                    binding.StartButtonID.setVisibility(View.VISIBLE);
                    WorkingState = "";
                }

            });



    }

}