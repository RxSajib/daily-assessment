package com.dailyneed.alermmanager.UI.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.dailyneed.alermmanager.databinding.RegisterBinding;
import com.google.firebase.auth.FirebaseAuth;
import com.dailyneed.alermmanager.R;

import dmax.dialog.SpotsDialog;
import es.dmoral.toasty.Toasty;

public class FreeRegi extends AppCompatActivity {

    private FirebaseAuth Mauth;
    private AlertDialog spotsDialog;
    private RegisterBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.register);

        statusbarcolor();
        finding_view();
    }

    private void statusbarcolor(){
        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.M){
            getWindow().setStatusBarColor(getResources().getColor(R.color.login_background));
        }
        else {
            getWindow().setStatusBarColor(getResources().getColor(R.color.login_background));
        };
    }
    void finding_view(){
        Mauth = FirebaseAuth.getInstance();
        binding.backButtonID.setOnClickListener(v -> {
            finish();
            Animatoo.animateSlideRight(FreeRegi.this);
        });
        binding.RegisterButtonID.setOnClickListener(v -> register_account());
    }

    private void register_account(){
        String EmailText = binding.EmailInputID.getText().toString().trim();
        String PasswordText = binding.PasswordInputID.getText().toString().trim();
        String RetypePasswordText = binding.RetypePasswordID.getText().toString().trim();
        if(EmailText.isEmpty()){
            Toast.makeText(getApplicationContext(), "Email Require", Toast.LENGTH_SHORT).show();
        }
        else if(PasswordText.isEmpty()){
            Toast.makeText(getApplicationContext(), "Password Require", Toast.LENGTH_SHORT).show();
        }
        else if(RetypePasswordText.isEmpty()){
            Toast.makeText(getApplicationContext(), "Confirm Password Require", Toast.LENGTH_SHORT).show();
        }

        else if(!PasswordText.equals(RetypePasswordText)){
            binding.PasswordCharCheackID.setText("Password not same");
            binding.PasswordCharCheackID.setTextColor(getResources().getColor(R.color.carbon_red_400));
        }
        else if(PasswordText.length() <= 7){
            binding.PasswordCharCheackID.setText("Password need 8 char must");
            binding.PasswordCharCheackID.setTextColor(getResources().getColor(R.color.carbon_red_400));
        }

        else {
            spotsDialog = new SpotsDialog.Builder()
                    .setContext(this)
                    .setMessage("Register")
                    .build();
            spotsDialog.show();

            binding.PasswordCharCheackID.setText("All is ok");
            binding.PasswordCharCheackID.setTextColor(getResources().getColor(R.color.carbon_black_38));
            Mauth.createUserWithEmailAndPassword(EmailText, PasswordText)
                    .addOnCompleteListener(task -> {
                        if(task.isSuccessful()){
                            spotsDialog.dismiss();
                            gotomain_page();
                        }
                        else {
                            spotsDialog.dismiss();
                            Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnFailureListener(e -> {
                     spotsDialog.dismiss();
                        Toast.makeText(getApplicationContext(), e.getMessage().toString(), Toast.LENGTH_LONG).show();
                    });
        }
    }


    private void gotomain_page(){
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();

        Toasty.success(getApplicationContext(), "Register Success", Toasty.LENGTH_LONG).show();
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(FreeRegi.this);
    }
}