package com.dailyneed.alermmanager.UI.BottomSheed;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;

import com.anstrontechnologies.corehelper.AnstronCoreHelper;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textview.MaterialTextView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.iceteck.silicompressorr.FileUtils;
import com.iceteck.silicompressorr.SiliCompressor;
import com.dailyneed.alermmanager.DataManager.DataManager;
import com.dailyneed.alermmanager.R;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.app.Activity.RESULT_OK;

public class TodoBottomSheed extends BottomSheetDialogFragment {

    private RelativeLayout Crossbutton;
    private RelativeLayout PickUpImageButton;
    private static final int IMAGECODE = 100;
    private static final int REQUESTCODE = 100;

    private EditText Title, Details;
    private RelativeLayout AddnoteButton;

    private ImageView WhiteIcon, BlackIcon, RedIcon, GreenIcon, OrangeIcon, TealIcon;
    private RelativeLayout WhiteOval, BlackOval, RedOval, GreenOval, OrangeOval, TealOval;
    private String Color = "";

    private AlertDialog alertDialog;

    //todo image uploading task-----------------
    private ProgressBar ImageUploadProgress;
    private LinearLayout ImageUploadingMessageBox;
    private ImageView ImageUploadStatus;
    private MaterialTextView ImageUploadStatusText;
    //todo image uploading task-----------------

    private FirebaseAuth Mauth;
    private String CurrentUserID;
    private StorageReference MImageStores;
    private com.anstrontechnologies.corehelper.AnstronCoreHelper anstronCoreHelper;
    private String ImageDownloadURI = "";

    private DatabaseReference MNoteData;

    private RelativeLayout Progressbar;

    private BottomSheedLisiner MLisiner;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.todolist_bottomsheed, container, false);


        finding_all_view(view);

        return view;
    }

    private void finding_all_view(View view) {

        /// todo convert image by AnstronCoreHelper -----------------------------------
        anstronCoreHelper = new AnstronCoreHelper(getActivity());
        /// todo convert image by AnstronCoreHelper -----------------------------------

        Progressbar = view.findViewById(R.id.ProgressbarID);

        Title = view.findViewById(R.id.TitleID);
        Details = view.findViewById(R.id.DetsilsID);
        AddnoteButton = view.findViewById(R.id.AddNoteButtonID);

        //todo Firebase Utils -------------------------------------------
        Mauth = FirebaseAuth.getInstance();
        CurrentUserID = Mauth.getCurrentUser().getUid();
        MImageStores = FirebaseStorage.getInstance().getReference().child(DataManager.TodoListImageRoot);
        MNoteData = FirebaseDatabase.getInstance().getReference().child(DataManager.NoteDatabase);
        MNoteData.keepSynced(true);
        //todo Firebase Utils -------------------------------------------

        //todo finding all done image and oval button------------------------------------------------------
        WhiteOval = view.findViewById(R.id.WhiteButtonID);
        BlackOval = view.findViewById(R.id.BlackButtonID);
        RedOval = view.findViewById(R.id.RedButttonID);
        GreenOval = view.findViewById(R.id.GreenButtonID);
        OrangeOval = view.findViewById(R.id.OrangeButtonID);
        TealOval = view.findViewById(R.id.TealButtonID);

        WhiteIcon = view.findViewById(R.id.WhiteButtonIconID);
        BlackIcon = view.findViewById(R.id.BlackImageButtonID);
        RedIcon = view.findViewById(R.id.RedImageButtonID);
        GreenIcon = view.findViewById(R.id.GreenImageButtonID);
        OrangeIcon = view.findViewById(R.id.OrangeImageButtonID);
        TealIcon = view.findViewById(R.id.TealImageButtonID);
        //todo finding all done image and oval button------------------------------------------------------


        //todo control all oval button -------------------------------------------------------
        WhiteOval.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WhiteIcon.setVisibility(View.VISIBLE);
                BlackIcon.setVisibility(View.GONE);
                RedIcon.setVisibility(View.GONE);
                GreenIcon.setVisibility(View.GONE);
                OrangeIcon.setVisibility(View.GONE);
                TealIcon.setVisibility(View.GONE);

                Color = DataManager.White;
            }
        });


        BlackOval.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WhiteIcon.setVisibility(View.GONE);
                BlackIcon.setVisibility(View.VISIBLE);
                RedIcon.setVisibility(View.GONE);
                GreenIcon.setVisibility(View.GONE);
                OrangeIcon.setVisibility(View.GONE);
                TealIcon.setVisibility(View.GONE);

                Color = DataManager.Black;
            }
        });

        RedOval.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WhiteIcon.setVisibility(View.GONE);
                BlackIcon.setVisibility(View.GONE);
                RedIcon.setVisibility(View.VISIBLE);
                GreenIcon.setVisibility(View.GONE);
                OrangeIcon.setVisibility(View.GONE);
                TealIcon.setVisibility(View.GONE);

                Color = DataManager.Red;
            }
        });

        GreenOval.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WhiteIcon.setVisibility(View.GONE);
                BlackIcon.setVisibility(View.GONE);
                RedIcon.setVisibility(View.GONE);
                GreenIcon.setVisibility(View.VISIBLE);
                OrangeIcon.setVisibility(View.GONE);
                TealIcon.setVisibility(View.GONE);

                Color = DataManager.Green;
            }
        });

        OrangeOval.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WhiteIcon.setVisibility(View.GONE);
                BlackIcon.setVisibility(View.GONE);
                RedIcon.setVisibility(View.GONE);
                GreenIcon.setVisibility(View.GONE);
                OrangeIcon.setVisibility(View.VISIBLE);
                TealIcon.setVisibility(View.GONE);

                Color = DataManager.Orange;
            }
        });

        TealOval.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WhiteIcon.setVisibility(View.GONE);
                BlackIcon.setVisibility(View.GONE);
                RedIcon.setVisibility(View.GONE);
                GreenIcon.setVisibility(View.GONE);
                OrangeIcon.setVisibility(View.GONE);
                TealIcon.setVisibility(View.VISIBLE);

                Color = DataManager.Teal;
            }
        });
        //todo control all oval button -------------------------------------------------------


        //todo image uploading task------------------------------------
        ImageUploadProgress = view.findViewById(R.id.ImageProgressbarID);
        ImageUploadingMessageBox = view.findViewById(R.id.ImageUploadingMessage);
        ImageUploadStatus = view.findViewById(R.id.ImageUploadStatus);
        ImageUploadStatusText = view.findViewById(R.id.ImageUploadStatusText);
        //todo image uploading task------------------------------------


        AddnoteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // posting_data();
                MLisiner.refreshList("click one");
            }
        });

        Crossbutton = view.findViewById(R.id.CrossButtonID);
        Crossbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        PickUpImageButton = view.findViewById(R.id.PicUpImageID);
        PickUpImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MaterialAlertDialogBuilder Mbuilder = new MaterialAlertDialogBuilder(getContext());

                View Mview = LayoutInflater.from(getContext()).inflate(R.layout.pickup_image_layout, null, false);
                Mbuilder.setView(Mview);


                alertDialog = Mbuilder.create();
                alertDialog.show();

                RelativeLayout CameraButton = Mview.findViewById(R.id.CameraButtonID);
                RelativeLayout PhotosButton = Mview.findViewById(R.id.PhotosButtonID);


                CameraButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });

                PhotosButton.setOnClickListener(new View.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onClick(View v) {
                        if (RequestExternal_Stroge()) {
                            Intent intent = new Intent(Intent.ACTION_PICK);
                            intent.setType("image/*");
                            startActivityForResult(intent, IMAGECODE);
                        }
                    }
                });
            }
        });
    }


    ///todo image permission ----------------------------------------------------------------
    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean RequestExternal_Stroge() {
        if (ActivityCompat.checkSelfPermission(getActivity(), READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            getActivity().requestPermissions(new String[]{READ_EXTERNAL_STORAGE}, REQUESTCODE);
            return false;
        }
    }
    ///todo image permission ----------------------------------------------------------------

    /// todo start posting to server ------------------------------------------------------
    private void posting_data() {



        String TitleText = Title.getText().toString().trim();
        String DetailsText = Details.getText().toString().trim();

        if (TitleText.isEmpty()) {
            Toast.makeText(getActivity(), "Title Require", Toast.LENGTH_SHORT).show();
        } else if (DetailsText.isEmpty()) {
            Toast.makeText(getActivity(), "Details Require", Toast.LENGTH_SHORT).show();
        } else {

            Progressbar.setVisibility(View.VISIBLE);
            AddnoteButton.setEnabled(false);

            Long Timestamplong = System.currentTimeMillis() / 1000;
            String TimestampString = Timestamplong.toString();

            Map<String, Object> PostMap = new HashMap<String, Object>();
            PostMap.put(DataManager.Title, TitleText);
            PostMap.put(DataManager.Details, DetailsText);
            PostMap.put(DataManager.Timestamp, Timestamplong);
            PostMap.put(DataManager.Timestamp, Timestamplong);
            PostMap.put(DataManager.Search, DetailsText.toLowerCase());
            PostMap.put(DataManager.Order, ~(Integer.parseInt(TimestampString) - 1));

            if(ImageDownloadURI != null){
                PostMap.put(DataManager.ImageURI, ImageDownloadURI);
                PostMap.put(DataManager.Type, DataManager.Image);
            }

            if(Color != null){
                PostMap.put(DataManager.Themes, Color);
            }
            if(Color == "") {
                PostMap.put(DataManager.Themes, DataManager.Default);
            }

            if(ImageDownloadURI == "") {
                PostMap.put(DataManager.Type, DataManager.Text);
            }


            MNoteData.child(CurrentUserID)
                    .child(TimestampString)
                    .updateChildren(PostMap)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                Progressbar.setVisibility(View.GONE);
                                AddnoteButton.setEnabled(true);

                                Toast.makeText(getContext(), "Success", Toast.LENGTH_LONG).show();



                                dismiss();


                            }
                            else {
                                Progressbar.setVisibility(View.GONE);
                                AddnoteButton.setEnabled(true);
                                Toast.makeText(getContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Progressbar.setVisibility(View.GONE);
                            AddnoteButton.setEnabled(true);
                            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });

        }
    }
    /// todo start posting to server ------------------------------------------------------



    ///todo image uploading function -----------------------------------------------------------
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == IMAGECODE && resultCode == RESULT_OK) {
            alertDialog.dismiss();

            ImageUploadProgress.setVisibility(View.VISIBLE);
            ImageUploadingMessageBox.setVisibility(View.GONE);

            Uri image_uri = data.getData();
            File Mfile = new File(SiliCompressor.with(getActivity())
                    .compress(FileUtils.getPath(getActivity(), image_uri), new File(getActivity().getCacheDir(), "temp")));

            Uri from_file = Uri.fromFile(Mfile);
            MImageStores.child(anstronCoreHelper.getFileNameFromUri(from_file))
                    .putFile(from_file)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            if (taskSnapshot.getMetadata() != null) {
                                if (taskSnapshot.getMetadata().getReference() != null) {
                                    Task<Uri> image_uri = taskSnapshot.getStorage().getDownloadUrl();

                                    image_uri.addOnSuccessListener(new OnSuccessListener<Uri>() {
                                        @Override
                                        public void onSuccess(Uri uri) {
                                            if(uri != null){
                                                ImageDownloadURI = uri.toString();

                                                ImageUploadProgress.setVisibility(View.GONE);
                                                ImageUploadingMessageBox.setVisibility(View.VISIBLE);
                                                ImageUploadStatus.setVisibility(View.VISIBLE);
                                                ImageUploadStatusText.setVisibility(View.VISIBLE);
                                                ImageUploadStatusText.setText("Image Upload Success");

                                            }
                                            else {

                                                ImageUploadProgress.setVisibility(View.GONE);
                                                ImageUploadingMessageBox.setVisibility(View.VISIBLE);
                                                ImageUploadStatus.setVisibility(View.GONE);
                                                ImageUploadStatusText.setVisibility(View.VISIBLE);
                                                ImageUploadStatusText.setText("Image Upload Failed");

                                                Toast.makeText(getContext(), "Somethings error on the server", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    })
                                            .addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(@NonNull Exception e) {
                                                    ImageUploadProgress.setVisibility(View.GONE);
                                                    ImageUploadingMessageBox.setVisibility(View.VISIBLE);
                                                    ImageUploadStatus.setVisibility(View.GONE);
                                                    ImageUploadStatusText.setVisibility(View.VISIBLE);
                                                    ImageUploadStatusText.setText("Image Upload Failed");
                                                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                }
                            }
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            ImageUploadProgress.setVisibility(View.GONE);
                            ImageUploadingMessageBox.setVisibility(View.VISIBLE);
                            ImageUploadStatus.setVisibility(View.GONE);
                            ImageUploadStatusText.setVisibility(View.VISIBLE);
                            ImageUploadStatusText.setText("Image Upload Failed");
                            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });

        } else {

        }
    }



    public interface BottomSheedLisiner{
        void refreshList(String data);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {
            MLisiner = (BottomSheedLisiner) context;
        }catch (ClassCastException e){
            e.printStackTrace();
        }

    }
    ///todo image uploading function -----------------------------------------------------------
}
