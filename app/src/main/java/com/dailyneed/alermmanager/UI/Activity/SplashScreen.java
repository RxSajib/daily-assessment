package com.dailyneed.alermmanager.UI.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.dailyneed.alermmanager.R;
import com.dailyneed.alermmanager.databinding.SplashscreenBinding;

import in.codeshuffle.typewriterview.TypeWriterListener;

public class SplashScreen extends AppCompatActivity {

    private Animation animation;
    private SplashscreenBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.splashscreen);

        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.M){
            getWindow().setStatusBarColor(getResources().getColor(R.color.white));
        }
        else {
            getWindow().setStatusBarColor(getResources().getColor(R.color.white));
        }


        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.top_animaction);
        binding.SplashScreenImage.setAnimation(animation);

        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                start_typing();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });



    }

    private void start_typing(){
        binding.AssestentTrext.setDelay(5000);
        binding.AssestentTrext.setWithMusic(false);
        binding.AssestentTrext.animateText("Daily Assistant");

        binding.AssestentTrext.setTypeWriterListener(new TypeWriterListener() {
            @Override
            public void onTypingStart(String text) {

            }

            @Override
            public void onTypingEnd(String text) {
                goto_mainpage();
            }

            @Override
            public void onCharacterTyped(String text, int position) {

            }

            @Override
            public void onTypingRemoved(String text) {

            }
        });
    }

    private void goto_mainpage(){
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        Animatoo.animateSlideLeft(SplashScreen.this);
        finish();
    }
}