package com.dailyneed.alermmanager.UI.Activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.dailyneed.alermmanager.databinding.ForgetpasswordBinding;
import com.google.android.material.textview.MaterialTextView;
import com.google.firebase.auth.FirebaseAuth;
import com.dailyneed.alermmanager.R;

import carbon.widget.LinearLayout;

public class ForgetPasswordPage extends AppCompatActivity {

    private FirebaseAuth Mauth;
    private ForgetpasswordBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.forgetpassword);


        binding.backButtonID.setOnClickListener(v -> {
            finish();
            Animatoo.animateSlideRight(ForgetPasswordPage.this);
        });

        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.M){
            getWindow().setStatusBarColor(getResources().getColor(R.color.login_background));
        }
        else {
            getWindow().setStatusBarColor(getResources().getColor(R.color.login_background));
        };
        Mauth = FirebaseAuth.getInstance();
        binding.LoginButtonID.setOnClickListener(v -> {
            String email_address = binding.EmailInputID.getText().toString().trim();

            if(email_address.isEmpty()){
                Toast.makeText(ForgetPasswordPage.this, "Email Address Require", Toast.LENGTH_SHORT).show();
            }
            else {
                binding.ProgressbarID.setVisibility(View.VISIBLE);
                binding.LoginButtonID.setEnabled(false);
                binding.LoginButtonID.setBackground(getResources().getDrawable(R.drawable.login_progressing_button_background));
                Mauth.sendPasswordResetEmail(email_address)
                        .addOnCompleteListener(task -> {
                            if(task.isSuccessful()){
                                binding.ProgressbarID.setVisibility(View.GONE);
                                binding.LoginButtonID.setEnabled(true);
                                binding.LoginButtonID.setBackground(getResources().getDrawable(R.drawable.normal_loginbutton));
                                openDialoag(email_address);
                            }
                            else {
                                binding.LoginButtonID.setBackground(getResources().getDrawable(R.drawable.normal_loginbutton));
                                binding.ProgressbarID.setVisibility(View.GONE);
                                binding.LoginButtonID.setEnabled(true);
                                Toast.makeText(ForgetPasswordPage.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        })
                        .addOnFailureListener(e -> {
                            binding.LoginButtonID.setBackground(getResources().getDrawable(R.drawable.normal_loginbutton));

                            binding.ProgressbarID.setVisibility(View.GONE);
                            binding.LoginButtonID.setEnabled(true);
                            Toast.makeText(ForgetPasswordPage.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        });
            }
        });
    }


    private void openDialoag(String Email){
        AlertDialog.Builder Mbuilder = new AlertDialog.Builder(ForgetPasswordPage.this);
        View Mview = LayoutInflater.from(ForgetPasswordPage.this).inflate(R.layout.success_layout, null, false);
        Mbuilder.setView(Mview);

        AlertDialog alertDialog = Mbuilder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();

        LinearLayout GmailButton = Mview.findViewById(R.id.GmailButtonID);
        GmailButton.setOnClickListener(v -> open_emailclint());
        MaterialTextView Message = Mview.findViewById(R.id.ExitTextID);
        String emailcolour = getColoredSpanned(Email, "#FF5252");
        String textone = "Great Job! We are sending a message on your email";
        String texttwo = "to resetting your password";
        Message.setText(Html.fromHtml(textone+" '  "+emailcolour+"  ' "+texttwo));
        LinearLayout ExitButton = Mview.findViewById(R.id.ExitButtonID);
        ExitButton.setOnClickListener(v -> alertDialog.dismiss());
    }


    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    private void open_emailclint(){
        Intent intent = getPackageManager().getLaunchIntentForPackage("com.google.android.gm");
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(ForgetPasswordPage.this);
    }
}