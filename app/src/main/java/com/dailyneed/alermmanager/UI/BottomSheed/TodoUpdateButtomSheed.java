package com.dailyneed.alermmanager.UI.BottomSheed;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;

import com.anstrontechnologies.corehelper.AnstronCoreHelper;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textview.MaterialTextView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.iceteck.silicompressorr.FileUtils;
import com.iceteck.silicompressorr.SiliCompressor;
import com.dailyneed.alermmanager.DataManager.DataManager;
import com.dailyneed.alermmanager.R;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import es.dmoral.toasty.Toasty;

import static android.app.Activity.RESULT_OK;

public class TodoUpdateButtomSheed extends BottomSheetDialogFragment {

    private String Link;
    private FirebaseAuth Mauth;
    private String CurrentUserID;
    private DatabaseReference MNoteData;

    public TodoUpdateButtomSheed(String link) {
        Link = link;
    }

    private EditText Title, Details;
    private RelativeLayout WhiteOval, BlackOval, RedOval, GreenOval, OrangeOval, TealOval;
    private ImageView WhiteDone, BlackDone, RedDone, GreenDone, OrangeDone, TealDone;
    private RelativeLayout UploadButton;
    private RelativeLayout Progressbar;
    private RelativeLayout UploadDataButton;
    private RelativeLayout Crossbutton;

    private RelativeLayout PickupImage;
    private AlertDialog alertDialog;
    private AnstronCoreHelper anstronCoreHelper;
    private StorageReference MImageStores;


    //todo data-------------------------------------------
    private String TitleText, DetailsText, ImageUri = "";
    private String Type = "";
    private String Themescolor = "";
    private static final int REQUESTCODE = 100;
    private static final int IMAGECODE = 100;

    //todo data-------------------------------------------

    //todo image function ---------------------------------
    private ProgressBar ImageUploadProgress;
    private LinearLayout ImageUploadingMessageBox;
    private ImageView ImageUploadStatus;
    private MaterialTextView ImageUploadStatusText;
    //todo image function ---------------------------------

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View Mview = inflater.inflate(R.layout.todo_update, container, false);


        initialize_view(Mview);

        return Mview;
    }

    private void initialize_view(View Mview){

        anstronCoreHelper = new AnstronCoreHelper(getContext());

        /// todo background work initialize ---------------------------------
        ImageUploadProgress = Mview.findViewById(R.id.ImageProgressbarID);
        ImageUploadingMessageBox = Mview.findViewById(R.id.ImageUploadingMessage);
        ImageUploadStatus = Mview.findViewById(R.id.ImageUploadStatus);
        ImageUploadStatusText = Mview.findViewById(R.id.ImageUploadStatusText);
        /// todo background work initialize ---------------------------------

        ///todo server initialize ---------------------------------------------
        Mauth = FirebaseAuth.getInstance();
        CurrentUserID = Mauth.getCurrentUser().getUid();
        MNoteData = FirebaseDatabase.getInstance().getReference().child(DataManager.NoteDatabase).child(CurrentUserID);
        MNoteData.keepSynced(true);
        MImageStores = FirebaseStorage.getInstance().getReference().child(DataManager.TodoListImageRoot);
        ///todo server initialize ---------------------------------------------



        PickupImage = Mview.findViewById(R.id.PicUpImageID);
        CurrentUserID = Mauth.getCurrentUser().getUid();
        UploadButton = Mview.findViewById(R.id.AddNoteButtonID);
        Crossbutton = Mview.findViewById(R.id.CrossButtonID);
        Crossbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        ///todo initialize all button and oval ----------------------------------------------------
        Title = Mview.findViewById(R.id.TitleID);
        Details = Mview.findViewById(R.id.DetsilsID);
        Progressbar = Mview.findViewById(R.id.ProgressbarID);



        WhiteOval = Mview.findViewById(R.id.WhiteButtonID);
        BlackOval = Mview.findViewById(R.id.BlackButtonID);
        RedOval = Mview.findViewById(R.id.RedButttonID);
        GreenOval = Mview.findViewById(R.id.GreenButtonID);
        OrangeOval = Mview.findViewById(R.id.GreenButtonID);
        TealOval = Mview.findViewById(R.id.TealButtonID);


        WhiteDone = Mview.findViewById(R.id.WhiteButtonIconID);
        BlackDone = Mview.findViewById(R.id.BlackImageButtonID);
        RedDone = Mview.findViewById(R.id.RedImageButtonID);
        GreenDone = Mview.findViewById(R.id.GreenImageButtonID);
        OrangeDone = Mview.findViewById(R.id.OrangeImageButtonID);
        TealDone = Mview.findViewById(R.id.TealImageButtonID);


        //todo control themes-------------------------------
        WhiteOval.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WhiteDone.setVisibility(View.VISIBLE);
                BlackDone.setVisibility(View.GONE);
                RedDone.setVisibility(View.GONE);
                GreenDone.setVisibility(View.GONE);
                OrangeDone.setVisibility(View.GONE);
                TealDone.setVisibility(View.GONE);
                Themescolor = DataManager.White;
            }
        });

        BlackOval.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WhiteDone.setVisibility(View.GONE);
                BlackDone.setVisibility(View.VISIBLE);
                RedDone.setVisibility(View.GONE);
                GreenDone.setVisibility(View.GONE);
                OrangeDone.setVisibility(View.GONE);
                TealDone.setVisibility(View.GONE);
                Themescolor = DataManager.Black;
            }
        });

        RedOval.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WhiteDone.setVisibility(View.GONE);
                BlackDone.setVisibility(View.GONE);
                RedDone.setVisibility(View.VISIBLE);
                GreenDone.setVisibility(View.GONE);
                OrangeDone.setVisibility(View.GONE);
                TealDone.setVisibility(View.GONE);
                Themescolor = DataManager.Red;
            }
        });

        GreenOval.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WhiteDone.setVisibility(View.GONE);
                BlackDone.setVisibility(View.GONE);
                RedDone.setVisibility(View.GONE);
                GreenDone.setVisibility(View.VISIBLE);
                OrangeDone.setVisibility(View.GONE);
                TealDone.setVisibility(View.GONE);
                Themescolor = DataManager.Green;

            }
        });

        OrangeOval.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WhiteDone.setVisibility(View.GONE);
                BlackDone.setVisibility(View.GONE);
                RedDone.setVisibility(View.GONE);
                GreenDone.setVisibility(View.GONE);
                OrangeDone.setVisibility(View.VISIBLE);
                TealDone.setVisibility(View.GONE);
                Themescolor = DataManager.Orange;
            }
        });

        TealOval.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WhiteDone.setVisibility(View.GONE);
                BlackDone.setVisibility(View.GONE);
                RedDone.setVisibility(View.GONE);
                GreenDone.setVisibility(View.GONE);
                OrangeDone.setVisibility(View.GONE);
                TealDone.setVisibility(View.VISIBLE);
                Themescolor = DataManager.Teal;
            }
        });
        //todo control themes-------------------------------


        UploadDataButton = Mview.findViewById(R.id.AddNoteButtonID);
        UploadDataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploading_data_server();
            }
        });
        ///todo initialize all button and oval ----------------------------------------------------

        PickupImage.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                if(ReadPhoneExtranalStoragePermission()){
                    opendialoag();
                }
            }
        });

        reading_server();
    }

    private void opendialoag(){
        MaterialAlertDialogBuilder Mbuilder = new MaterialAlertDialogBuilder(getContext());
        View Mview = LayoutInflater.from(getContext()).inflate(R.layout.pickup_image_layout, null, false);
        Mbuilder.setView(Mview);


        alertDialog = Mbuilder.create();
        alertDialog.show();

        RelativeLayout PhotoButton, CameraButton;
        PhotoButton = Mview.findViewById(R.id.PhotosButtonID);
        CameraButton = Mview.findViewById(R.id.CameraButtonID);


        PhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, IMAGECODE);
            }
        });

        CameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void reading_server(){
        MNoteData.child(Link)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if(snapshot.exists()){
                            if(snapshot.hasChild(DataManager.Title)){
                                TitleText = snapshot.child(DataManager.Title).getValue().toString();
                                Title.setText(TitleText);
                            }

                            if(snapshot.hasChild(DataManager.Details)){
                                DetailsText = snapshot.child(DataManager.Details).getValue().toString();
                                Details.setText(DetailsText);
                            }

                            if(snapshot.hasChild(DataManager.ImageURI)){
                                ImageUri = snapshot.child(DataManager.ImageURI).getValue().toString();
                                
                            }

                            if(snapshot.hasChild(DataManager.Type)){
                                Type = snapshot.child(DataManager.Type).getValue().toString();
                            }

                            if(snapshot.hasChild(DataManager.Themes)){
                                Themescolor = snapshot.child(DataManager.Themes).getValue().toString();

                                if(Themescolor.equals(DataManager.White)){
                                    WhiteDone.setVisibility(View.VISIBLE);
                                }
                                if(Themescolor.equals(DataManager.Black)){
                                    BlackDone.setVisibility(View.VISIBLE);

                                }
                                if(Themescolor.equals(DataManager.Red)){
                                    RedDone.setVisibility(View.VISIBLE);
                                }
                                if(Themescolor.equals(DataManager.Green)){
                                    GreenOval.setVisibility(View.VISIBLE);
                                }
                                if(Themescolor.equals(DataManager.Orange)){
                                    OrangeDone.setVisibility(View.VISIBLE);
                                }
                                if(Themescolor.equals(DataManager.Teal)){
                                    TealDone.setVisibility(View.VISIBLE);
                                }
                            }

                        }
                        else {
//                            Toast.makeText(getContext(), "something error", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
    }

    private void uploading_data_server(){
        String GetTitleText = Title.getText().toString().trim();
        String GetDetailsText = Details.getText().toString().trim();

        if(GetTitleText.isEmpty()){
            Toast.makeText(getContext(), "Title Require", Toast.LENGTH_LONG).show();
        }
        else if(GetDetailsText.isEmpty()){
            Toast.makeText(getContext(), "Details Require", Toast.LENGTH_SHORT).show();
        }
        else {

            Progressbar.setVisibility(View.VISIBLE);
            UploadDataButton.setEnabled(false);

            Long Timestamp = System.currentTimeMillis() / 1000;
            String TimestampString = Timestamp.toString();

            Map<String, Object> PostMap = new HashMap<>();
            PostMap.put(DataManager.Title, GetTitleText);
            PostMap.put(DataManager.Details, GetDetailsText);
            PostMap.put(DataManager.Timestamp, Timestamp);
            PostMap.put(DataManager.Search, DetailsText.toLowerCase());
            PostMap.put(DataManager.Order, ~(Integer.parseInt(TimestampString) - 1));

            if(Type.equals(DataManager.Image)){
                PostMap.put(DataManager.ImageURI, ImageUri);
                PostMap.put(DataManager.Type, DataManager.Image);
            }

            if(Type.equals(DataManager.Text)){
                PostMap.put(DataManager.Type, DataManager.Text);
            }

            if(Themescolor.equals(DataManager.Default)){
                PostMap.put(DataManager.Themes, DataManager.Default);
            }

            if(!Themescolor.equals(DataManager.Default)){
                PostMap.put(DataManager.Themes, Themescolor);
            }

            MNoteData.child(Link)
                    .updateChildren(PostMap)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                Progressbar.setVisibility(View.GONE);
                                UploadDataButton.setEnabled(true);
                                Toasty.success(getContext(), "Success", Toasty.LENGTH_LONG).show();
                                dismiss();
                            }
                            else {
                                Toast.makeText(getContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                Progressbar.setVisibility(View.GONE);
                                UploadDataButton.setEnabled(true);
                            }
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getContext(), e.getMessage().toString(), Toast.LENGTH_SHORT).show();
                            Progressbar.setVisibility(View.GONE);
                            UploadDataButton.setEnabled(true);
                        }
                    });

        }
    }

    ///todo extranal permission-------------------------------------------
    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean ReadPhoneExtranalStoragePermission(){
        if(ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
            return true;
        }
        else {
            getActivity().requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUESTCODE);
            return false;
        }
    }
    ///todo extranal permission-------------------------------------------


    ///todo upload image to server ---------------------------------------------------------

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == IMAGECODE && resultCode == RESULT_OK){
            alertDialog.dismiss();

            ImageUploadProgress.setVisibility(View.VISIBLE);
            ImageUploadingMessageBox.setVisibility(View.GONE);

            Uri image_uri = data.getData();
            File Mfile = new File(SiliCompressor.with(getActivity())
                    .compress(FileUtils.getPath(getActivity(), image_uri), new File(getActivity().getCacheDir(), "temp")));

            Uri from_file = Uri.fromFile(Mfile);
            MImageStores.child(anstronCoreHelper.getFileNameFromUri(from_file))
                    .putFile(from_file)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            if (taskSnapshot.getMetadata() != null) {
                                if (taskSnapshot.getMetadata().getReference() != null) {
                                    Task<Uri> image_uri = taskSnapshot.getStorage().getDownloadUrl();

                                    image_uri.addOnSuccessListener(new OnSuccessListener<Uri>() {
                                        @Override
                                        public void onSuccess(Uri uri) {
                                            if(uri != null){
                                                ImageUri = uri.toString();
                                                Type = DataManager.Image;
                                                ImageUploadProgress.setVisibility(View.GONE);
                                                ImageUploadingMessageBox.setVisibility(View.VISIBLE);
                                                ImageUploadStatus.setVisibility(View.VISIBLE);
                                                ImageUploadStatusText.setVisibility(View.VISIBLE);
                                                ImageUploadStatusText.setText("Image Upload Success");

                                            }
                                            else {

                                                ImageUploadProgress.setVisibility(View.GONE);
                                                ImageUploadingMessageBox.setVisibility(View.VISIBLE);
                                                ImageUploadStatus.setVisibility(View.GONE);
                                                ImageUploadStatusText.setVisibility(View.VISIBLE);
                                                ImageUploadStatusText.setText("Image Upload Failed");

                                                Toast.makeText(getContext(), "Somethings error on the server", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    })
                                            .addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(@NonNull Exception e) {
                                                    ImageUploadProgress.setVisibility(View.GONE);
                                                    ImageUploadingMessageBox.setVisibility(View.VISIBLE);
                                                    ImageUploadStatus.setVisibility(View.GONE);
                                                    ImageUploadStatusText.setVisibility(View.VISIBLE);
                                                    ImageUploadStatusText.setText("Image Upload Failed");
                                                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                }
                            }
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            ImageUploadProgress.setVisibility(View.GONE);
                            ImageUploadingMessageBox.setVisibility(View.VISIBLE);
                            ImageUploadStatus.setVisibility(View.GONE);
                            ImageUploadStatusText.setVisibility(View.VISIBLE);
                            ImageUploadStatusText.setText("Image Upload Failed");
                            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
        }
    }

    ///todo upload image to server ---------------------------------------------------------



    public interface OnBottomSheedLisiner{
        void onBottomSheed(String data);
    }
}
