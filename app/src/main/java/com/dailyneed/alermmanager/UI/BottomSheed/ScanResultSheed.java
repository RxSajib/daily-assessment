package com.dailyneed.alermmanager.UI.BottomSheed;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.ClipboardManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.textview.MaterialTextView;
import com.dailyneed.alermmanager.R;
import com.tapadoo.alerter.Alerter;

public class ScanResultSheed extends BottomSheetDialogFragment {

    private MaterialTextView ScanText;

    private String Result;
    private LinearLayout CopyBtn;
    private LinearLayout WebBtn;

    public ScanResultSheed(String result) {
        Result = result;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View Mview = inflater.inflate(R.layout.bottom_sheed_dialoag_qrcode, null, false);



        ScanText = Mview.findViewById(R.id.QrCodeScanTextID);
        CopyBtn = Mview.findViewById(R.id.CopyButtonID);
        WebBtn = Mview.findViewById(R.id.WebButtonID);



        set_result_data(Result);


        return Mview;

    }

    private void set_result_data(String data) {


        if (data != null) {

            boolean isValid = URLUtil.isValidUrl(data);
            if (isValid) {
                WebBtn.setVisibility(View.VISIBLE);

                WebBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(data));
                        startActivity(i);
                    }
                });

                CopyBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ClipboardManager clipboardManager = (ClipboardManager) (getActivity()).getSystemService(Context.CLIPBOARD_SERVICE);
                        clipboardManager.setText(data);

                        Alerter.create(getActivity())
                                .setTitle("Copy Success")
                                .setText(data)
                                .show();
                    }
                });

            } else {
                WebBtn.setVisibility(View.GONE);

                CopyBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        ClipboardManager clipboardManager = (ClipboardManager) (getActivity()).getSystemService(Context.CLIPBOARD_SERVICE);
                        clipboardManager.setText(data);

                        Alerter.create(getActivity())
                                .setTitle("Copy Success")
                                .setText(data)
                                .show();
                    }
                });
            }

            ScanText.setText(data);
        } else {
            ScanText.setText("Error scan the qr code or bar code try again !");
        }
    }
}
