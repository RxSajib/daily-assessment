package com.dailyneed.alermmanager.MusicControler;

import android.content.Context;
import android.media.MediaPlayer;
import android.provider.Settings;

public class MusicControler {


    public static MediaPlayer mediaPlayer;

    public void startplayingMusic(Context context){
        mediaPlayer = MediaPlayer.create(context, Settings.System.DEFAULT_RINGTONE_URI);
        mediaPlayer.start();
    }

    public void stopplayingMusic(Context context){

            if(mediaPlayer.isPlaying()){
                mediaPlayer.stop();
                mediaPlayer.release();
                mediaPlayer = null;
            }


    }

}
