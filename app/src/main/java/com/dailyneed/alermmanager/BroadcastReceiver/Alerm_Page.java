package com.dailyneed.alermmanager.BroadcastReceiver;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.dailyneed.alermmanager.Adapter.AlermCatAdapter;
import com.dailyneed.alermmanager.DataManager.DataManager;
import com.dailyneed.alermmanager.Memory.CashAlerm;
import com.dailyneed.alermmanager.Memory.CashMac;
import com.dailyneed.alermmanager.Model.AlermTypeData;
import com.dailyneed.alermmanager.R;
import com.dailyneed.alermmanager.RoomDatabase.AlarmData;
import com.dailyneed.alermmanager.RoomDatabase.AlarmDatabaseRepository;
import com.dailyneed.alermmanager.databinding.AlermviewBinding;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Alerm_Page extends AppCompatActivity {

    private String AlermCondition = "";

    private AlermCatAdapter alermCatAdapter;
    private List<AlermTypeData> alermTypeDataList = new ArrayList<>();
    private AlermTypeData one, two, three, four;
    private DatabaseReference MAlermDatabaseReference;

    private CashAlerm memory;
    private CashMac cashMac;
    private String MacAddress;
    private AlermviewBinding binding;
    private AlarmDatabaseRepository databaseRepository;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.alermview);


        statusbarcolor();
        init_view();
    }

    private void statusbarcolor() {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.background));
        } else {
            getWindow().setStatusBarColor(getResources().getColor(R.color.background));
        }
    }

    private void init_view() {
        databaseRepository = new ViewModelProvider(this).get(AlarmDatabaseRepository.class);
        binding.WindowCloseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                Animatoo.animateSlideRight(Alerm_Page.this);
            }
        });

        cashMac = new CashMac(getApplicationContext());
        memory = new CashAlerm(getApplicationContext());


        MAlermDatabaseReference = FirebaseDatabase.getInstance().getReference().child(DataManager.MyAlarm);
        one = new AlermTypeData();
        one.setImage(R.drawable.capsules);
        one.setTitle(getResources().getString(R.string.capsules));

        two = new AlermTypeData();
        two.setTitle(getResources().getString(R.string.study));
        two.setImage(R.drawable.study);

        three = new AlermTypeData();
        three.setImage(R.drawable.workout);
        three.setTitle(getResources().getString(R.string.workout));

        four = new AlermTypeData();
        four.setImage(R.drawable.meal);
        four.setTitle(getResources().getString(R.string.meal));

     //   binding.AlermOneButtonID.setBackgroundResource(R.drawable.click_alerm_day);
    //    binding.AlermDailyButtonID.setBackgroundResource(R.drawable.alerm_one_background);
        AlermCondition = DataManager.Once;
/*
        binding.AlermOneButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.AlermOneButtonID.setBackgroundResource(R.drawable.click_alerm_day);
                binding.AlermDailyButtonID.setBackgroundResource(R.drawable.alerm_one_background);

                AlermCondition = DataManager.Once;
            }
        });

        binding.AlermDailyButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.AlermOneButtonID.setBackgroundResource(R.drawable.alerm_one_background);
                binding.AlermDailyButtonID.setBackgroundResource(R.drawable.click_alerm_day);

                AlermCondition = DataManager.Daily;
            }
        });

        */

        binding.AlermTypeButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getalermtype();
            }
        });

        binding.SaveButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setupalerm();

            }
        });
    }


    private void getalermtype() {
        BottomSheetDialog Mdialoag = new BottomSheetDialog(Alerm_Page.this);
        View Mview = LayoutInflater.from(getApplicationContext()).inflate(R.layout.alerm_typelayout, null, false);
        Mdialoag.setContentView(Mview);

        RecyclerView recyclerView = Mview.findViewById(R.id.RecylerViewID);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        alermTypeDataList.clear();


        alermTypeDataList.add(one);
        alermTypeDataList.add(two);
        alermTypeDataList.add(three);
        alermTypeDataList.add(four);
        alermCatAdapter = new AlermCatAdapter(getApplicationContext(), alermTypeDataList);
        recyclerView.setAdapter(alermCatAdapter);

        alermCatAdapter.setOnClickListener(new AlermCatAdapter.setOnClick() {
            @Override
            public void Click(String title, int image) {
                binding.AlermTypeID.setText(title);
                binding.Image.setImageResource(image);
                Mdialoag.dismiss();
            }
        });

        Mdialoag.show();
    }


    private void setupalerm() {

        Calendar calendar = Calendar.getInstance();
        if (Build.VERSION.SDK_INT >= 23) {


            calendar.set(
                    calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH),

                    binding.simpleTimePicker.getHour(),
                    binding.simpleTimePicker.getMinute(),
                    0
            );


        } else {
            calendar.set(
                    calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH),

                    binding.simpleTimePicker.getCurrentHour(),
                    binding.simpleTimePicker.getCurrentMinute(),
                    0
            );

        }

        String AlarmType = binding.AlermTypeID.getText().toString().trim();
        String Details = binding.DetailsInputID.getText().toString().trim();

        if (AlarmType.equals(getResources().getString(R.string.alermtype))) {
            Toast.makeText(this, "Set Alarm Type", Toast.LENGTH_SHORT).show();
        } else if (AlermCondition.equals("")) {
            Toast.makeText(this, "Set Alarm Mode", Toast.LENGTH_SHORT).show();
        } else {

            long Timestamp = calendar.getTimeInMillis();

            Map<String, Object> alermMap = new HashMap<String, Object>();
            alermMap.put(DataManager.AlarmType, AlarmType);
            alermMap.put(DataManager.AlarmDetails, Details);
            alermMap.put(DataManager.AlarmMode, AlermCondition);
            alermMap.put(DataManager.AlarmTimestamp, String.valueOf(calendar.getTimeInMillis()));

            AlarmData alarmData = new AlarmData(AlarmType, Timestamp, Details, AlarmType);
            databaseRepository.DeleteDataByID(AlarmType);
            databaseRepository.SetAlarmData(alarmData);

            if (AlarmType.equals(DataManager.Capsules)) {

                memory.PutTime(DataManager.Capsules, String.valueOf(Timestamp));

                AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                Intent intent = new Intent(this, CaptuleAlarm.class);

                PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intent, 0);
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, Timestamp, alarmManager.INTERVAL_DAY, pendingIntent);

                Toast.makeText(this, "Alarm set success", Toast.LENGTH_SHORT).show();

                Intent alarmChanged = new Intent("android.intent.action.ALARM_CHANGED");
                alarmChanged.putExtra("alarmSet", true);
                sendBroadcast(alarmChanged);
            }

            if (AlarmType.equals(DataManager.Study)) {
                memory.PutTime(DataManager.Study, String.valueOf(Timestamp));
                AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                Intent intent = new Intent(this, StudyAlarm.class);

                PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intent, 0);
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, Timestamp, alarmManager.INTERVAL_DAY, pendingIntent);

                Toast.makeText(this, "Alarm set success", Toast.LENGTH_SHORT).show();

                Intent alarmChanged = new Intent("android.intent.action.ALARM_CHANGED");
                alarmChanged.putExtra("alarmSet", true);
                sendBroadcast(alarmChanged);
            }

            if (AlarmType.equals(DataManager.Workout)) {
                memory.PutTime(DataManager.Workout, String.valueOf(Timestamp));

                AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                Intent intent = new Intent(this, WorkOut.class);

                PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intent, 0);
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, Timestamp, alarmManager.INTERVAL_DAY, pendingIntent);

                Toast.makeText(this, "Alarm set success", Toast.LENGTH_SHORT).show();

                Intent alarmChanged = new Intent("android.intent.action.ALARM_CHANGED");
                alarmChanged.putExtra("alarmSet", true);
                sendBroadcast(alarmChanged);
            }

            if (AlarmType.equals(DataManager.Meal)) {
                memory.PutTime(DataManager.Meal, String.valueOf(Timestamp));
                AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                Intent intent = new Intent(this, MealAlarm.class);

                PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intent, 0);
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, Timestamp, alarmManager.INTERVAL_DAY, pendingIntent);

                Toast.makeText(this, "Alarm set success", Toast.LENGTH_SHORT).show();

                Intent alarmChanged = new Intent("android.intent.action.ALARM_CHANGED");
                alarmChanged.putExtra("alarmSet", true);
                sendBroadcast(alarmChanged);
            }

            MacAddress = cashMac.getMacAddress(DataManager.MacKey);
            finish();
            Animatoo.animateSlideRight(Alerm_Page.this);

              /*  MAlermDatabaseReference.child(MacAddress).child(AlarmType)
                        .updateChildren(alermMap)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(task.isSuccessful()){
                                    finish();
                                    Animatoo.animateSlideRight(Alerm_Page.this);
                                }
                                else {
                                    Toast.makeText(Alerm_Page.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(Alerm_Page.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });

*/

        }

    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(Alerm_Page.this);
    }


}