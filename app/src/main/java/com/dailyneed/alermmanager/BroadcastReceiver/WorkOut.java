package com.dailyneed.alermmanager.BroadcastReceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.dailyneed.Services.MyAlarmServices;
import com.dailyneed.alermmanager.DataManager.DataManager;
import com.dailyneed.alermmanager.Memory.CashAlerm;
import com.google.android.gms.vision.CameraSource;

public class WorkOut extends BroadcastReceiver {

    private CashAlerm memory;
    private int Code = 123;
    CameraSource cameraSource;


    @Override
    public void onReceive(Context context, Intent intent) {


        memory = new CashAlerm(context);
        String Timestamp = memory.gettimestamp(DataManager.Workout);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            Intent intent1 = new Intent(context, MyAlarmServices.class);
            intent1.putExtra("Timestamp", Timestamp);
            intent1.putExtra(DataManager.ALARM, DataManager.Workout);
            context.startForegroundService(intent1);

        }
        else {
            Intent serviceIntent = new Intent(context, MyAlarmServices.class);
            serviceIntent.putExtra("Timestamp", Timestamp);
            serviceIntent.putExtra(DataManager.ALARM, DataManager.Workout);
            context.startService(serviceIntent);
        }

    

    }







}
