package com.dailyneed.ViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.dailyneed.Network.AlarmGET;
import com.dailyneed.alermmanager.Model.AlarmModel;

import java.util.List;

public class ViewModel extends AndroidViewModel {

    private AlarmGET alarmGET;

    public ViewModel(@NonNull Application application) {
        super(application);
        alarmGET = new AlarmGET(application);
    }

    public LiveData<List<AlarmModel>> getalarm(){
        return alarmGET.getalarm_data();
    }
}
