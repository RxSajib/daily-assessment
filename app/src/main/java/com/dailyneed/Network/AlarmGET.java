package com.dailyneed.Network;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.dailyneed.alermmanager.DataManager.DataManager;
import com.dailyneed.alermmanager.Memory.CashMac;
import com.dailyneed.alermmanager.Model.AlarmModel;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class AlarmGET {

    private Application application;
    private MutableLiveData<List<AlarmModel>> data;
    private DatabaseReference MAlarmDatabase;
    private List<AlarmModel> alarmModelList ;
    private CashMac cashMac;
    private String macaddress;

    public AlarmGET(Application application){

        this.application = application;
        data = new MutableLiveData<>();
        MAlarmDatabase.keepSynced(true);
        alarmModelList = new ArrayList<>();
        cashMac = new CashMac(application);
        macaddress = cashMac.getMacAddress(DataManager.MacKey);
        MAlarmDatabase = FirebaseDatabase.getInstance().getReference().child(DataManager.MyAlarm);
    }

    public LiveData<List<AlarmModel>> getalarm_data(){
        MAlarmDatabase.child(macaddress).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

                if(snapshot.exists()){
                    AlarmModel alarmModel = snapshot.getValue(AlarmModel.class);
                    alarmModelList.add(alarmModel);
                    data.setValue(alarmModelList);
                }
                else {
                    data.setValue(null);
                }

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        return data;
    }
}
